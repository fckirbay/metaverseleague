const UserInfo = Moralis.Object.extend("UserInfo");
const ClubInfo = Moralis.Object.extend("ClubInfo");
const Players = Moralis.Object.extend("Players");
const Watchlist = Moralis.Object.extend("Watchlist");
const ClubPlayers = Moralis.Object.extend("ClubPlayers");
const web3 = Moralis.web3ByChain("0x89"); // polygon
const BN = web3.utils.BN;
const mainContractAddress = "0x3BE10dcdb81311EC095ff975FdaC4c77a3baf92B"; 

const mainContractAbi = [
	{
		"inputs": [],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "account",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "operator",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "bool",
				"name": "approved",
				"type": "bool"
			}
		],
		"name": "ApprovalForAll",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "operator",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "from",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "uint256[]",
				"name": "ids",
				"type": "uint256[]"
			},
			{
				"indexed": false,
				"internalType": "uint256[]",
				"name": "values",
				"type": "uint256[]"
			}
		],
		"name": "TransferBatch",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "operator",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "from",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			},
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "value",
				"type": "uint256"
			}
		],
		"name": "TransferSingle",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"internalType": "string",
				"name": "value",
				"type": "string"
			},
			{
				"indexed": true,
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			}
		],
		"name": "URI",
		"type": "event"
	},
	{
		"inputs": [],
		"name": "addFundBalance",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "account",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			}
		],
		"name": "balanceOf",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address[]",
				"name": "accounts",
				"type": "address[]"
			},
			{
				"internalType": "uint256[]",
				"name": "ids",
				"type": "uint256[]"
			}
		],
		"name": "balanceOfBatch",
		"outputs": [
			{
				"internalType": "uint256[]",
				"name": "",
				"type": "uint256[]"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "tokenId",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "price",
				"type": "uint256"
			}
		],
		"name": "deListMftItem",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "",
				"type": "address"
			}
		],
		"name": "fundBalances",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "getFundBalance",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "tokenId",
				"type": "uint256"
			}
		],
		"name": "getTokenPrice",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "account",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "operator",
				"type": "address"
			}
		],
		"name": "isApprovedForAll",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "tokenId",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "price",
				"type": "uint256"
			}
		],
		"name": "listMftItem",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "price",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "data",
				"type": "bytes"
			}
		],
		"name": "mint",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"internalType": "uint256[]",
				"name": "ids",
				"type": "uint256[]"
			},
			{
				"internalType": "uint256[]",
				"name": "amounts",
				"type": "uint256[]"
			},
			{
				"internalType": "uint256[]",
				"name": "prices",
				"type": "uint256[]"
			},
			{
				"internalType": "bytes",
				"name": "data",
				"type": "bytes"
			}
		],
		"name": "mintBatch",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			}
		],
		"name": "preSaleMftItem",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "from",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"internalType": "uint256[]",
				"name": "ids",
				"type": "uint256[]"
			},
			{
				"internalType": "uint256[]",
				"name": "amounts",
				"type": "uint256[]"
			},
			{
				"internalType": "bytes",
				"name": "data",
				"type": "bytes"
			}
		],
		"name": "safeBatchTransferFrom",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "from",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "id",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			},
			{
				"internalType": "bytes",
				"name": "data",
				"type": "bytes"
			}
		],
		"name": "safeTransferFrom",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "operator",
				"type": "address"
			},
			{
				"internalType": "bool",
				"name": "approved",
				"type": "bool"
			}
		],
		"name": "setApprovalForAll",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "string",
				"name": "newuri",
				"type": "string"
			}
		],
		"name": "setURI",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "bytes4",
				"name": "interfaceId",
				"type": "bytes4"
			}
		],
		"name": "supportsInterface",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "tokenId",
				"type": "uint256"
			},
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			}
		],
		"name": "transferMftErc20",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "tokenId",
				"type": "uint256"
			}
		],
		"name": "transferMftItemWithErc20",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"name": "uri",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			}
		],
		"name": "withdrawToWalletFundBalance",
		"outputs": [],
		"stateMutability": "payable",
		"type": "function"
	}
];

Moralis.Cloud.define("createUserInfo", async (request) => {
  const query = new Moralis.Query("UserInfo");
  query.equalTo("userId", request.user);
  const results = await query.first();
  
  if(!results) {
	const userInfo = new UserInfo();
	userInfo.set("userId", request.user);
    userInfo.set("ethAddress", request.user.get("ethAddress"));
    userInfo.set("username", request.user.get("username"));
    const userInfoResult = await userInfo.save();
    
    return {"result": "success"};
  }
});

Moralis.Cloud.define("watchlist", async (request) => {
  logger.info("xxxxx")
  logger.info(JSON.stringify(request.params.player));
  
  const playerQuery = new Moralis.Query("Players");
  playerQuery.equalTo("player_id", request.params.player);
  const playerResult = await playerQuery.first();
  
  logger.info(JSON.stringify(playerResult));
  logger.info(JSON.stringify(request.user));
  
  const userQuery = new Moralis.Query("UserInfo");
  userQuery.equalTo("userId", request.user);
  const userInfo = await userQuery.first();
  
  logger.info(JSON.stringify(userInfo));


  const query = new Moralis.Query("Watchlist");
  query.equalTo("watcherId", userInfo);
  query.equalTo("playerId", playerResult);
  const results = await query.first();
  
  if(!results) {
	const watchlist = new Watchlist();
    watchlist.set("watcherId", userInfo);
	watchlist.set("playerId", playerResult);
    const watchResult = await watchlist.save();
    
    logger.info("xxxxxxx");
    logger.info(userInfo.id);
    logger.info("xxxxxxx");
    
    logger.info("xxxxxxx");
    logger.info(request.params.player);
    logger.info("xxxxxxx");

    const increaseWatchers = new Moralis.Query("Players");
    increaseWatchers.equalTo("player_id", request.params.player);
    const resultWatchers = await increaseWatchers.first();
    resultWatchers.increment("watcherCount");
    await resultWatchers.save();
    logger.info(resultWatchers);
    return "watch"
  } else {
    await results.destroy(null, {useMasterKey:true});
    const increaseWatchers = new Moralis.Query("Players");
    increaseWatchers.equalTo("player_id", request.params.player);
    const resultWatchers = await increaseWatchers.first();
    resultWatchers.decrement("watcherCount");
    await resultWatchers.save();
    /*const query = new Moralis.Query("NftLikes");
    query.equalTo("nft", nftPointer);
    query.equalTo("user", request.user);
    const deleteResult = await query.destroy();
    return {"result": "Unliked"};*/
    return "disable watch"
  }
});

Moralis.Cloud.define("startTransfer", async (request) => {

  const query = new Moralis.Query("UserInfo");
  query.equalTo("userId", request.user);
  const userInfo = await query.first();
  
  if(userInfo) {
    
    const queryPlayer = new Moralis.Query("Players");
    queryPlayer.equalTo("player_id", parseInt(request.params.player));
    const playerInfo = await queryPlayer.first();
    
    const clubPlayerQuery = new Moralis.Query("ClubPlayers");
    clubPlayerQuery.equalTo("player", playerInfo);
    clubPlayerQuery.equalTo("completed", false);
    const isNegotiating = await clubPlayerQuery.count();
    
    if(isNegotiating == 0) {
      const clubPlayer = new ClubPlayers();
      clubPlayer.set("club", userInfo.get("club"));
      clubPlayer.set("player", playerInfo);
      clubPlayer.set("contractEndDate", new Date());
      clubPlayer.set("transferFee", playerInfo.get("market_value"));
      clubPlayer.set("managerCommission", playerInfo.get("market_value") * 0.05);
      clubPlayer.set("transactionHash", request.params.transactionHash);
      clubPlayer.set("completed", false);
      clubPlayer.set("user", userInfo);
      clubPlayer.set("transferFee", playerInfo.get("market_value") / 10000);
      clubPlayer.set("managerCommission", playerInfo.get("market_value") * 0.05 / 10000);
      const clubPlayerResult = await clubPlayer.save();
      playerInfo.set("on_sale", false);
      playerInfo.set("open_loan", false);
      await playerInfo.save();
      
      return {"result": "success"};
    } else {
      return {"result": "fail", "message": "This player already negotiating with a club!"};
    }
    
  } else {
    return {"result": "fail"};
  }
});

Moralis.Cloud.define("checkNegotiation", async (request) => {

  let result = false;
  let owner = false;

  const queryPlayer = new Moralis.Query("Players");
  queryPlayer.equalTo("player_id", parseInt(request.params.player));
  const playerInfo = await queryPlayer.first();
  
  const clubPlayerQuery = new Moralis.Query("ClubPlayers");
  clubPlayerQuery.equalTo("player", playerInfo);
  clubPlayerQuery.include("club");
  const isNegotiating = await clubPlayerQuery.first();
  
  if(isNegotiating) {
    const query = new Moralis.Query("UserInfo");
    query.equalTo("userId", request.user);
    const userInfo = await query.first();

    result = !isNegotiating.get("completed");

    if(userInfo.get("club").id == isNegotiating.get("club").id) {
      owner = true;
    } else {
      owner = false;
    }
  }
  return {"result": result, "owner": owner};
  
});

Moralis.Cloud.afterSave("PolygonNFTTransfers", async function(request) {
  const confirmed = request.object.get("confirmed");
  if (confirmed == true) {
    const queryClubPlayers = new Moralis.Query("ClubPlayers");
    queryClubPlayers.equalTo("transactionHash", request.object.get("transaction_hash"));
    const resultClubPlayers = await queryClubPlayers.first();
    resultClubPlayers.set("completed", true);
    await resultClubPlayers.save();
    
    /*const queryPlayer = new Moralis.Query("Players");
    queryPlayer.equalTo("player", resultClubPlayers.get("player"));
    const resultPlayer = await queryPlayer.first();
    resultPlayer.set("club", resultClubPlayers.get("club"));
    await resultPlayer.save();*/
  } else if(confirmed == false) {
    const queryClubPlayers = new Moralis.Query("ClubPlayers");
    queryClubPlayers.equalTo("transactionHash", request.object.get("transaction_hash"));
    const resultClubPlayers = await queryClubPlayers.first();
    await resultClubPlayers.destroy();
  } else {
    logger.info("not confirmed!!!");
    // handle unconfirmed case 
  }
});

Moralis.Cloud.afterSave("ClubPlayers", async function(request) {
  const completed = request.object.get("completed");
  if (completed == true) {
    logger.info('Mint Address: ' + rrequest.object.get("user").get("ethAddress") );
    logger.info('player: ' + request.object.get("player").get("player_id"));

    
      const contract = new web3.eth.Contract(mainContractAbi, mainContractAddress);
      const name = await contract.methods
          .name()
          .call()
          .catch(() => "");
      logger.info('Contract name: ' + name);
  
      const data = await contract.methods.mint(request.object.get("user").get("ethAddress"), request.object.get("player").get("player_id"), 1, 0, [] );
    
      let baseFee = await web3.eth.getGasPrice();
      let baseFeeBN = web3.utils.toBN(baseFee);
      let maxPriorityFee = new BN(3000000000);
      let maxFee = baseFeeBN.mul(new BN(3)).add(maxPriorityFee);
      //  var maxPriorityFee = web3.utils.toWei('10', 'wei');
  
      logger.info('baseFee : ' + baseFee);
  
      var estGas = await data.estimateGas({
        "from" : "0x5F9e9bcdA6E73827a0D159319CE341b8E245cC8f",  // payer
        "data" : data.encodeABI(),
      });
    
    logger.info("gassss -> " + estGas);
    logger.info("baseFee -> " + baseFee);
  
      //var maxFee = baseFee * estGas;
      
      logger.info('maxFee         : ' + maxFee.toString());
      logger.info('maxPriorityFee : ' + maxPriorityFee.toString());
    
      var rawTx = {
          "to": mainContractAddress,
          "from": request.object.get("user").get("ethAddress"),
          "value": web3.utils.toWei('0', 'ether'),
          "data": data.encodeABI(),
          "gasLimit": estGas,
          "maxFeePerGas": maxFee,
          "maxPriorityFeePerGas": maxFee,
          "type": "0x02"
      };
  
      logger.info('RawTx: ' + JSON.stringify(rawTx));
    
      /*var cTx = await new EthereumTx(rawTx, { 'chain': 'polygon' });
      cTx.sign(new Buffer("ee65b9926b4a0c6e2bee198f327639c5c5a4af079d397f39722fe1ce4a3bc58f", 'hex'))*/
  
      const transaction_data = await web3.eth.accounts.signTransaction(
          rawTx,
          "679e06020b20b6b74a37e8d8239e49df35e9efca75fe896b257a6ac2ea2c3e80"
      );
  
      const transaction_receipt = await web3.eth.sendSignedTransaction(
          transaction_data.rawTransaction
      );
  
      logger.info('Hash: ' + transaction_receipt.transactionHash);
      return transaction_receipt.transactionHash;
  }
});

Moralis.Cloud.afterSave("MintNft", async function(request) {

  logger.info('Mint Address: ' + request.params.address);
  logger.info('Token URI   : ' + request.params.tokenUri);
  
    const contract = new web3.eth.Contract(mainAbi, mainContractAddress);
    const name = await contract.methods
        .name()
        .call()
        .catch(() => "");
    logger.info('Contract name: ' + name);

    const data = await contract.methods.mintNft(request.params.address, request.params.tokenUri);
  
    let baseFee = await web3.eth.getGasPrice();
    let baseFeeBN = web3.utils.toBN(baseFee);
    let maxPriorityFee = new BN(3000000000);
    let maxFee = baseFeeBN.mul(new BN(3)).add(maxPriorityFee);
    //  var maxPriorityFee = web3.utils.toWei('10', 'wei');

    logger.info('baseFee : ' + baseFee);

    var estGas = await data.estimateGas({
      "from" : payerAddress,
      "data" : data.encodeABI(),
    });
  
  logger.info("gassss -> " + estGas);
  logger.info("baseFee -> " + baseFee);

    //var maxFee = baseFee * estGas;
    
    logger.info('maxFee         : ' + maxFee.toString());
    logger.info('maxPriorityFee : ' + maxPriorityFee.toString());
  
    var rawTx = {
        "to": mainContractAddress,
        "from": request.params.address,
        "value": web3.utils.toWei('0', 'ether'),
        "data": data.encodeABI(),
        "gasLimit": estGas,
        "maxFeePerGas": maxFee,
        "maxPriorityFeePerGas": maxFee,
        "type": "0x02"
    };

    logger.info('RawTx: ' + JSON.stringify(rawTx));
  
    /*var cTx = await new EthereumTx(rawTx, { 'chain': 'polygon' });
  	cTx.sign(new Buffer("ee65b9926b4a0c6e2bee198f327639c5c5a4af079d397f39722fe1ce4a3bc58f", 'hex'))*/

    const transaction_data = await web3.eth.accounts.signTransaction(
        rawTx,
      	"ee65b9926b4a0c6e2bee198f327639c5c5a4af079d397f39722fe1ce4a3bc58f"
    );

    const transaction_receipt = await web3.eth.sendSignedTransaction(
        transaction_data.rawTransaction
    );

    logger.info('Hash: ' + transaction_receipt.transactionHash);
    return transaction_receipt.transactionHash;
});