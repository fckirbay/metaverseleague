import React from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

const TermsAndConditions = () => {

    return (
        <div className="App">
            <Header />
            <div className="site-content">
                <div className="container">
                    <div className="card card--has-table">
                        <div className="card__header">
                            <h4>Terms & Conditions</h4>
                        </div>
                        <div className="card__content">
                            ...
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default TermsAndConditions;
