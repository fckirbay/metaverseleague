import React, { useState, useEffect } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import PlayerCard from "../../components/shared/PlayerCard";
import { useMoralis } from "react-moralis";
import InfiniteScroll from 'react-infinite-scroll-component';

const UserWatchlist = () => {
  const { Moralis } = useMoralis();


  const [players, setPlayers] = useState([]);
  const [hasMore, setHasMore] = useState(true);


  async function getWatchlist(user) {
    const query = new Moralis.Query("UserInfo");
    query.equalTo("userId", user);
    const userResult = await query.first();
    const query1 = new Moralis.Query("Watchlist");
    query1.equalTo("watcherId", userResult);
    query1.limit(30);
    const watcherResult = await query1.find();
    setPlayers(watcherResult)
    if(watcherResult.length < 30) {
      setHasMore(false);
    }
  }

  useEffect(() => {
    Moralis.User.current()
      .fetch()
      .then((user) => {
        if (user) {
          getWatchlist(user);
        }
      });

  }, []);

  return (
    <div className="App">
      <Header />
      <div className="site-content">
        <div className="container">
          <InfiniteScroll
            dataLength={players.length} //This is important field to render the next data
            next={() => getWatchlist()}
            hasMore={hasMore}
            loader={<h4>Loading...</h4>}
            /*endMessage={
              <p style={{ textAlign: 'center' }}>
                <b>Yay! You have seen it all</b>
              </p>
            }*/
          >
            <div className="row">
              {players.map((item, index) => {
                return <PlayerCard key={index} data={item.get("playerId")} />
              })}
            </div>
          </InfiniteScroll>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default UserWatchlist;
