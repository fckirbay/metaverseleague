import React from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

const Disclaimer = () => {

    return (
        <div className="App">
            <Header />
                <iframe src="/docs/index.html" title="Whitepaper" style={{width: "100%", height: "750px"}}></iframe>
            <Footer />
        </div>
    );
};

export default Disclaimer;
