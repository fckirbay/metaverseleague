import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import { useMoralis } from "react-moralis";

const Fixture = () => {
  const { Moralis } = useMoralis();

  const [players, setPlayers] = useState([]);
  const [playerName, setPlayerName] = useState("");

  const [fixture, setFixture] = useState([]);

  async function getPlayers() {
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    if (playerName) {
      query.fullText("long_name", playerName);
    }
    query.descending("market_value");
    query.limit(30);
    const results = await query.find({ useMasterKey: true });
    setPlayers(results);
  }

  useEffect(() => {
    //getPlayers();
  }, []);

  /*<i className="icon-svg icon-arrow-board"></i> Hedef tahtası <br />
            <i className="icon-svg icon-calendar"></i> Takvim <br />
            <i className="icon-svg icon-corner-flag"></i> korner bayrak <br />
            <i className="icon-svg icon-crosshair"></i> <br />
            <i className="icon-svg icon-in"></i> Oyuncu giriş <br />
            <i className="icon-svg icon-injury"></i> Sakatlık <br />
            <i className="icon-svg icon-keepers-glove"></i> Kaleci eldiveni <br />
            <i className="icon-svg icon-map-pin"></i> Harita pin <br />
            <i className="icon-svg icon-offside-flag"></i> Ofsayt bayrağı <br />
            <i className="icon-svg icon-out"></i> Oyuncu çıkış <br />
            <i className="icon-svg icon-person"></i> Kişi <br />
            <i className="icon-svg icon-red-card"></i> Kırmızı kart <br />
            <i className="icon-svg icon-shirt"></i> Tshirt <br />
            <i className="icon-svg icon-shirt-alt"></i> Tshirt <br />
            <i className="icon-svg icon-smile"></i> Gülücük <br />
            <i className="icon-svg icon-soccer-ball"></i> Futbol topu <br />
            <i className="icon-svg icon-soccer-ball-missed-penalty"></i> Kaçan penaltı <br />
            <i className="icon-svg icon-soccer-ball-own-goal"></i> Kendi kalesine gol <br />
            <i className="icon-svg icon-soccer-ball-penalty"></i> Penaltı <br />
            <i className="icon-svg icon-soccer-gate"></i> Kapı <br />
            <i className="icon-svg icon-soccer-shoe"></i> Krampon <br />
            <i className="icon-svg icon-soccer-shots"></i> Şutlar <br />
            <i className="icon-svg icon-stopwatch"></i> Maç saati <br />
            <i className="icon-svg icon-substitution"></i> Oyuncu değişikliği <br />
            <i className="icon-svg icon-trophy"></i> Kupa <br />
            <i className="icon-svg icon-trophy-new"></i> Kupa yeni <br />
            <i className="icon-svg icon-whistle"></i> Düdük <br />
            <i className="icon-svg icon-yellow-card"></i> Sarı kart <br />*/

  return (
    <div className="App">
      <Header />
      
      <div className="site-content">
        <div className="container">
          <div className="card card--has-table">
            <div className="card__header">
              <h4>Fixture and Tickets</h4>
            </div>
            <div className="card__content">
            

              <div className="table-responsive">
                <table className="table table-hover team-schedule team-schedule--full">
                  <thead>
                    <tr>
                      <th className="team-schedule__date">Date</th>
                      <th className="team-schedule__versus">Home</th>
                      <th className="team-schedule__versus-away">Away</th>
                      <th className="team-schedule__time">Time</th>
                      <th className="team-schedule__compet">Competition</th>
                      <th className="team-schedule__venue">Venue</th>
                      <th className="team-schedule__tickets">Tickets</th>
                    </tr>
                  </thead>
                  <tbody>
                    {fixture.length == 0 &&
                      <tr>
                        <td className="game-player-result__date no-results-found" colSpan="7">Coming soon...</td>
                      </tr>
                    }
                    {fixture.map((data, index) => (
                    <tr>
                      <td className="team-schedule__date">Saturday, Mar 24</td>
                      <td className="team-schedule__versus">
                        <div className="team-meta">
                          <figure className="team-meta__logo">
                            <img
                              src="assets/images/samples/logos/lucky_clovers_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="team-meta__info">
                            <h6 className="team-meta__name">Lucky Clovers</h6>
                            <span className="team-meta__place">
                              St. Patrick’s Institute
                            </span>
                          </div>
                        </div>
                      </td>
                      <td className="team-schedule__status">
                        <div className="widget-results__team widget-results__team--second">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/soccer/logos/alchemists_s_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              Alchemists
                            </h5>
                            <span className="widget-results__team-info">
                              Eric Bros School
                            </span>
                          </div>
                        </div>
                      </td>
                      <td className="team-schedule__time">9:00PM EST</td>
                      <td className="team-schedule__compet">
                        West Bay Playoffs 2018
                      </td>
                      <td className="team-schedule__venue">
                        Madison Cube Stadium
                      </td>
                      <td className="team-schedule__tickets">
                        <a
                          href="#"
                          className="btn btn-xs btn-default btn-outline btn-block"
                        >
                          Buy Ticket
                        </a>
                      </td>
                    </tr>))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="row" style={{justifyContent:"center"}}>
            <Link to="/transfer-market" className="btn btn-primary">
              Create Your Team
            </Link>{" "}
          </div>
          
          {fixture.length > 10 &&
          <nav className="team-pagination" aria-label="Team navigation">
            <ul className="pagination pagination--condensed pagination--lg justify-content-center">
              <li className="page-item active">
                <a className="page-link" href="#">
                  1
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  2
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  3
                </a>
              </li>
              <li className="page-item">
                <span className="page-link">...</span>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  16
                </a>
              </li>
            </ul>
          </nav>}
        </div>
          </div>
      <Footer />
    </div>
  );
};

export default Fixture;
