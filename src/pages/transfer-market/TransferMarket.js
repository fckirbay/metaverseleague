import React, { useState, useEffect } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import PlayerCard from "../../components/shared/PlayerCard";
import { useMoralis } from "react-moralis";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";
import InfiniteScroll from 'react-infinite-scroll-component';

const TransferMarket = () => {
  const { Moralis } = useMoralis();
  
  const [players, setPlayers] = useState([]);
  const [playerName, setPlayerName] = useState('');
  const [statusChoice, setStatusChoice] = useState(false);
  const [positionChoice, setPositionChoice] = useState(false);
  const [orderChoice, setOrderChoice] = useState(false);
  const [minValue, setMinValue] = useState(null);
  const [maxValue, setMaxValue] = useState(null);
  const [hasMore, setHasMore] = useState(true);

  const [selectedStatus, setSelectedStatus] = useState(0);
  const [selectedPosition, setSelectedPosition] = useState(0);
  const [selectedOrder, setSelectedOrder] = useState(4);

  const statusList = [
    { "id": 0, "title": "All Players" },
    { "id": 1, "title": "On Sale" },
    { "id": 2, "title": "Available for Loan" },
  ];

  const positionList = [
    { "id": 0, "title": "All Positions" },
    { "id": 1, "title": "Goalkeeper" },
    { "id": 2, "title": "Defender" },
    { "id": 3, "title": "Midfielder" },
    { "id": 4, "title": "Striker" },
  ];

  const orderList = [
    { "id": 0, "title": "Market Value Desc" },
    { "id": 1, "title": "Market Value Asc" },
    { "id": 2, "title": "Age Desc" },
    { "id": 3, "title": "Age Asc" },
    { "id": 4, "title": "Overall" }
  ];

  async function getPlayers(filter = false) {

    if(filter) {
      setPlayers([]);
    }
    
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    if (playerName) {
      query.fullText("long_name", playerName);
    }
    switch (selectedStatus) {
      case 1:
        query.equalTo("on_sale", true);
        break;
      case 2:
        query.equalTo("open_loan", true);
        break;
      default:
        break;
    }
    switch (selectedPosition) {
      case 1:
        query.equalTo("positions", "GK");
        break;
      case 2:
        query.containedIn("positions", ["RWB", "RB", "CB", "LB", "LWB"]);
        break;
      case 3:
        query.containedIn("positions", ["CDM", "RM", "CM", "LM", "CAM"]);
        break;
      case 4:
        query.containedIn("positions", ["RF", "CF", "LF", "RW", "ST", "LW"])
        break;
      default:
        break;
    }
    if (minValue) {
      query.greaterThanOrEqualTo("market_value", parseInt(minValue) * 10000);
    }
    if (maxValue) {
      query.lessThanOrEqualTo("market_value", parseInt(maxValue) * 10000);
    }
    switch (selectedOrder) {
      case 0:
        query.descending("market_value");
        break;
      case 1:
        query.ascending("market_value");
        break;
      case 2:
        query.descending("age");
        break;
      case 3:
        query.ascending("age");
        break;
      case 4:
        query.descending("overall");
        break;
      default:
        break;
    }
    query.limit(30);
    if(!filter) {
      query.skip(players.length);
    }
    const results = await query.find({ useMasterKey: true });
    if(results.length < 30) {
      setHasMore(false);
    }
    setPlayers(players => [...players, ...results]);

  }

  async function updatePrices() {
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    query.equalTo("ethValue", null)
    query.limit(1000);
    query.descending("market_value");
    const results = await query.find();

    results.map((player, index) => {
      player.set("ethValue", player.get("market_value") / 20000000);
      player.save(null).then(
        (res) => {
          console.log("ok");
        },
        (error) => {
          console.log("no");
        }
      );
    })
  }

  async function removeZeros() {
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    query.equalTo("ethValue", 0)
    const results = await query.find();

    results.map((player, index) => {

      if(player.get("overall") >= 50 && player.get("overall") < 60) {
        player.set("market_value", 600000);
        player.set("ethValue", 600000 / 20000000);
      } else if(player.get("overall") >= 60 && player.get("overall") < 70) {
        player.set("market_value", 800000);
        player.set("ethValue", 800000 / 20000000);
      } else if(player.get("overall") >= 70 && player.get("overall") < 80) {
        player.set("market_value", 1000000);
        player.set("ethValue", 1000000 / 20000000);
      } else if(player.get("overall") >= 80) {
        player.set("market_value", 2000000);
        player.set("ethValue", 2000000 / 20000000);
      }
      player.save(null).then(
        (res) => {
          console.log("ok");
        },
        (error) => {
          console.log("no");
        }
      );
    })
  }

  useEffect(() => {
    getPlayers();
    //updatePrices();
    //removeZeros();
  }, []);

  return (
    <div className="App">
      <Header />
      <div className="post-filter">
        <div className="container">
          <div className="post-filter__form clearfix">
            <div className="post-filter__select">
              <label className="post-filter__label">Status</label>{" "}
              <div className={`cs-select cs-skin-border ${statusChoice ? "cs-active" : ""}`} tabIndex="0" onClick={() => { setStatusChoice(!statusChoice); setPositionChoice(false); setOrderChoice(false) }}>
                <span className="cs-placeholder">{statusList[selectedStatus].title}</span>
                <div className="cs-options">
                  <ul>
                    {statusList.map((data, index) => (
                      selectedStatus !== data.id && <li data-option="" data-value={data.id} onClick={() => { setSelectedStatus(data.id) }} key={`status-${data.id}`}>
                        <span>{data.title}</span>
                      </li>
                    ))}
                  </ul>
                </div>
                {/*disabled="disabled" defaultValue="selected"*/}
                <select className="cs-select cs-skin-border">
                  {statusList.map((data, index) => (
                    selectedStatus !== data.id && <option value={data.id} key={`status-option-${data.id}`}>
                      {data.title}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="post-filter__select">
              <label className="post-filter__label">Position</label>{" "}
              <div className={`cs-select cs-skin-border ${positionChoice ? "cs-active" : ""}`} tabIndex="0" onClick={() => { setPositionChoice(!positionChoice); setStatusChoice(false); setOrderChoice(false) }}>
                <span className="cs-placeholder">{positionList[selectedPosition].title}</span>
                <div className="cs-options">
                  <ul>
                    {positionList.map((data, index) => (
                      selectedPosition !== data.id && <li data-option="" data-value={data.id} onClick={() => { setSelectedPosition(data.id) }} key={`position-${data.id}`}>
                        <span>{data.title}</span>
                      </li>
                    ))}
                  </ul>
                </div>
                {/*disabled="disabled" defaultValue="selected"*/}
                <select className="cs-select cs-skin-border">
                  {positionList.map((data, index) => (
                    selectedPosition !== data.id && <option value={data.id} key={`position-option-${data.id}`}>
                      {data.title}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="post-filter__select">
              <label className="post-filter__label">Order</label>{" "}
              <div className={`cs-select cs-skin-border ${orderChoice ? "cs-active" : ""}`} tabIndex="0" onClick={() => { setOrderChoice(!orderChoice); setStatusChoice(false); setPositionChoice(false) }}>
                <span className="cs-placeholder">{orderList[selectedOrder].title}</span>
                <div className="cs-options">
                  <ul>
                    {orderList.map((data, index) => (
                      selectedOrder !== data.id && <li data-option="" data-value={data.id} onClick={() => { setSelectedOrder(data.id) }} key={`order-${data.id}`}>
                        <span>{data.title}</span>
                      </li>
                    ))}
                  </ul>
                </div>
                {/*disabled="disabled" defaultValue="selected"*/}
                <select className="cs-select cs-skin-border">
                  {orderList.map((data, index) => (
                    selectedOrder !== data.id && <option value={data.id} key={`order-option-${data.id}`}>
                      {data.title}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="post-filter__select player-name-filter">
              <label className="post-filter__label text-box-filter">Player Name</label>{" "}
              <div className="cs-select cs-skin-border" tabIndex="0">
                <input
                  type="text"
                  className="form-control search-player-name-input"
                  name="account-email"
                  id="account-email"
                  placeholder="Player Name"
                  value={playerName}
                  onChange={(event) => { setPlayerName(event.target.value) }}
                />
              </div>
            </div>
            <div className="post-filter__select player-name-filter">
              <label className="post-filter__label text-box-filter">Min Value</label>{" "}
              <div className="cs-select cs-skin-border" tabIndex="0">
                <input
                  type="text"
                  className="form-control search-player-name-input"
                  name="account-email"
                  id="account-email"
                  placeholder="Min Value"
                  value={minValue}
                  onChange={(event) => { setMinValue(event.target.value) }}
                />
              </div>
            </div>
            <div className="post-filter__select">
              <label className="post-filter__label text-box-filter">Max Value</label>{" "}
              <div className="cs-select cs-skin-border" tabIndex="0">
                <input
                  type="text"
                  className="form-control search-player-name-input"
                  name="account-email"
                  id="account-email"
                  placeholder="Max Value"
                  value={maxValue}
                  onChange={(event) => { setMaxValue(event.target.value) }}
                />
              </div>
            </div>
            <div className="post-filter__submit">
              <button
                type="button"
                className="btn btn-default btn-lg btn-block"
                onClick={() => getPlayers(true)}
              >
                Filter Players
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="site-content">
        <div className="container">
          
            <InfiniteScroll
              dataLength={players.length} //This is important field to render the next data
              next={() => getPlayers()}
              hasMore={hasMore}
              loader={<div><h4>Loading...</h4></div>}
              /*endMessage={
                <p style={{ textAlign: 'center' }}>
                  <b>Yay! You have seen it all</b>
                </p>
              }*/
            >
              <div className="row">
              {players.map((item, index) => {
                return <PlayerCard key={index} data={item} />
              })}
              </div>
            </InfiniteScroll>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default TransferMarket;
