import React from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

const StockMarket = () => {

    return (
        <div className="App">
            <Header />
            <div className="site-content">
                <div className="container">
                    <div className="card card--has-table">
                        <div className="card__header">
                            <h4>Stock Market</h4>
                        </div>
                        <div className="card__content">
                            <div className="table-responsive">
                                <table className="table table-hover team-schedule team-schedule--full">
                                    <tbody>
                                        <tr>
                                            <td className="game-player-result__date no-results-found" colSpan="7">Coming soon...</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default StockMarket;
