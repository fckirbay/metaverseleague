import React, { useRef, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import PlayerCard from "../../components/shared/PlayerCard";
import Slider from "../../components/Slider";
import { useMoralis } from "react-moralis";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";

const Home = () => {
  const { Moralis } = useMoralis();

  const [players, setPlayers] = useState([]);
  const [filterTab, setFilterTab] = useState(0);

  async function getPlayers(type = 0) {
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    if (type == 1) {
      query.equalTo("on_sale", true);
    } else if (type == 2) {
      query.equalTo("open_loan", true);
    }
    query.descending("market_value");
    query.limit(30);
    const results = await query.find({ useMasterKey: true });
    setPlayers(results);
  }

  useEffect(() => {
    getPlayers();
  }, []);

  return (
    <div className="App">
      <Header />
      <Slider />
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <aside className="widget widget--sidebar card card--has-table widget-team-stats">
                <div className="widget__title card__header">
                  <h4>Roadmap</h4>
                </div>
                <div className="widget__content card__content">
                  <ul className="team-stats-box">
                    <li className="team-stats__item team-stats__item--clean">
                      <div className="team-stats__value">2022 Q3</div>
                      <div className="team-stats__label">Presale Opportunities</div>
                      <div className="team-stats__label">Player Presale</div>
                      <div className="team-stats__label">Coach Presale</div>
                      <div className="team-stats__label">Transfer Negotiations</div>
                    </li>
                    <li className="team-stats__item team-stats__item--clean">
                      <div className="team-stats__value">2022 Q4</div>
                      <div className="team-stats__label">Player Loan</div>
                      <div className="team-stats__label">Friendly Matches (Comments)</div>
                      <div className="team-stats__label">Stadium Constuctions</div>
                      <div className="team-stats__label">Stock Sales</div>
                    </li>
                    <li className="team-stats__item team-stats__item--clean">
                      <div className="team-stats__value">2023 Q1</div>
                      <div className="team-stats__label">Prem-match Bets</div>
                      <div className="team-stats__label">Buy / Sell Stocks</div>
                      <div className="team-stats__label">Rewareded Tournaments (Comments)</div>
                      <div className="team-stats__label">Custom Jersey Design</div>
                      <div className="team-stats__label">Trainings & Team Improving</div>
                    </li>
                    <li className="team-stats__item team-stats__item--clean">
                      <div className="team-stats__value">2023</div>
                      <div className="team-stats__label">Friendly Matches (2D)</div>
                      <div className="team-stats__label">Rewareded Tournaments (2D)</div>
                      <div className="team-stats__label">Live Betting</div>
                      <div className="team-stats__label">Stadium Tickets</div>
                      <div className="team-stats__label">Additional Revenue Models</div>
                    </li>
                  </ul>
                </div>
              </aside>
            </div>
          </div>
          <div className="row">
            <div className="content col-lg-12">
              <div className="card card--clean">
                <header className="card__header card__header--has-filter">
                  <h4>Transfer Market</h4>
                  <ul className="category-filter category-filter--featured">
                    <li
                      className="category-filter__item"
                      onClick={() => {
                        getPlayers();
                        setFilterTab(0);
                      }}
                    >
                      <a
                        className={`category-filter__link ${
                          filterTab == 0
                            ? "category-filter__link--reset category-filter__link--active"
                            : ""
                        }`}
                      >
                        All
                      </a>
                    </li>
                    <li
                      className="category-filter__item"
                      onClick={() => {
                        getPlayers(1);
                        setFilterTab(1);
                      }}
                    >
                      <a
                        className={`category-filter__link ${
                          filterTab == 1
                            ? "category-filter__link--reset category-filter__link--active"
                            : ""
                        }`}
                        data-category="posts__item--category-1"
                      >
                        On Sale
                      </a>
                    </li>
                    <li
                      className="category-filter__item"
                      onClick={() => {
                        getPlayers(2);
                        setFilterTab(2);
                      }}
                    >
                      <a
                        className={`category-filter__link ${
                          filterTab == 2
                            ? "category-filter__link--reset category-filter__link--active"
                            : ""
                        }`}
                        data-category="posts__item--category-3"
                      >
                        Available for Loan
                      </a>
                    </li>
                  </ul>
                </header>
              </div>
            </div>
          </div>

          <div className="row">
            {players.map((item, index) => (
              <PlayerCard key={index} data={item} />
            ))}
          </div>
          <div className="row show-all-button">
            <Link to="/transfer-market" className="btn btn-primary">
              Show All
            </Link>
          </div>

          {/*<div className="row">
                    <div className="content col-lg-8">
                        <div className="card card--clean">
                            <header className="card__header card__header--has-filter">
                                <h4>Featured News</h4>
                                <ul className="category-filter category-filter--featured">
                                    <li className="category-filter__item"><Link to="#"
                                        className="category-filter__link category-filter__link--reset category-filter__link--active">All</Link>
                                    </li>
                                    <li className="category-filter__item"><Link to="#" className="category-filter__link"
                                        data-category="posts__item--category-1">The Team</Link></li>
                                    <li className="category-filter__item"><Link to="#" className="category-filter__link"
                                        data-category="posts__item--category-3">The League</Link></li>
                                    <li className="category-filter__item"><Link to="#" className="category-filter__link"
                                        data-category="posts__item--category-2">Injuries</Link></li>
                                </ul>
                            </header>
                            <div className="card__content">
                                <div className="slick posts posts--slider-featured posts-slider posts-slider--center">
                                    <Swiper
                                        slidesPerView={1}
                                        spaceBetween={30}
                                        pagination={{
                                            clickable: true,
                                        }}
                                        navigation={false}
                                        modules={[Pagination, Navigation, Autoplay]}
                                        className="mySwiper"
                                        autoplay={{ delay: 3000, disableOnInteraction: false, reverseDirection: false, waitForTransition: true }}
                                    >
                                        <SwiperSlide>
                                            <div className="posts__item posts__item--category-1"><Link to="#"
                                                className="posts__link-wrapper">
                                                <figure className="posts__thumb"><img
                                                    src="assets/images/soccer/samples/post-slide7.jpg" alt="" /></figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        Team</span></div>
                                                    <h3 className="posts__title">The Planettroters will <span
                                                        className="posts__title-higlight">perform this May 4th</span> at
                                                        Madison Cube</h3>
                                                    <div className="post-author">
                                                        <figure className="post-author__avatar"><img
                                                            src="assets/images/samples/avatar-4.jpg"
                                                            alt="Post Author Avatar" /></figure>
                                                        <div className="post-author__info">
                                                            <h4 className="post-author__name">Jesse Stevens</h4><time
                                                                dateTime="2017-08-28" className="posts__date">August 28th,
                                                                2018</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link></div>
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <div className="posts__item posts__item--category-3"><Link to="#"
                                                className="posts__link-wrapper">
                                                <figure className="posts__thumb"><img
                                                    src="assets/images/samples/post-slide2.jpg" alt="" /></figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        League</span></div>
                                                    <h3 className="posts__title">New York is preparing all <span
                                                        className="posts__title-higlight">for the Playoffs Final</span> next
                                                        December</h3>
                                                    <div className="post-author">
                                                        <figure className="post-author__avatar"><img
                                                            src="assets/images/samples/avatar-1.jpg"
                                                            alt="Post Author Avatar" /></figure>
                                                        <div className="post-author__info">
                                                            <h4 className="post-author__name">James Spiegel</h4><time
                                                                dateTime="2017-08-28" className="posts__date">June 28th,
                                                                2018</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link></div>
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <div className="posts__item posts__item--category-1"><Link to="#"
                                                className="posts__link-wrapper">
                                                <figure className="posts__thumb"><img
                                                    src="assets/images/samples/post-slide3.jpg" alt="" /></figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        Team</span></div>
                                                    <h3 className="posts__title">The new eco friendly <span
                                                        className="posts__title-higlight">stadium won a Leafy</span> Award
                                                        in 2016</h3>
                                                    <div className="post-author">
                                                        <figure className="post-author__avatar"><img
                                                            src="assets/images/samples/avatar-1.jpg"
                                                            alt="Post Author Avatar" /></figure>
                                                        <div className="post-author__info">
                                                            <h4 className="post-author__name">James Spiegel</h4><time
                                                                dateTime="2017-08-28" className="posts__date">June 28th,
                                                                2018</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link></div>
                                        </SwiperSlide>
                                        <SwiperSlide>

                                            <div className="posts__item posts__item--category-3"><Link to="#"
                                                className="posts__link-wrapper">
                                                <figure className="posts__thumb"><img
                                                    src="assets/images/samples/post-slide4.jpg" alt="" /></figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        League</span></div>
                                                    <h3 className="posts__title">New York Islanders are <span
                                                        className="posts__title-higlight">now flying to California</span>
                                                        for the big game</h3>
                                                    <div className="post-author">
                                                        <figure className="post-author__avatar"><img
                                                            src="assets/images/samples/avatar-1.jpg"
                                                            alt="Post Author Avatar" /></figure>
                                                        <div className="post-author__info">
                                                            <h4 className="post-author__name">James Spiegel</h4><time
                                                                dateTime="2017-08-28" className="posts__date">June 28th,
                                                                2018</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link></div>
                                        </SwiperSlide>
                                        <SwiperSlide>

                                            <div className="posts__item posts__item--category-3"><Link to="#"
                                                className="posts__link-wrapper">
                                                <figure className="posts__thumb"><img
                                                    src="assets/images/samples/post-slide6.jpg" alt="" /></figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        League</span></div>
                                                    <h3 className="posts__title">Jeremy Rittersen was <span
                                                        className="posts__title-higlight">called to be</span> in the
                                                        National Team</h3>
                                                    <div className="post-author">
                                                        <figure className="post-author__avatar"><img
                                                            src="assets/images/samples/avatar-2.jpg"
                                                            alt="Post Author Avatar" /></figure>
                                                        <div className="post-author__info">
                                                            <h4 className="post-author__name">Jessica Hoops</h4><time
                                                                dateTime="2017-08-28" className="posts__date">May 24th,
                                                                2018</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link></div>
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <div className="posts__item posts__item--category-2"><Link to="#"
                                                className="posts__link-wrapper">
                                                <figure className="posts__thumb"><img
                                                    src="assets/images/samples/post-slide5.jpg" alt="" /></figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span
                                                        className="label posts__cat-label">Injuries</span></div>
                                                    <h3 className="posts__title">Jessica Valentine has a <span
                                                        className="posts__title-higlight">sprained ankle</span> and won't be
                                                        in the Final</h3>
                                                    <div className="post-author">
                                                        <figure className="post-author__avatar"><img
                                                            src="assets/images/samples/avatar-4.jpg"
                                                            alt="Post Author Avatar" /></figure>
                                                        <div className="post-author__info">
                                                            <h4 className="post-author__name">Jesse Stevens</h4><time
                                                                dateTime="2017-08-28" className="posts__date">April 12th,
                                                                2018</time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link></div>
                                        </SwiperSlide>
                                    </Swiper>
                                </div>
                            </div>
                        </div>
                        <div className="posts posts--cards post-grid row">
                            <div className="post-grid__item col-sm-6">
                                <div className="posts__item posts__item--card posts__item--category-1 card">
                                    <figure className="posts__thumb">
                                        <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                        </div><Link to="#"><img src="assets/images/soccer/samples/_soccer_post-img3.jpg"
                                            alt="" /></Link>
                                    </figure>
                                    <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link> <time
                                        dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                        <h6 className="posts__title"><Link to="#">Brian Kingster saw the red card after a
                                            kick against Jason T.</Link></h6>
                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet, consectetur adipisi nel
                                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                            enim ad mini veniam, quis nostrud en derum sum laborem.</div>
                                    </div>
                                    <footer className="posts__footer card__footer">
                                        <div className="post-author">
                                            <figure className="post-author__avatar"><img
                                                src="assets/images/samples/avatar-1.jpg" alt="Post Author Avatar" />
                                            </figure>
                                            <div className="post-author__info">
                                                <h4 className="post-author__name">James Spiegel</h4>
                                            </div>
                                        </div>
                                        <ul className="post__meta meta">
                                            <li className="meta__item meta__item--views">2369</li>
                                            <li className="meta__item meta__item--likes"><Link to="#"><i
                                                className="meta-like icon-heart"></i> 530</Link></li>
                                            <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                        </ul>
                                    </footer>
                                </div>
                            </div>
                            <div className="post-grid__item col-sm-6">
                                <div className="posts__item posts__item--card posts__item--category-1 card">
                                    <figure className="posts__thumb">
                                        <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                        </div><Link to="#"><img src="assets/images/soccer/samples/_soccer_post-img1.jpg"
                                            alt="" /></Link>
                                    </figure>
                                    <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link> <time
                                        dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                        <h6 className="posts__title"><Link to="#">The new eco friendly stadium won a Leafy
                                            Award in 2016</Link></h6>
                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet, consectetur adipisi nel
                                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                            enim ad mini veniam, quis nostrud en derum sum laborem.</div>
                                    </div>
                                    <footer className="posts__footer card__footer">
                                        <div className="post-author">
                                            <figure className="post-author__avatar"><img
                                                src="assets/images/samples/avatar-2.jpg" alt="Post Author Avatar" />
                                            </figure>
                                            <div className="post-author__info">
                                                <h4 className="post-author__name">Jessica Hoops</h4>
                                            </div>
                                        </div>
                                        <ul className="post__meta meta">
                                            <li className="meta__item meta__item--views">2369</li>
                                            <li className="meta__item meta__item--likes"><Link to="#"><i
                                                className="meta-like icon-heart"></i> 530</Link></li>
                                            <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                        </ul>
                                    </footer>
                                </div>
                            </div>
                            <div className="post-grid__item col-sm-6">
                                <div className="posts__item posts__item--card posts__item--category-1 card">
                                    <figure className="posts__thumb">
                                        <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                        </div><Link to="#"><img src="assets/images/samples/post-img3.jpg" alt="" /></Link>
                                    </figure>
                                    <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link> <time
                                        dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                        <h6 className="posts__title"><Link to="#">The team is taking a summer vacation on
                                            Woody Valley</Link></h6>
                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet, consectetur adipisi nel
                                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                            enim ad mini veniam, quis nostrud en derum sum laborem.</div>
                                    </div>
                                    <footer className="posts__footer card__footer">
                                        <div className="post-author">
                                            <figure className="post-author__avatar"><img
                                                src="assets/images/samples/avatar-2.jpg" alt="Post Author Avatar" />
                                            </figure>
                                            <div className="post-author__info">
                                                <h4 className="post-author__name">Jessica Hoops</h4>
                                            </div>
                                        </div>
                                        <ul className="post__meta meta">
                                            <li className="meta__item meta__item--views">2369</li>
                                            <li className="meta__item meta__item--likes"><Link to="#"><i
                                                className="meta-like icon-heart"></i> 530</Link></li>
                                            <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                        </ul>
                                    </footer>
                                </div>
                            </div>
                            <div className="post-grid__item col-sm-6">
                                <div className="posts__item posts__item--card posts__item--category-1 card">
                                    <figure className="posts__thumb">
                                        <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                        </div><Link to="#"><img src="assets/images/samples/post-img5.jpg" alt="" /></Link>
                                    </figure>
                                    <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link> <time
                                        dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                        <h6 className="posts__title"><Link to="#">The Alchemists team is appearing in L.A.
                                            beach for chartiy</Link></h6>
                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet, consectetur adipisi nel
                                            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                            enim ad mini veniam, quis nostrud en derum sum laborem.</div>
                                    </div>
                                    <footer className="posts__footer card__footer">
                                        <div className="post-author">
                                            <figure className="post-author__avatar"><img
                                                src="assets/images/samples/avatar-1.jpg" alt="Post Author Avatar" />
                                            </figure>
                                            <div className="post-author__info">
                                                <h4 className="post-author__name">James Spiegel</h4>
                                            </div>
                                        </div>
                                        <ul className="post__meta meta">
                                            <li className="meta__item meta__item--views">2369</li>
                                            <li className="meta__item meta__item--likes"><Link to="#"><i
                                                className="meta-like icon-heart"></i> 530</Link></li>
                                            <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                        </ul>
                                    </footer>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="card">
                                    <div className="card__content">
                                        <ul className="posts posts--simple-list">
                                            <li className="posts__item posts__item--category-1">
                                                <figure className="posts__thumb"><Link to="#"><img
                                                    src="assets/images/samples/post-img22-xs.jpg" alt="" /></Link>
                                                </figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        Team</span></div>
                                                    <h6 className="posts__title"><Link to="#">Brian Kingster was penalized
                                                        with four games out</Link></h6><time dateTime="2016-08-23"
                                                            className="posts__date">August 28th, 2018</time>
                                                </div>
                                                <div className="posts__excerpt posts__excerpt--space-sm">Lorem ipsum dolor
                                                    sit amet, consectetur adipisi ng elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua.</div>
                                            </li>
                                            <li className="posts__item posts__item--category-1">
                                                <figure className="posts__thumb"><Link to="#"><img
                                                    src="assets/images/samples/post-img23-xs.jpg" alt="" /></Link>
                                                </figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        Team</span></div>
                                                    <h6 className="posts__title"><Link to="#">The next game will be played at
                                                        California Beach</Link></h6><time dateTime="2016-08-23"
                                                            className="posts__date">August 28th, 2018</time>
                                                </div>
                                                <div className="posts__excerpt posts__excerpt--space-sm">Lorem ipsum dolor
                                                    sit amet, consectetur adipisi ng elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua.</div>
                                            </li>
                                            <li className="posts__item posts__item--category-3">
                                                <figure className="posts__thumb"><Link to="#"><img
                                                    src="assets/images/samples/post-img20-xs.jpg" alt="" /></Link>
                                                </figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        League</span></div>
                                                    <h6 className="posts__title"><Link to="#">The Alchemists need two win the
                                                        next two games</Link></h6><time dateTime="2016-08-23"
                                                            className="posts__date">August 28th, 2018</time>
                                                </div>
                                                <div className="posts__excerpt posts__excerpt--space-sm">Lorem ipsum dolor
                                                    sit amet, consectetur adipisi ng elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua.</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="card">
                                    <div className="card__content">
                                        <ul className="posts posts--simple-list">
                                            <li className="posts__item posts__item--category-1">
                                                <figure className="posts__thumb"><Link to="#"><img
                                                    src="assets/images/samples/post-img24-xs.jpg" alt="" /></Link>
                                                </figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        Team</span></div>
                                                    <h6 className="posts__title"><Link to="#">he team will make a small
                                                        vacation to the Caribbean</Link></h6><time
                                                            dateTime="2016-08-23" className="posts__date">August 28th,
                                                        2018</time>
                                                </div>
                                                <div className="posts__excerpt posts__excerpt--space-sm">Lorem ipsum dolor
                                                    sit amet, consectetur adipisi ng elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua.</div>
                                            </li>
                                            <li className="posts__item posts__item--category-2">
                                                <figure className="posts__thumb"><Link to="#"><img
                                                    src="assets/images/samples/post-img18-xs.jpg" alt="" /></Link>
                                                </figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span
                                                        className="label posts__cat-label">Injuries</span></div>
                                                    <h6 className="posts__title"><Link to="#">Jenny Jackson won&#x27;t be
                                                        able to play next game</Link></h6><time dateTime="2016-08-23"
                                                            className="posts__date">August 28th, 2018</time>
                                                </div>
                                                <div className="posts__excerpt posts__excerpt--space-sm">Lorem ipsum dolor
                                                    sit amet, consectetur adipisi ng elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua.</div>
                                            </li>
                                            <li className="posts__item posts__item--category-3">
                                                <figure className="posts__thumb"><Link to="#"><img
                                                    src="assets/images/samples/post-img4-xs.jpg" alt="" /></Link>
                                                </figure>
                                                <div className="posts__inner">
                                                    <div className="posts__cat"><span className="label posts__cat-label">The
                                                        League</span></div>
                                                    <h6 className="posts__title"><Link to="#">The team is starting a new
                                                        power breakfast regimen</Link></h6><time dateTime="2016-08-23"
                                                            className="posts__date">August 28th, 2018</time>
                                                </div>
                                                <div className="posts__excerpt posts__excerpt--space-sm">Lorem ipsum dolor
                                                    sit amet, consectetur adipisi ng elit, sed do eiusmod tempor
                                                    incididunt ut labore et dolore magna aliqua.</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="main-news-banner main-news-banner--soccer-ball">
                            <div className="main-news-banner__inner">
                                <div className="posts posts--simple-list posts--simple-list--xlg">
                                    <div className="posts__item posts__item--category-1">
                                        <div className="posts__inner">
                                            <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                            </div>
                                            <h6 className="posts__title"><Link to="#">Today was unveiled the <span
                                                className="main-news-banner__highlight">New Football</span> for the
                                                league</Link></h6><time dateTime="2016-08-23"
                                                    className="posts__date">August 23rd, 2016</time>
                                            <div className="posts__excerpt">Lorem ipsum dolor sit amet, consectetur adipisi
                                                nel elit, sed do eiusmod tempor incididunt ut labore et dolore.</div>
                                            <div className="posts__more"><Link to="#"
                                                className="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed">Read
                                                More <i className="fas fa-plus text-primary"></i></Link></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card card--clean">
                            <header className="card__header card__header--has-btn">
                                <h4>Latest News</h4><Link to="#"
                                    className="btn btn-default btn-outline btn-xs card-header__button">See All Posts</Link>
                            </header>
                            <div className="card__content">
                                <div className="posts posts--cards post-grid row">
                                    <div className="post-grid__item col-sm-6">
                                        <div className="posts__item posts__item--card posts__item--category-2 card">
                                            <figure className="posts__thumb">
                                                <div className="posts__cat"><span
                                                    className="label posts__cat-label">Injuries</span></div><a
                                                        href="#"><img src="assets/images/samples/post-img1.jpg" alt="" /></Link>
                                            </figure>
                                            <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link>
                                                <time dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                                <h6 className="posts__title"><Link to="#">Franklin Stevens has a knee
                                                    fracture and is gonna be out</Link></h6>
                                            </div>
                                            <footer className="posts__footer card__footer">
                                                <div className="post-author">
                                                    <figure className="post-author__avatar"><img
                                                        src="assets/images/samples/avatar-1.jpg"
                                                        alt="Post Author Avatar" /></figure>
                                                    <div className="post-author__info">
                                                        <h4 className="post-author__name">James Spiegel</h4>
                                                    </div>
                                                </div>
                                                <ul className="post__meta meta">
                                                    <li className="meta__item meta__item--views">2369</li>
                                                    <li className="meta__item meta__item--likes"><Link to="#"><i
                                                        className="meta-like icon-heart"></i> 530</Link></li>
                                                    <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                    <div className="post-grid__item col-sm-6">
                                        <div className="posts__item posts__item--card posts__item--category-1 card">
                                            <figure className="posts__thumb">
                                                <div className="posts__cat"><span className="label posts__cat-label">The
                                                    Team</span></div><Link to="#"><img
                                                        src="assets/images/soccer/samples/_soccer_post-img4.jpg"
                                                        alt="" /></Link>
                                            </figure>
                                            <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link>
                                                <time dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                                <h6 className="posts__title"><Link to="#">Jay Rorks is only 24 points away
                                                    from breaking the record</Link></h6>
                                            </div>
                                            <footer className="posts__footer card__footer">
                                                <div className="post-author">
                                                    <figure className="post-author__avatar"><img
                                                        src="assets/images/samples/avatar-2.jpg"
                                                        alt="Post Author Avatar" /></figure>
                                                    <div className="post-author__info">
                                                        <h4 className="post-author__name">Jessica Hoops</h4>
                                                    </div>
                                                </div>
                                                <ul className="post__meta meta">
                                                    <li className="meta__item meta__item--views">2369</li>
                                                    <li className="meta__item meta__item--likes"><Link to="#"><i
                                                        className="meta-like icon-heart"></i> 530</Link></li>
                                                    <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                    <div className="post-grid__item col-sm-6">
                                        <div className="posts__item posts__item--card posts__item--category-1 card">
                                            <figure className="posts__thumb">
                                                <div className="posts__cat"><span className="label posts__cat-label">The
                                                    Team</span></div><Link to="#"><img
                                                        src="assets/images/samples/post-img13.jpg" alt="" /></Link>
                                            </figure>
                                            <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link>
                                                <time dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                                <h6 className="posts__title"><Link to="#">The team is starting a new power
                                                    breakfast regimen</Link></h6>
                                            </div>
                                            <footer className="posts__footer card__footer">
                                                <div className="post-author">
                                                    <figure className="post-author__avatar"><img
                                                        src="assets/images/samples/avatar-1.jpg"
                                                        alt="Post Author Avatar" /></figure>
                                                    <div className="post-author__info">
                                                        <h4 className="post-author__name">James Spiegel</h4>
                                                    </div>
                                                </div>
                                                <ul className="post__meta meta">
                                                    <li className="meta__item meta__item--views">2369</li>
                                                    <li className="meta__item meta__item--likes"><Link to="#"><i
                                                        className="meta-like icon-heart"></i> 530</Link></li>
                                                    <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                    <div className="post-grid__item col-sm-6">
                                        <div className="posts__item posts__item--card posts__item--category-1 card">
                                            <figure className="posts__thumb">
                                                <div className="posts__cat"><span className="label posts__cat-label">The
                                                    Team</span></div><Link to="#"><img
                                                        src="assets/images/samples/post-img2.jpg" alt="" /></Link>
                                            </figure>
                                            <div className="posts__inner card__content"><Link to="#" className="posts__cta"></Link>
                                                <time dateTime="2016-08-23" className="posts__date">August 28th, 2018</time>
                                                <h6 className="posts__title"><Link to="#">Cheerleader tryouts will start next
                                                    Friday at 5pm</Link></h6>
                                            </div>
                                            <footer className="posts__footer card__footer">
                                                <div className="post-author">
                                                    <figure className="post-author__avatar"><img
                                                        src="assets/images/samples/avatar-1.jpg"
                                                        alt="Post Author Avatar" /></figure>
                                                    <div className="post-author__info">
                                                        <h4 className="post-author__name">James Spiegel</h4>
                                                    </div>
                                                </div>
                                                <ul className="post__meta meta">
                                                    <li className="meta__item meta__item--views">2369</li>
                                                    <li className="meta__item meta__item--likes"><Link to="#"><i
                                                        className="meta-like icon-heart"></i> 530</Link></li>
                                                    <li className="meta__item meta__item--comments"><Link to="#">18</Link></li>
                                                </ul>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav className="post-pagination" aria-label="Blog navigation">
                            <ul className="pagination pagination--condensed pagination--lg justify-content-center">
                                <li className="page-item active"><a className="page-link" href="#">1</Link></li>
                                <li className="page-item"><a className="page-link" href="#">2</Link></li>
                                <li className="page-item"><a className="page-link" href="#">3</Link></li>
                                <li className="page-item"><span className="page-link">...</span></li>
                                <li className="page-item"><a className="page-link" href="#">16</Link></li>
                            </ul>
                        </nav>
                    </div>
                    <div id="sidebar" className="sidebar col-lg-4">
                        <aside className="widget card widget--sidebar widget-standings">
                            <div className="widget__title card__header card__header--has-btn">
                                <h4>West League 2018</h4><Link to="#"
                                    className="btn btn-default btn-outline btn-xs card-header__button">See All Stats</Link>
                            </div>
                            <div className="widget__content card__content">
                                <div className="table-responsive">
                                    <table className="table table-hover table-standings">
                                        <thead>
                                            <tr>
                                                <th>Team Positions</th>
                                                <th>W</th>
                                                <th>L</th>
                                                <th>D</th>
                                                <th>PTS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/pirates_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">L.A Pirates</h6><span
                                                                className="team-meta__place">Bebop Institute</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>36</td>
                                                <td>14</td>
                                                <td>10</td>
                                                <td>118</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/sharks_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">Sharks</h6><span
                                                                className="team-meta__place">Marine College</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>32</td>
                                                <td>20</td>
                                                <td>8</td>
                                                <td>104</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/alchemists_b_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">The Alchemists</h6><span
                                                                className="team-meta__place">Eric Bros School</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>32</td>
                                                <td>21</td>
                                                <td>7</td>
                                                <td>103</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/ocean_kings_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">Ocean Kings</h6><span
                                                                className="team-meta__place">Bay College</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>30</td>
                                                <td>20</td>
                                                <td>10</td>
                                                <td>100</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/red_wings_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">Red Wings</h6><span
                                                                className="team-meta__place">Icarus College</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>28</td>
                                                <td>24</td>
                                                <td>8</td>
                                                <td>92</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/lucky_clovers_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">Lucky Clovers</h6><span
                                                                className="team-meta__place">St. Patrick’s Institute</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>27</td>
                                                <td>24</td>
                                                <td>9</td>
                                                <td>90</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/draconians_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">Draconians</h6><span
                                                                className="team-meta__place">High Rock College</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>25</td>
                                                <td>28</td>
                                                <td>7</td>
                                                <td>82</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div className="team-meta">
                                                        <figure className="team-meta__logo"><img
                                                            src="assets/images/samples/logos/bloody_wave_shield.png"
                                                            alt="" /></figure>
                                                        <div className="team-meta__info">
                                                            <h6 className="team-meta__name">Bloody Wave</h6><span
                                                                className="team-meta__place">Atlantic School</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>6</td>
                                                <td>78</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </aside>
                        <aside className="widget widget--sidebar widget-social widget-social--condensed"><Link to="#"
                            className="btn-social-counter btn-social-counter--facebook" target="_blank">
                            <div className="btn-social-counter__icon"><i className="fab fa-facebook"></i></div>
                            <h6 className="btn-social-counter__title">Like Our Facebook Page</h6><span
                                className="btn-social-counter__count"><span
                                    className="btn-social-counter__count-num"></span> Likes</span> <span
                                        className="btn-social-counter__add-icon"></span>
                        </Link><Link to="#" className="btn-social-counter btn-social-counter--twitter" target="_blank">
                                <div className="btn-social-counter__icon"><i className="fab fa-twitter"></i></div>
                                <h6 className="btn-social-counter__title">Follow Us on Twitter</h6><span
                                    className="btn-social-counter__count"><span
                                        className="btn-social-counter__count-num"></span> Followers</span> <span
                                            className="btn-social-counter__add-icon"></span>
                            </Link><Link to="#" className="btn-social-counter btn-social-counter--rss" target="_blank">
                                <div className="btn-social-counter__icon"><i className="fas fa-rss"></i></div>
                                <h6 className="btn-social-counter__title">Subscribe to Our RSS</h6><span
                                    className="btn-social-counter__count"><span
                                        className="btn-social-counter__count-num">840</span> Subscribers</span> <span
                                            className="btn-social-counter__add-icon"></span>
                            </Link></aside>
                        <aside className="widget widget--sidebar card widget-popular-posts">
                            <div className="widget__title card__header">
                                <h4>Popular News</h4>
                            </div>
                            <div className="widget__content card__content">
                                <ul className="posts posts--simple-list">
                                    <li className="posts__item posts__item--category-2">
                                        <figure className="posts__thumb"><Link to="#"><img
                                            src="assets/images/samples/post-img1-xs.jpg" alt="" /></Link></figure>
                                        <div className="posts__inner">
                                            <div className="posts__cat"><span className="label posts__cat-label">Injuries</span>
                                            </div>
                                            <h6 className="posts__title"><Link to="#">Mark Johnson has a Tibia Fracture and
                                                is gonna be out</Link></h6><time dateTime="2016-08-23"
                                                    className="posts__date">August 23rd, 2018</time>
                                        </div>
                                    </li>
                                    <li className="posts__item posts__item--category-1">
                                        <figure className="posts__thumb"><Link to="#"><img
                                            src="assets/images/samples/post-img2-xs.jpg" alt="" /></Link></figure>
                                        <div className="posts__inner">
                                            <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                            </div>
                                            <h6 className="posts__title"><Link to="#">Jay Rorks is only 24 points away from
                                                breaking the record</Link></h6><time dateTime="2016-08-23"
                                                    className="posts__date">August 22nd, 2018</time>
                                        </div>
                                    </li>
                                    <li className="posts__item posts__item--category-1">
                                        <figure className="posts__thumb"><Link to="#"><img
                                            src="assets/images/samples/post-img3-xs.jpg" alt="" /></Link></figure>
                                        <div className="posts__inner">
                                            <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                            </div>
                                            <h6 className="posts__title"><Link to="#">The new eco friendly stadium won a
                                                Leafy Award in 2016</Link></h6><time dateTime="2016-08-23"
                                                    className="posts__date">June 8th, 2018</time>
                                        </div>
                                    </li>
                                    <li className="posts__item posts__item--category-1">
                                        <figure className="posts__thumb"><Link to="#"><img
                                            src="assets/images/samples/post-img4-xs.jpg" alt="" /></Link></figure>
                                        <div className="posts__inner">
                                            <div className="posts__cat"><span className="label posts__cat-label">The Team</span>
                                            </div>
                                            <h6 className="posts__title"><Link to="#">The team is starting a new power
                                                breakfast regimen</Link></h6><time dateTime="2016-08-23"
                                                    className="posts__date">May 12th, 2018</time>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </aside>
                        <aside className="widget card widget--sidebar widget-player widget-player--soccer">
                            <div className="widget__content card__content">
                                <div className="widget-player__ribbon">
                                    <div className="fas fa-star"></div>
                                </div>
                                <figure className="widget-player__photo"><img
                                    src="assets/images/soccer/samples/_soccer_widget-featured-player.png" alt="" />
                                </figure>
                                <header className="widget-player__header clearfix">
                                    <div className="widget-player__number">07</div>
                                    <h4 className="widget-player__name"><span className="widget-player__first-name">James</span>
                                        <span className="widget-player__last-name">Messinal</span></h4>
                                </header>
                                <div className="widget-player__content">
                                    <div className="widget-player__content-inner">
                                        <div className="widget-player__stat widget-player__goals">
                                            <div className="widget-player__stat-number">104</div>
                                            <h6 className="widget-player__stat-label">Goals</h6>
                                        </div>
                                        <div className="widget-player__stat widget-player__shots">
                                            <div className="widget-player__stat-number">129</div>
                                            <h6 className="widget-player__stat-label">Shots</h6>
                                        </div>
                                        <div className="widget-player__stat widget-player__assists">
                                            <div className="widget-player__stat-number">57</div>
                                            <h6 className="widget-player__stat-label">Assists</h6>
                                        </div>
                                        <div className="widget-player__stat widget-player__games">
                                            <div className="widget-player__stat-number">86</div>
                                            <h6 className="widget-player__stat-label">Games</h6>
                                        </div>
                                    </div>
                                    <div className="widget-player__content-alt">
                                        <div className="progress-stats">
                                            <div className="progress__label">SHOT ACC</div>
                                            <div className="progress">
                                                <div className="progress__bar progress__bar--success progress__bar-width-90"
                                                    role="progressbar" aria-valuenow="90" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                            <div className="progress__number">96%</div>
                                        </div>
                                        <div className="progress-stats">
                                            <div className="progress__label">PASS ACC</div>
                                            <div className="progress">
                                                <div className="progress__bar progress__bar--success progress__bar-width-70"
                                                    role="progressbar" aria-valuenow="70" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                            <div className="progress__number">74%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <aside className="widget card widget--sidebar widget-game-result">
                            <div className="widget__title card__header card__header--has-btn">
                                <h4>Last Game Results</h4><Link to="#"
                                    className="btn btn-default btn-outline btn-xs card-header__button">Expand Stats</Link>
                            </div>
                            <div className="widget__content card__content">
                                <div className="widget-game-result__section">
                                    <div className="widget-game-result__section-inner">
                                        <header className="widget-game-result__header">
                                            <h3 className="widget-game-result__title">Championship Quarter Finals</h3><time
                                                className="widget-game-result__date" dateTime="2016-03-24">Saturday, March
                                                24th, 2016</time>
                                        </header>
                                        <div className="widget-game-result__main">
                                            <div className="widget-game-result__team widget-game-result__team--first">
                                                <figure className="widget-game-result__team-logo"><Link to="#"><img
                                                    src="assets/images/soccer/logos/alchemists_last_game_results_big.png"
                                                    alt="" /></Link></figure>
                                                <div className="widget-game-result__team-info">
                                                    <h5 className="widget-game-result__team-name">Alchemists</h5>
                                                    <div className="widget-game-result__team-desc">Elric Bros School</div>
                                                </div>
                                            </div>
                                            <div className="widget-game-result__score-wrap">
                                                <div className="widget-game-result__score"><span
                                                    className="widget-game-result__score-result widget-game-result__score-result--winner">2</span>
                                                    <span className="widget-game-result__score-dash">-</span> <span
                                                        className="widget-game-result__score-result widget-game-result__score-result--loser">0</span>
                                                </div>
                                                <div className="widget-game-result__score-label">Final Score</div>
                                            </div>
                                            <div className="widget-game-result__team widget-game-result__team--second">
                                                <figure className="widget-game-result__team-logo"><Link to="#"><img
                                                    src="assets/images/samples/logo-l-clovers--sm.png"
                                                    alt="" /></Link></figure>
                                                <div className="widget-game-result__team-info">
                                                    <h5 className="widget-game-result__team-name">Clovers</h5>
                                                    <div className="widget-game-result__team-desc">St Paddy's Institute
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="widget-game-result__section">
                                    <div className="df-timeline-wrapper">
                                        <div className="df-timeline">
                                            <div className="df-timeline__event df-timeline__event--start">
                                                <div className="df-timeline__team-1">
                                                    <div className="df-timeline__team-shirt"><i
                                                        className="icon-svg icon-shirt"></i></div>
                                                </div>
                                                <div className="df-timeline__time">0’</div>
                                                <div className="df-timeline__team-2">
                                                    <div className="df-timeline__team-shirt"><i
                                                        className="icon-svg icon-shirt-alt"></i></div>
                                                </div>
                                            </div>
                                            <div className="df-timeline__event df-timeline__event--empty"></div>
                                            <div className="df-timeline__event">
                                                <div className="df-timeline__team-1">
                                                    <div className="df-timeline__event-info">
                                                        <div className="df-timeline__event-name">F. Stevens</div>
                                                        <div className="df-timeline__event-desc">Alchemists 1-0</div>
                                                    </div>
                                                    <div className="df-timeline__event-icon"><i
                                                        className="icon-svg icon-soccer-ball"></i></div>
                                                </div>
                                                <div className="df-timeline__time">22’</div>
                                            </div>
                                            <div className="df-timeline__event">
                                                <div className="df-timeline__time">36’</div>
                                                <div className="df-timeline__team-2">
                                                    <div className="df-timeline__event-icon"><i
                                                        className="icon-svg icon-yellow-card"></i></div>
                                                    <div className="df-timeline__event-info">
                                                        <div className="df-timeline__event-name">Johnny Griffin</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="df-timeline__event df-timeline__event--empty"></div>
                                            <div className="df-timeline__event">
                                                <div className="df-timeline__time">HT</div>
                                                <div className="df-timeline__team-2">
                                                    <div className="df-timeline__event-icon"><i
                                                        className="icon-svg icon-substitution"></i></div>
                                                    <div className="df-timeline__event-info">
                                                        <div className="df-timeline__event-name">Markus Jackson</div>
                                                        <div className="df-timeline__event-name">Rick Valentine</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="df-timeline__event">
                                                <div className="df-timeline__team-1">
                                                    <div className="df-timeline__event-info">
                                                        <div className="df-timeline__event-name">Brian Kingster</div>
                                                    </div>
                                                    <div className="df-timeline__event-icon"><i
                                                        className="icon-svg icon-red-card"></i></div>
                                                </div>
                                                <div className="df-timeline__time">59’</div>
                                            </div>
                                            <div className="df-timeline__event">
                                                <div className="df-timeline__team-1">
                                                    <div className="df-timeline__event-info">
                                                        <div className="df-timeline__event-name">Christofer Grass (P)</div>
                                                        <div className="df-timeline__event-desc">Alchemists 2-0</div>
                                                    </div>
                                                    <div className="df-timeline__event-icon"><i
                                                        className="icon-svg icon-soccer-ball-penalty"></i></div>
                                                </div>
                                                <div className="df-timeline__time">68’</div>
                                            </div>
                                            <div className="df-timeline__event df-timeline__event--empty"></div>
                                            <div className="df-timeline__event">
                                                <div className="df-timeline__time">84’</div>
                                                <div className="df-timeline__team-2">
                                                    <div className="df-timeline__event-icon"><i
                                                        className="icon-svg icon-yellow-card"></i></div>
                                                    <div className="df-timeline__event-info">
                                                        <div className="df-timeline__event-name">Wally Christison</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="widget-game-result__section">
                                    <header
                                        className="widget-game-result__subheader card__subheader card__subheader--sm card__subheader--nomargins">
                                        <h5 className="widget-game-result__subtitle">Game Statistics</h5>
                                    </header>
                                    <div className="widget-game-result__section-inner">
                                        <div className="progress-double-wrapper">
                                            <h6 className="progress-title">Shots on Goal</h6>
                                            <div className="progress-inner-holder">
                                                <div className="progress__digit progress__digit--left progress__digit--40">
                                                    15</div>
                                                <div className="progress__double">
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar-width-60"
                                                            role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar--success progress__bar-width-80"
                                                            role="progressbar" aria-valuenow="80" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="progress__digit progress__digit--right progress__digit--highlight progress__digit--40">
                                                    24</div>
                                            </div>
                                        </div>
                                        <div className="progress-double-wrapper">
                                            <h6 className="progress-title">Ball Possession</h6>
                                            <div className="progress-inner-holder">
                                                <div
                                                    className="progress__digit progress__digit--left progress__digit--highlight progress__digit--40">
                                                    75%</div>
                                                <div className="progress__double">
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar-width-80"
                                                            role="progressbar" aria-valuenow="80" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar--success progress__bar-width-40"
                                                            role="progressbar" aria-valuenow="40" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div className="progress__digit progress__digit--right progress__digit--40">
                                                    35%</div>
                                            </div>
                                        </div>
                                        <div className="progress-double-wrapper">
                                            <h6 className="progress-title">Fouls</h6>
                                            <div className="progress-inner-holder">
                                                <div className="progress__digit progress__digit--left progress__digit--40">5
                                                </div>
                                                <div className="progress__double">
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar-width-30"
                                                            role="progressbar" aria-valuenow="30" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar--success progress__bar-width-60"
                                                            role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="progress__digit progress__digit--right progress__digit--highlight progress__digit--40">
                                                    8</div>
                                            </div>
                                        </div>
                                        <div className="progress-double-wrapper">
                                            <h6 className="progress-title">Corner Kicks</h6>
                                            <div className="progress-inner-holder">
                                                <div className="progress__digit progress__digit--left progress__digit--40">
                                                    10</div>
                                                <div className="progress__double">
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar-width-30"
                                                            role="progressbar" aria-valuenow="30" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                    <div className="progress">
                                                        <div className="progress__bar progress__bar--success progress__bar-width-80"
                                                            role="progressbar" aria-valuenow="80" aria-valuemin="0"
                                                            aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="progress__digit progress__digit--right progress__digit--highlight progress__digit--40">
                                                    12</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <aside className="widget widget--sidebar card widget-tabbed">
                            <div className="widget__title card__header">
                                <h4>Top Trending News</h4>
                            </div>
                            <div className="widget__content card__content">
                                <div className="widget-tabbed__tabs">
                                    <ul className="nav nav-tabs nav-justified widget-tabbed__nav" role="tablist">
                                        <li className="nav-item"><Link to="#widget-tabbed-sm-newest" className="nav-link active"
                                            aria-controls="widget-tabbed-sm-newest" role="tab"
                                            data-toggle="tab">Newest</Link></li>
                                        <li className="nav-item"><Link to="#widget-tabbed-sm-commented" className="nav-link"
                                            aria-controls="widget-tabbed-sm-commented" role="tab"
                                            data-toggle="tab">Most Commented</Link></li>
                                        <li className="nav-item"><Link to="#widget-tabbed-sm-popular" className="nav-link"
                                            aria-controls="widget-tabbed-sm-popular" role="tab"
                                            data-toggle="tab">Popular</Link></li>
                                    </ul>
                                    <div className="tab-content widget-tabbed__tab-content">
                                        <div role="tabpanel" className="tab-pane fade show active"
                                            id="widget-tabbed-sm-newest">
                                            <ul className="posts posts--simple-list">
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">Jake Dribbler Announced
                                                            that he is retiring next month</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">The Alchemists news coach
                                                            is bringin a new shooting guard</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">This Saturday starts the
                                                            intensive training for the Final</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div role="tabpanel" className="tab-pane fade" id="widget-tabbed-sm-commented">
                                            <ul className="posts posts--simple-list">
                                                <li className="posts__item posts__item--category-3">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span
                                                            className="label posts__cat-label">Playoffs</span></div>
                                                        <h6 className="posts__title"><Link to="#">New York Islanders are now
                                                            flying to California for the big game</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">The Female Division is
                                                            growing strong after their third game</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">The Alchemists news coach
                                                            is bringin a new shooting guard</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div role="tabpanel" className="tab-pane fade" id="widget-tabbed-sm-popular">
                                            <ul className="posts posts--simple-list">
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">The Alchemists news coach
                                                            is bringin a new shooting guard</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">This Saturday starts the
                                                            intensive training for the Final</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                                <li className="posts__item posts__item--category-1">
                                                    <div className="posts__inner">
                                                        <div className="posts__cat"><span className="label posts__cat-label">The
                                                            Team</span></div>
                                                        <h6 className="posts__title"><Link to="#">Jake Dribbler Announced
                                                            that he is retiring next month</Link></h6><time
                                                                dateTime="2016-08-23" className="posts__date">August 23rd,
                                                            2018</time>
                                                        <div className="posts__excerpt">Lorem ipsum dolor sit amet,
                                                            consectetur adipisi ng elit, sed do eiusmod tempor
                                                            incididunt ut labore et dolore magna aliqua.</div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                        <aside className="widget widget--sidebar card widget-preview">
                            <div className="widget__title card__header">
                                <h4>Next Match</h4>
                            </div>
                            <div className="widget__content card__content">
                                <div className="match-preview">
                                    <section className="match-preview__body">
                                        <header className="match-preview__header"><time className="match-preview__date"
                                            dateTime="2018-08-14">Friday August 14th</time>
                                            <h3 className="match-preview__title match-preview__title--lg">Quarter Finals
                                            </h3>
                                        </header>
                                        <div className="match-preview__content">
                                            <div className="match-preview__team match-preview__team--first">
                                                <figure className="match-preview__team-logo"><img
                                                    src="assets/images/soccer/logos/alchemists_buy_tickets.png"
                                                    alt="" /></figure>
                                                <h5 className="match-preview__team-name">Alchemists</h5>
                                                <div className="match-preview__team-info">Elric Bros School</div>
                                            </div>
                                            <div className="match-preview__vs">
                                                <div className="match-preview__conj">VS</div>
                                                <div className="match-preview__match-info"><time
                                                    className="match-preview__match-time"
                                                    dateTime="2017-08-12 09:00">9:00 PM</time>
                                                    <div className="match-preview__match-place">Madison Cube Stadium</div>
                                                </div>
                                            </div>
                                            <div className="match-preview__team match-preview__team--second">
                                                <figure className="match-preview__team-logo"><img
                                                    src="assets/images/soccer/logos/pirates_buy_tickets.png" alt="" />
                                                </figure>
                                                <h5 className="match-preview__team-name">Pirates</h5>
                                                <div className="match-preview__team-info">Bebop Institute</div>
                                            </div>
                                        </div>
                                    </section>
                                    <div className="countdown__content">
                                        <div className="countdown-counter" data-date="June 18, 2022 21:00:00"></div>
                                    </div>
                                    <div className="match-preview__action match-preview__action--ticket"><Link to="#"
                                        className="btn btn-primary-inverse btn-lg btn-block">Buy Tickets Now</Link></div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>*/}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
