import React, { useState, useEffect } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

const Disclaimer = () => {
  return (
    <div className="App">
      <Header />
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="content col-lg-12">
              <article className="card card--lg post post--single">
                <div className="card__content">
                  <header className="post__header">
                    <h2 className="post__title">
                        Disclaimer
                    </h2>
                  </header>
                  <div className="post__content">
                    <p>
                        In particular, it is highlighted that MVLIGA: (a) does not have any tangible or physical manifestation, and does not have any intrinsic value (nor does any person make any representation or give any commitment as to its value); (b) is non-refundable and cannot be exchanged for cash (or its equivalent value in any other virtual currency) or any payment obligation by the Company, the Distributor or any of their respective affiliates; (c) does not represent or confer on the token holder any right of any form with respect to the Company, the Distributor (or any of their respective affiliates), or its revenues or assets, including without limitation any right to receive future dividends, revenue, shares, ownership right or stake, share or security, any voting, distribution, redemption, liquidation, proprietary (including all forms of intellectual property or license rights), right to receive accounts, financial statements or other financial data, the right to requisition or participate in shareholder meetings, the right to nominate a director, or other financial or legal rights or equivalent rights, or intellectual property rights or any other form of participation in or relating to the MVLiga ecosystem, the Company, the Distributor and/or their service providers; (d) is not intended to represent any rights under a contract for differences or under any other contract the purpose or pretended purpose of which is to secure a profit or avoid a loss; (e) is not intended to be a representation of money (including electronic money), security, commodity, bond, debt instrument, unit in a collective investment scheme or any other kind of financial instrument or investment; (f) is not a loan to the Company, the Distributor or any of their respective affiliates, is not intended to represent a debt owed by the Company, the Distributor or any of their respective affiliates, and there is no expectation of profit; and (g) does not provide the token holder with any ownership or other interest in the Company, the Distributor or any of their respective affiliates. The contributions in the token sale will be held by the Distributor (or their respective affiliate) after the token sale, and contributors will have no economic or legal right over or beneficial interest in these contributions or the assets of that entity after the token sale. To the extent a secondary market or exchange for trading MVLIGA does develop, it would be run and operated wholly independently of the Company, the Distributor, the sale of MVLIGA and the MVLiga ecosystem. Neither the Company nor the Distributor will create such secondary markets nor will either entity act as an exchange for MVLIGA.
                    </p>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Disclaimer;
