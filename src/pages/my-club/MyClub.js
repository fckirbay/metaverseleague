import React, { useRef, useState, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Slider from "../../components/Slider";
import { useMoralis } from "react-moralis";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";

const MyClub = () => {
  const { Moralis } = useMoralis();

  const [players, setPlayers] = useState([]);

  async function getPlayers() {
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    query.descending("market_value");
    query.limit(30);
    const results = await query.find({ useMasterKey: true });
    setPlayers(results);
  }

  useEffect(() => {
    //getPlayers();
  }, []);

  return (
    <div className="App">
      <Header />
      <div className="page-heading">
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <h1 className="page-heading__title">
                Team <span className="highlight">Gallery</span>
              </h1>
              <ol className="page-heading__breadcrumb breadcrumb">
                <li className="breadcrumb-item">
                  <Link to="#">Home</Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to="#">Team</Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Gallery
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <nav className="content-filter">
        <div className="container">
          <Link to="#" className="content-filter__toggle"></Link>
          <ul className="content-filter__list">
            <li className="content-filter__item content-filter__item--active">
              <Link to="#"
                className="content-filter__link"
              >
                <small>The Team</small>Overview
              </Link>
            </li>
            <li className="content-filter__item">
              <Link to="#"
                className="content-filter__link"
              >
                <small>The Team</small>Roster
              </Link>
            </li>
            <li className="content-filter__item">
              <Link to="#"
                className="content-filter__link"
              >
                <small>The Team</small>Standings
              </Link>
            </li>
            <li className="content-filter__item">
              <Link to="#"
                className="content-filter__link"
              >
                <small>The Team</small>Latest Results
              </Link>
            </li>
            <li className="content-filter__item">
              <Link to="#"
                className="content-filter__link"
              >
                <small>The Team</small>Schedule
              </Link>
            </li>
            <li className="content-filter__item">
              <Link to="#"
                className="content-filter__link"
              >
                <small>The Team</small>Gallery
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="content col-lg-8">
              <div className="card">
                <header className="card__header card__header--has-btn">
                  <h4>Last Game Results</h4>
                  <Link to="#"
                    className="btn btn-default btn-outline btn-xs card-header__button"
                  >
                    Check previous Results
                  </Link>
                </header>
                <div className="card__content">
                  <div className="game-result">
                    <section className="game-result__section pt-0">
                      <header className="game-result__header game-result__header--alt">
                        <span className="game-result__league">
                          West League 2016
                        </span>
                        <h3 className="game-result__title">Madison Cube Stadium</h3>
                        <time className="game-result__date" dateTime="2017-03-17">
                          Saturday, March 17th, 2017
                        </time>
                      </header>
                      <div className="game-result__content">
                        <div className="game-result__team game-result__team--first">
                          <figure className="game-result__team-logo">
                            <img
                              src="assets/images/soccer/logos/alchemists_last_game_results_big.png"
                              alt=""
                            />
                          </figure>
                          <div className="game-result__team-info">
                            <h5 className="game-result__team-name">Alchemists</h5>
                            <div className="game-result__team-desc">
                              Elric Bros School
                            </div>
                          </div>
                        </div>
                        <div className="game-result__score-wrap">
                          <div className="game-result__score game-result__score--lg">
                            <span className="game-result__score-result game-result__score-result--winner">
                              2
                            </span>{" "}
                            <span className="game-result__score-dash">-</span>{" "}
                            <span className="game-result__score-result game-result__score-result--loser">
                              0
                            </span>
                          </div>
                          <div className="game-result__score-label">
                            Final Score
                          </div>
                        </div>
                        <div className="game-result__team game-result__team--second">
                          <figure className="game-result__team-logo">
                            <img
                              src="assets/images/samples/logo-l-clovers--sm.png"
                              alt=""
                            />
                          </figure>
                          <div className="game-result__team-info">
                            <h5 className="game-result__team-name">Clovers</h5>
                            <div className="game-result__team-desc">
                              St Paddy's Institute
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <div className="spacer"></div>
                    <section className="game-result__section">
                      <header className="game-result__header game-result__header--alt">
                        <span className="game-result__goal">
                          Franklin Stevens (22’){" "}
                          <i className="icon-svg icon-soccer-ball"></i>
                        </span>{" "}
                        <span className="game-result__goal">
                          Christofer Grass (68’) (P){" "}
                          <i className="icon-svg icon-soccer-ball"></i>
                        </span>
                      </header>
                      <div className="game-result__content mb-0">
                        <div className="game-result__stats">
                          <div className="row">
                            <div className="col-12 col-md-6 order-md-2">
                              <div className="game-result__table-stats game-result__table-stats--soccer">
                                <table className="table table-wrap-bordered table-thead-color">
                                  <thead>
                                    <tr>
                                      <th colSpan="3">Game Statistics</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>25(14)</td>
                                      <td>Shots (on goal)</td>
                                      <td>16(6)</td>
                                    </tr>
                                    <tr>
                                      <td>9</td>
                                      <td>Corner Kicks</td>
                                      <td>7</td>
                                    </tr>
                                    <tr>
                                      <td>8</td>
                                      <td>Saves</td>
                                      <td>5</td>
                                    </tr>
                                    <tr>
                                      <td>0</td>
                                      <td>Yellow Cards</td>
                                      <td>2</td>
                                    </tr>
                                    <tr>
                                      <td>1</td>
                                      <td>Red Cards</td>
                                      <td>0</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                            <div className="col-6 col-md-3 order-md-1 game-result__stats-team-1">
                              <div className="row">
                                <div className="col-6">
                                  <div className="circular circular--size-70">
                                    <div
                                      className="circular__bar"
                                      data-percent="84.5"
                                    >
                                      <span className="circular__percents">
                                        84.5<small>%</small>
                                      </span>
                                      <canvas
                                        height="180"
                                        width="180"
                                        style={{height: '90px', width: '90px'}}
                                      ></canvas>
                                    </div>
                                    <span className="circular__label">
                                      Shot
                                      <br />
                                      Accuracy
                                    </span>
                                  </div>
                                </div>
                                <div className="col-6">
                                  <div className="circular circular--size-70">
                                    <div
                                      className="circular__bar"
                                      data-percent="62.3"
                                    >
                                      <span className="circular__percents">
                                        62.3<small>%</small>
                                      </span>
                                      <canvas
                                        height="180"
                                        width="180"
                                        style={{height: '90px', width: '90px'}}
                                      ></canvas>
                                    </div>
                                    <span className="circular__label">
                                      Pass
                                      <br />
                                      Accuracy
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div className="spacer"></div>
                              <div className="progress-stats">
                                <div className="progress__label">Sho</div>
                                <div className="progress">
                                  <div
                                    className="progress__bar progress__bar-width-90"
                                    role="progressbar"
                                    aria-valuenow="90"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                  ></div>
                                </div>
                                <div className="progress__number">25</div>
                              </div>
                              <div className="progress-stats">
                                <div className="progress__label">Fou</div>
                                <div className="progress">
                                  <div
                                    className="progress__bar progress__bar-width-40"
                                    role="progressbar"
                                    aria-valuenow="40"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                  ></div>
                                </div>
                                <div className="progress__number">12</div>
                              </div>
                              <div className="progress-stats">
                                <div className="progress__label">OFF</div>
                                <div className="progress">
                                  <div
                                    className="progress__bar progress__bar-width-30"
                                    role="progressbar"
                                    aria-valuenow="30"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                  ></div>
                                </div>
                                <div className="progress__number">10</div>
                              </div>
                            </div>
                            <div className="col-6 col-md-3 order-md-3 game-result__stats-team-2">
                              <div className="row">
                                <div className="col-6">
                                  <div className="circular circular--size-70">
                                    <div
                                      className="circular__bar"
                                      data-percent="74.6"
                                      data-bar-color="#9fe900"
                                    >
                                      <span className="circular__percents">
                                        74.6<small>%</small>
                                      </span>
                                      <canvas
                                        height="180"
                                        width="180"
                                        style={{height: '90px', width: '90px'}}
                                      ></canvas>
                                    </div>
                                    <span className="circular__label">
                                      Shot
                                      <br />
                                      Accuracy
                                    </span>
                                  </div>
                                </div>
                                <div className="col-6">
                                  <div className="circular circular--size-70">
                                    <div
                                      className="circular__bar"
                                      data-percent="53.9"
                                      data-bar-color="#9fe900"
                                    >
                                      <span className="circular__percents">
                                        53.9<small>%</small>
                                      </span>
                                      <canvas
                                        height="180"
                                        width="180"
                                        style={{height: '90px', width: '90px'}}
                                      ></canvas>
                                    </div>
                                    <span className="circular__label">
                                      Pass
                                      <br />
                                      Accuracy
                                    </span>
                                  </div>
                                </div>
                              </div>
                              <div className="spacer"></div>
                              <div className="progress-stats">
                                <div className="progress__label">Sho</div>
                                <div className="progress">
                                  <div
                                    className="progress__bar progress__bar--success progress__bar-width-80"
                                    role="progressbar"
                                    aria-valuenow="80"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                  ></div>
                                </div>
                                <div className="progress__number">25</div>
                              </div>
                              <div className="progress-stats">
                                <div className="progress__label">Fou</div>
                                <div className="progress">
                                  <div
                                    className="progress__bar progress__bar--success progress__bar-width-60"
                                    role="progressbar"
                                    aria-valuenow="60"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                  ></div>
                                </div>
                                <div className="progress__number">14</div>
                              </div>
                              <div className="progress-stats">
                                <div className="progress__label">OFF</div>
                                <div className="progress">
                                  <div
                                    className="progress__bar progress__bar--success progress__bar-width-40"
                                    role="progressbar"
                                    aria-valuenow="40"
                                    aria-valuemin="0"
                                    aria-valuemax="100"
                                  ></div>
                                </div>
                                <div className="progress__number">12</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
              <div className="card card--has-table">
                <div className="card__header card__header--has-btn">
                  <h4>Main Lineup</h4>
                  <a
                    href="_soccer_team-roster.html"
                    className="btn btn-default btn-outline btn-xs card-header__button"
                  >
                    See Complete Roster
                  </a>
                </div>
                <div className="card__content">
                  <div className="table-responsive">
                    <table className="table table--lg team-roster-table">
                      <thead>
                        <tr>
                          <th className="team-roster-table__number">NBR</th>
                          <th className="team-roster-table__position">Pos</th>
                          <th className="team-roster-table__name">Player Name</th>
                          <th className="team-roster-table__foot">Foot</th>
                          <th className="team-roster-table__age">Age</th>
                          <th className="team-roster-table__height">Height</th>
                          <th className="team-roster-table__weight">Weight</th>
                          <th className="team-roster-table__college d-none d-sm-table-cell">
                            College
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="team-roster-table__number">01</td>
                          <td className="team-roster-table__position">GK</td>
                          <td className="team-roster-table__name">Nick Rodgers</td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">18</td>
                          <td className="team-roster-table__height">6’6’’</td>
                          <td className="team-roster-table__weight">205 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            South Beach College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">04</td>
                          <td className="team-roster-table__position">DF</td>
                          <td className="team-roster-table__name">Mark Ironson</td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">17</td>
                          <td className="team-roster-table__height">6’1’’</td>
                          <td className="team-roster-table__weight">189 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            Green Palm College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">03</td>
                          <td className="team-roster-table__position">DF</td>
                          <td className="team-roster-table__name">
                            Brian Kingster
                          </td>
                          <td className="team-roster-table__foot">Left</td>
                          <td className="team-roster-table__age">16</td>
                          <td className="team-roster-table__height">5’2’’</td>
                          <td className="team-roster-table__weight">186 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            Green Palm College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">22</td>
                          <td className="team-roster-table__position">DF</td>
                          <td className="team-roster-table__name">
                            James Girobilli
                          </td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">18</td>
                          <td className="team-roster-table__height">6’2’’</td>
                          <td className="team-roster-table__weight">198 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            The Sand Bay Institute
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">05</td>
                          <td className="team-roster-table__position">DF</td>
                          <td className="team-roster-table__name">Thomas Black</td>
                          <td className="team-roster-table__foot">Left</td>
                          <td className="team-roster-table__age">17</td>
                          <td className="team-roster-table__height">5’8’’</td>
                          <td className="team-roster-table__weight">194 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            South Beach College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">08</td>
                          <td className="team-roster-table__position">MF</td>
                          <td className="team-roster-table__name">
                            Christofer Grass
                          </td>
                          <td className="team-roster-table__foot">Left</td>
                          <td className="team-roster-table__age">18</td>
                          <td className="team-roster-table__height">5’6’’</td>
                          <td className="team-roster-table__weight">176 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            The Sand Bay Institute
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">02</td>
                          <td className="team-roster-table__position">MF</td>
                          <td className="team-roster-table__name">
                            Spike Arrowhead
                          </td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">17</td>
                          <td className="team-roster-table__height">5’5’’</td>
                          <td className="team-roster-table__weight">180 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            Green Palm College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">26</td>
                          <td className="team-roster-table__position">MF</td>
                          <td className="team-roster-table__name">
                            Griffin Peterson
                          </td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">17</td>
                          <td className="team-roster-table__height">6’6’’</td>
                          <td className="team-roster-table__weight">184 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            South Beach College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">07</td>
                          <td className="team-roster-table__position">MF</td>
                          <td className="team-roster-table__name">
                            James Messinal
                          </td>
                          <td className="team-roster-table__foot">Left</td>
                          <td className="team-roster-table__age">18</td>
                          <td className="team-roster-table__height">5’8’’</td>
                          <td className="team-roster-table__weight">176 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            Green Palm College
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">09</td>
                          <td className="team-roster-table__position">FD</td>
                          <td className="team-roster-table__name">
                            Franklin Stevens
                          </td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">16</td>
                          <td className="team-roster-table__height">5’9’’</td>
                          <td className="team-roster-table__weight">196 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            The Sand Bay Institute
                          </td>
                        </tr>
                        <tr>
                          <td className="team-roster-table__number">18</td>
                          <td className="team-roster-table__position">FD</td>
                          <td className="team-roster-table__name">David Hawkins</td>
                          <td className="team-roster-table__foot">Right</td>
                          <td className="team-roster-table__age">18</td>
                          <td className="team-roster-table__height">6’3’’</td>
                          <td className="team-roster-table__weight">202 lbs</td>
                          <td className="team-roster-table__college d-none d-sm-table-cell">
                            South Beach College
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <aside className="widget widget--sidebar card card--has-table widget-team-stats">
                    <div className="widget__title card__header">
                      <h4>Team Stats</h4>
                    </div>
                    <div className="widget__content card__content">
                      <ul className="team-stats-box">
                        <li className="team-stats__item team-stats__item--clean">
                          <div className="team-stats__icon team-stats__icon--circle">
                            <div className="team-stats__icon-primary">
                              <i className="icon-svg icon-soccer-ball"></i>
                            </div>
                          </div>
                          <div className="team-stats__value">2.54</div>
                          <div className="team-stats__label">Goals Per Game</div>
                        </li>
                        <li className="team-stats__item team-stats__item--clean">
                          <div className="team-stats__icon team-stats__icon--circle team-stats__icon--shots-ot">
                            <div className="team-stats__icon-primary">
                              <i className="icon-svg icon-soccer-ball"></i>
                            </div>
                            <div className="team-stats__icon-secondary">
                              <i className="icon-svg icon-soccer-gate"></i>
                            </div>
                          </div>
                          <div className="team-stats__value">31.6</div>
                          <div className="team-stats__label">Shots OT Per Game</div>
                        </li>
                        <li className="team-stats__item team-stats__item--clean">
                          <div className="team-stats__icon team-stats__icon--circle team-stats__icon--shots">
                            <div className="team-stats__icon-primary">
                              <i className="icon-svg icon-soccer-ball"></i>
                            </div>
                            <div className="team-stats__icon-secondary">
                              <i className="icon-svg icon-soccer-shots"></i>
                            </div>
                          </div>
                          <div className="team-stats__value">68.9</div>
                          <div className="team-stats__label">
                            Total Shots Per Game
                          </div>
                        </li>
                        <li className="team-stats__item team-stats__item--clean">
                          <div className="team-stats__icon team-stats__icon--circle team-stats__icon--assists">
                            <div className="team-stats__icon-primary">
                              <i className="icon-svg icon-soccer-ball"></i>
                            </div>
                            <span className="team-stats__icon-secondary">A</span>
                          </div>
                          <div className="team-stats__value">1.7</div>
                          <div className="team-stats__label">
                            Goal Assists Per Game
                          </div>
                        </li>
                      </ul>
                    </div>
                  </aside>
                </div>
                <div className="col-md-6">
                  <aside className="widget card widget--sidebar widget-awards">
                    <div className="widget__title card__header">
                      <h4>Our Awards</h4>
                    </div>
                    <div className="widget__content card__content">
                      <div className="awards awards--slider slick-initialized slick-slider slick-dotted">
                        <button
                          className="slick-prev slick-arrow"
                          aria-label="Previous"
                          type="button"
                        >
                          Previous
                        </button>
                        <div className="slick-list draggable">
                          <div
                            className="slick-track"
                            style={{opacity: '1', width: '2310px', transform: 'translate3d(-330px, 0px, 0px)'}}
                          >
                            <div
                              className="awards__item slick-slide slick-cloned"
                              data-slick-index="-1"
                              aria-hidden="true"
                              style={{width: '330px'}}
                              tabIndex="-1"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  Big League Champions
                                </h5>
                                <div className="awards__date">December 2012</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                            <div
                              className="awards__item slick-slide slick-current slick-active"
                              data-slick-index="0"
                              aria-hidden="false"
                              style={{width: '330px'}}
                              role="tabpanel"
                              id="slick-slide00"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  West League Champions
                                </h5>
                                <div className="awards__date">December 2009</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                            <div
                              className="awards__item slick-slide"
                              data-slick-index="1"
                              aria-hidden="true"
                              style={{width: '330px'}}
                              tabIndex="-1"
                              role="tabpanel"
                              id="slick-slide01"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  East League Champions
                                </h5>
                                <div className="awards__date">November 2010</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                            <div
                              className="awards__item slick-slide"
                              data-slick-index="2"
                              aria-hidden="true"
                              style={{width: '330px'}}
                              tabIndex="-1"
                              role="tabpanel"
                              id="slick-slide02"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  Big League Champions
                                </h5>
                                <div className="awards__date">December 2012</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                            <div
                              className="awards__item slick-slide slick-cloned"
                              data-slick-index="3"
                              aria-hidden="true"
                              style={{width: '330px'}}
                              tabIndex="-1"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  West League Champions
                                </h5>
                                <div className="awards__date">December 2009</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                            <div
                              className="awards__item slick-slide slick-cloned"
                              data-slick-index="4"
                              aria-hidden="true"
                              style={{width: '330px'}}
                              tabIndex="-1"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  East League Champions
                                </h5>
                                <div className="awards__date">November 2010</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                            <div
                              className="awards__item slick-slide slick-cloned"
                              data-slick-index="5"
                              aria-hidden="true"
                              style={{width: '330px'}}
                              tabIndex="-1"
                            >
                              <figure className="awards__figure awards__figure--space">
                                <img
                                  src="assets/images/soccer/trophy-04.svg"
                                  alt=""
                                />
                              </figure>
                              <div className="awards__desc">
                                <h5 className="awards__name">
                                  Big League Champions
                                </h5>
                                <div className="awards__date">December 2012</div>
                                <div className="awards__stars">
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>{" "}
                                  <i className="fas fa-star"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <button
                          className="slick-next slick-arrow"
                          aria-label="Next"
                          type="button"
                        >
                          Next
                        </button>
                        <ul className="slick-dots" role="tablist">
                          <li className="slick-active" role="presentation">
                            <button
                              type="button"
                              role="tab"
                              id="slick-slide-control00"
                              aria-controls="slick-slide00"
                              aria-label="1 of 3"
                              tabIndex="0"
                              aria-selected="true"
                            >
                              1
                            </button>
                          </li>
                          <li role="presentation">
                            <button
                              type="button"
                              role="tab"
                              id="slick-slide-control01"
                              aria-controls="slick-slide01"
                              aria-label="2 of 3"
                              tabIndex="-1"
                            >
                              2
                            </button>
                          </li>
                          <li role="presentation">
                            <button
                              type="button"
                              role="tab"
                              id="slick-slide-control02"
                              aria-controls="slick-slide02"
                              aria-label="3 of 3"
                              tabIndex="-1"
                            >
                              3
                            </button>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </aside>
                </div>
              </div>
              <div className="card card--has-table">
                <div className="card__header card__header--has-btn">
                  <h4>Games Schedule</h4>
                  <a
                    href="_soccer_team-schedule.html"
                    className="btn btn-default btn-outline btn-xs card-header__button"
                  >
                    See Complete Schedule
                  </a>
                </div>
                <div className="card__content">
                  <div className="table-responsive">
                    <table className="table table-hover team-schedule">
                      <thead>
                        <tr>
                          <th className="team-schedule__date">Date</th>
                          <th className="team-schedule__versus">Versus</th>
                          <th className="team-schedule__time">Time</th>
                          <th className="team-schedule__venue">Venue</th>
                          <th className="team-schedule__tickets">Tickets</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="team-schedule__date">Saturday, Mar 24</td>
                          <td className="team-schedule__versus">
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/lucky_clovers_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Lucky Clovers</h6>
                                <span className="team-meta__place">
                                  St. Patrick’s Institute
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-schedule__time">9:00PM EST</td>
                          <td className="team-schedule__venue">
                            Madison Cube Stadium
                          </td>
                          <td className="team-schedule__tickets">
                            <a
                              href="#"
                              className="btn btn-xs btn-default btn-outline btn-block"
                            >
                              Buy Game Tickets
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-schedule__date">Friday, May 31</td>
                          <td className="team-schedule__versus">
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/red_wings_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Red Wings</h6>
                                <span className="team-meta__place">
                                  Icarus College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-schedule__time">9:30PM EST</td>
                          <td className="team-schedule__venue">
                            Alchemists Stadium
                          </td>
                          <td className="team-schedule__tickets">
                            <a
                              href="#"
                              className="btn btn-xs btn-default btn-outline btn-block disabled"
                            >
                              Sold Out
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-schedule__date">Saturday, May 8</td>
                          <td className="team-schedule__versus">
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/draconians_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Draconians</h6>
                                <span className="team-meta__place">
                                  Wyvern College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-schedule__time">10:00PM EST</td>
                          <td className="team-schedule__venue">
                            Scalding Rock Stadium
                          </td>
                          <td className="team-schedule__tickets">
                            <a
                              href="#"
                              className="btn btn-xs btn-default btn-outline btn-block"
                            >
                              Buy Game Tickets
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-schedule__date">Friday, May 14</td>
                          <td className="team-schedule__versus">
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/aqua_keyes_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Aqua Keyes</h6>
                                <span className="team-meta__place">
                                  Pacific Institute
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-schedule__time">10:00PM EST</td>
                          <td className="team-schedule__venue">
                            Alchemists Stadium
                          </td>
                          <td className="team-schedule__tickets">
                            <a
                              href="#"
                              className="btn btn-xs btn-default btn-outline btn-block"
                            >
                              Buy Game Tickets
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-schedule__date">Saturday, May 22</td>
                          <td className="team-schedule__versus">
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/icarus_wings_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Icarus Wings</h6>
                                <span className="team-meta__place">
                                  Waxer College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-schedule__time">10:30PM EST</td>
                          <td className="team-schedule__venue">
                            The FireStar Arena
                          </td>
                          <td className="team-schedule__tickets">
                            <a
                              href="#"
                              className="btn btn-xs btn-default btn-outline btn-block disabled"
                            >
                              Sold Out
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-schedule__date">Saturday, May 29</td>
                          <td className="team-schedule__versus">
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/bloody_wave_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Bloody Wave</h6>
                                <span className="team-meta__place">
                                  Atlantic School
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-schedule__time">9:00PM EST</td>
                          <td className="team-schedule__venue">
                            Alchemists Stadium
                          </td>
                          <td className="team-schedule__tickets">
                            <a
                              href="#"
                              className="btn btn-xs btn-default btn-outline btn-block"
                            >
                              Buy Game Tickets
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="card__header card__header--has-legend">
                  <h4>Points History</h4>
                  <div id="gamesPoinstsLegendSoccer" className="chart-legend">
                    <ul className="1-legend">
                      <li>
                        <span style={{backgroundColor: 'rgba(194,255,31,0.8)'}}></span>
                        PREV RECORD
                      </li>
                      <li>
                        <span style={{backgroundColor: 'rgba(56,169,255,0.8)'}}></span>
                        THIS YEAR
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card__content">
                  <iframe
                    className="chartjs-hidden-iframe"
                    tabIndex="-1"
                    style={{display: 'block', overflow: 'hidden', border: '0px', margin: '0px', inset: '0px', height: '100%', width: '100%', position: 'absolute', pointerEvents: 'none', zIndex: '-1'}}
                  ></iframe>
                  <canvas
                    id="points-history-soccer"
                    className="points-history-chart"
                    height="652"
                    width="1450"
                    style={{display: 'block', height: '326px', width: '725px'}}
                  ></canvas>
                </div>
              </div>
            </div>
            <div className="sidebar col-lg-4">
              <aside className="widget card widget--sidebar widget-standings">
                <div className="widget__title card__header card__header--has-btn">
                  <h4>West League 2018</h4>
                  <a
                    href="#"
                    className="btn btn-default btn-outline btn-xs card-header__button"
                  >
                    See All Stats
                  </a>
                </div>
                <div className="widget__content card__content">
                  <div className="table-responsive">
                    <table className="table table-hover table-standings">
                      <thead>
                        <tr>
                          <th>Team Positions</th>
                          <th>W</th>
                          <th>L</th>
                          <th>D</th>
                          <th>PTS</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/pirates_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">L.A Pirates</h6>
                                <span className="team-meta__place">
                                  Bebop Institute
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>36</td>
                          <td>14</td>
                          <td>10</td>
                          <td>118</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/sharks_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Sharks</h6>
                                <span className="team-meta__place">
                                  Marine College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>0</td>
                          <td>0</td>
                          <td>0</td>
                          <td>0</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/alchemists_b_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">The Alchemists</h6>
                                <span className="team-meta__place">
                                  Eric Bros School
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>32</td>
                          <td>21</td>
                          <td>7</td>
                          <td>103</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/ocean_kings_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Ocean Kings</h6>
                                <span className="team-meta__place">
                                  Bay College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>30</td>
                          <td>20</td>
                          <td>10</td>
                          <td>100</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/red_wings_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Red Wings</h6>
                                <span className="team-meta__place">
                                  Icarus College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>28</td>
                          <td>24</td>
                          <td>8</td>
                          <td>92</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/lucky_clovers_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Lucky Clovers</h6>
                                <span className="team-meta__place">
                                  St. Patrick’s Institute
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>27</td>
                          <td>24</td>
                          <td>9</td>
                          <td>90</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/draconians_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Draconians</h6>
                                <span className="team-meta__place">
                                  High Rock College
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>25</td>
                          <td>28</td>
                          <td>7</td>
                          <td>82</td>
                        </tr>
                        <tr>
                          <td>
                            <div className="team-meta">
                              <figure className="team-meta__logo">
                                <img
                                  src="assets/images/samples/logos/bloody_wave_shield.png"
                                  alt=""
                                />
                              </figure>
                              <div className="team-meta__info">
                                <h6 className="team-meta__name">Bloody Wave</h6>
                                <span className="team-meta__place">
                                  Atlantic School
                                </span>
                              </div>
                            </div>
                          </td>
                          <td>24</td>
                          <td>30</td>
                          <td>6</td>
                          <td>78</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </aside>
              <aside className="widget widget--sidebar card card--has-table widget-leaders">
                <div className="widget__title card__header">
                  <h4>Team Goalscorers</h4>
                </div>
                <div className="widget__content card__content">
                  <div className="table-responsive">
                    <table className="table team-leader">
                      <thead>
                        <tr>
                          <th className="team-leader__type">West League 2016</th>
                          <th className="team-leader__goals">G</th>
                          <th className="team-leader__gp">P</th>
                          <th className="team-leader__avg">AVG</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="team-leader__player">
                            <div className="team-leader__player-info">
                              <figure className="team-leader__player-img team-leader__player-img--sm">
                                <img
                                  src="assets/images/soccer/samples/goalscorer_01.jpg"
                                  alt=""
                                />
                              </figure>
                              <div className="team-leader__player-inner">
                                <h5 className="team-leader__player-name">
                                  James Messinal
                                </h5>
                                <span className="team-leader__player-position">
                                  Forward
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-leader__goals">11</td>
                          <td className="team-leader__gp">12</td>
                          <td className="team-leader__avg">
                            <div className="circular">
                              <div className="circular__bar" data-percent="91">
                                <span className="circular__percents">0.91</span>
                                <canvas
                                  height="180"
                                  width="180"
                                  style={{height: '90px', width: '90px'}}
                                ></canvas>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-leader__player">
                            <div className="team-leader__player-info">
                              <figure className="team-leader__player-img team-leader__player-img--sm">
                                <img
                                  src="assets/images/soccer/samples/goalscorer_02.jpg"
                                  alt=""
                                />
                              </figure>
                              <div className="team-leader__player-inner">
                                <h5 className="team-leader__player-name">
                                  David Hawkins
                                </h5>
                                <span className="team-leader__player-position">
                                  Forward
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-leader__goals">10</td>
                          <td className="team-leader__gp">12</td>
                          <td className="team-leader__avg">
                            <div className="circular">
                              <div className="circular__bar" data-percent="83">
                                <span className="circular__percents">0.83</span>
                                <canvas
                                  height="180"
                                  width="180"
                                  style={{height: '90px', width: '90px'}}
                                ></canvas>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-leader__player">
                            <div className="team-leader__player-info">
                              <figure className="team-leader__player-img team-leader__player-img--sm">
                                <img
                                  src="assets/images/soccer/samples/goalscorer_01.jpg"
                                  alt=""
                                />
                              </figure>
                              <div className="team-leader__player-inner">
                                <h5 className="team-leader__player-name">
                                  Griffin Peterson
                                </h5>
                                <span className="team-leader__player-position">
                                  Midfielder
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-leader__goals">8</td>
                          <td className="team-leader__gp">10</td>
                          <td className="team-leader__avg">
                            <div className="circular">
                              <div className="circular__bar" data-percent="80">
                                <span className="circular__percents">0.80</span>
                                <canvas
                                  height="180"
                                  width="180"
                                  style={{height: '90px', width: '90px'}}
                                ></canvas>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-leader__player">
                            <div className="team-leader__player-info">
                              <figure className="team-leader__player-img team-leader__player-img--sm">
                                <img
                                  src="assets/images/soccer/samples/goalscorer_02.jpg"
                                  alt=""
                                />
                              </figure>
                              <div className="team-leader__player-inner">
                                <h5 className="team-leader__player-name">
                                  Christofer Grass
                                </h5>
                                <span className="team-leader__player-position">
                                  Midfielder
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-leader__goals">7</td>
                          <td className="team-leader__gp">10</td>
                          <td className="team-leader__avg">
                            <div className="circular">
                              <div className="circular__bar" data-percent="70">
                                <span className="circular__percents">0.70</span>
                                <canvas
                                  height="180"
                                  width="180"
                                  style={{height: '90px', width: '90px'}}
                                ></canvas>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td className="team-leader__player">
                            <div className="team-leader__player-info">
                              <figure className="team-leader__player-img team-leader__player-img--sm">
                                <img
                                  src="assets/images/soccer/samples/goalscorer_01.jpg"
                                  alt=""
                                />
                              </figure>
                              <div className="team-leader__player-inner">
                                <h5 className="team-leader__player-name">
                                  Mark Ironson
                                </h5>
                                <span className="team-leader__player-position">
                                  Defender
                                </span>
                              </div>
                            </div>
                          </td>
                          <td className="team-leader__goals">5</td>
                          <td className="team-leader__gp">10</td>
                          <td className="team-leader__avg">
                            <div className="circular">
                              <div className="circular__bar" data-percent="50">
                                <span className="circular__percents">0.50</span>
                                <canvas
                                  height="180"
                                  width="180"
                                  style={{height: '90px', width: '90px'}}
                                ></canvas>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </aside>
              <aside className="widget card widget--sidebar widget-results">
                <div className="widget__title card__header card__header--has-btn">
                  <h4>Latest Results</h4>
                  <a
                    href="#"
                    className="btn btn-default btn-outline btn-xs card-header__button"
                  >
                    See Full Results
                  </a>
                </div>
                <div className="widget__content card__content">
                  <ul className="widget-results__list">
                    <li className="widget-results__item">
                      <div className="widget-results__header justify-content-center">
                        <div className="widget-results__title">
                          Saturday, March 24
                        </div>
                      </div>
                      <div className="widget-results__content">
                        <div className="widget-results__team widget-results__team--first">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/soccer/logos/alchemists_s_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              Alchemists
                            </h5>
                            <span className="widget-results__team-info">
                              Elric Bros School
                            </span>
                          </div>
                        </div>
                        <div className="widget-results__result">
                          <div className="widget-results__score">
                            <span className="widget-results__score-winner">2</span>{" "}
                            - <span className="widget-results__score-loser">0</span>
                            <div className="widget-results__status">Home</div>
                          </div>
                        </div>
                        <div className="widget-results__team widget-results__team--second">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/samples/logos/sharks_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">Sharks</h5>
                            <span className="widget-results__team-info">
                              Marine College
                            </span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li className="widget-results__item">
                      <div className="widget-results__header justify-content-center">
                        <div className="widget-results__title">
                          Friday, March 16
                        </div>
                      </div>
                      <div className="widget-results__content">
                        <div className="widget-results__team widget-results__team--first">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/samples/logos/pirates_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              L.A. Pirates
                            </h5>
                            <span className="widget-results__team-info">
                              Bebop Institute
                            </span>
                          </div>
                        </div>
                        <div className="widget-results__result">
                          <div className="widget-results__score">
                            <span className="widget-results__score-winner">4</span>{" "}
                            - <span className="widget-results__score-loser">2</span>
                            <div className="widget-results__status">Away</div>
                          </div>
                        </div>
                        <div className="widget-results__team widget-results__team--second">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/soccer/logos/alchemists_s_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              Alchemists
                            </h5>
                            <span className="widget-results__team-info">
                              Eric Bros School
                            </span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li className="widget-results__item">
                      <div className="widget-results__header justify-content-center">
                        <div className="widget-results__title">
                          Saturday, March 10
                        </div>
                      </div>
                      <div className="widget-results__content">
                        <div className="widget-results__team widget-results__team--first">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/soccer/logos/alchemists_s_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              Alchemists
                            </h5>
                            <span className="widget-results__team-info">
                              Eric Bros School
                            </span>
                          </div>
                        </div>
                        <div className="widget-results__result">
                          <div className="widget-results__score">
                            <span className="widget-results__score-loser">0</span> -{" "}
                            <span className="widget-results__score-loser">0</span>
                            <div className="widget-results__status">Home</div>
                          </div>
                        </div>
                        <div className="widget-results__team widget-results__team--second">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/samples/logos/lucky_clovers_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">Clovers</h5>
                            <span className="widget-results__team-info">
                              St. Patrick’s Inst
                            </span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li className="widget-results__item">
                      <div className="widget-results__header justify-content-center">
                        <div className="widget-results__title">Friday, March 4</div>
                      </div>
                      <div className="widget-results__content">
                        <div className="widget-results__team widget-results__team--first">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/samples/logos/ocean_kings_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              Ocean Kings
                            </h5>
                            <span className="widget-results__team-info">
                              Bay College
                            </span>
                          </div>
                        </div>
                        <div className="widget-results__result">
                          <div className="widget-results__score">
                            <span className="widget-results__score-loser">2</span> -{" "}
                            <span className="widget-results__score-winner">3</span>
                            <div className="widget-results__status">Home</div>
                          </div>
                        </div>
                        <div className="widget-results__team widget-results__team--second">
                          <figure className="widget-results__team-logo">
                            <img
                              src="assets/images/soccer/logos/alchemists_s_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="widget-results__team-details">
                            <h5 className="widget-results__team-name">
                              Alchemists
                            </h5>
                            <span className="widget-results__team-info">
                              Eric Bros School
                            </span>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </aside>
              <aside className="widget card widget--sidebar widget-newslog">
                <div className="widget__title card__header">
                  <h4>Team Newslog</h4>
                </div>
                <div className="widget__content card__content">
                  <ul className="newslog">
                    <li className="newslog__item newslog__item--injury">
                      <div className="newslog__item-inner">
                        <div className="newslog__content">
                          <strong>Mark Johnson</strong> has a tibia fracture and
                          he’s gonna be out of the play for{" "}
                          <strong>2 months</strong>.
                        </div>
                        <time className="newslog__date" dateTime="2016-01-19">
                          September 18, 2016
                        </time>
                      </div>
                    </li>
                    <li className="newslog__item newslog__item--join">
                      <div className="newslog__item-inner">
                        <div className="newslog__content">
                          <strong>Dominick R. Blink</strong>, a new{" "}
                          <strong>Shooting Guard</strong> from San Francisco has
                          joined the team!.
                        </div>
                        <time className="newslog__date" dateTime="2016-01-19">
                          September 6, 2016
                        </time>
                      </div>
                    </li>
                    <li className="newslog__item newslog__item--join">
                      <div className="newslog__item-inner">
                        <div className="newslog__content">
                          <strong>Christopher Grifin</strong>, a new{" "}
                          <strong>Power Forward</strong> from Rockstar Bay
                          College has joined the team!.
                        </div>
                        <time className="newslog__date" dateTime="2016-01-19">
                          August 24, 2016
                        </time>
                      </div>
                    </li>
                    <li className="newslog__item newslog__item--injury">
                      <div className="newslog__item-inner">
                        <div className="newslog__content">
                          <strong>Thomas Durry</strong> has a composed hand
                          fracture and he won’t be playing untill{" "}
                          <strong>September</strong>.
                        </div>
                        <time className="newslog__date" dateTime="2016-01-19">
                          August 17, 2016
                        </time>
                      </div>
                    </li>
                    <li className="newslog__item newslog__item--exit">
                      <div className="newslog__item-inner">
                        <div className="newslog__content">
                          <strong>Jeremy Rittersen</strong> left the team after
                          2 year in the club and his new house are the{" "}
                          <strong>Clovers</strong>.
                        </div>
                        <time className="newslog__date" dateTime="2016-01-19">
                          August 12, 2016
                        </time>
                      </div>
                    </li>
                  </ul>
                </div>
              </aside>
              <aside className="widget card widget--sidebar widget-games-history">
                <div className="widget__title card__header card__header--has-legend">
                  <h4>Games History</h4>
                  <div
                    id="gamesHistoryLegendSoccer"
                    className="chart-legend chart-legend--games-history"
                  >
                    <ul className="0-legend">
                      <li>
                        <span style={{backgroundColor:'#c2ff1f'}}></span>WON
                      </li>
                      <li>
                        <span style={{backgroundColor:'#38a9ff'}}></span>LOST
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="widget__content card__content">
                  <iframe
                    className="chartjs-hidden-iframe"
                    tabIndex="-1"
                    style={{display: 'block', overflow: 'hidden', border: '0px', margin: '0px', inset: '0px', height: '100%', width: '100%', position: 'absolute', pointerEvents: 'none', zIndex: -1}}
                  ></iframe>
                  <canvas
                    id="games-history-soccer"
                    className="games-history-chart"
                    height="252"
                    width="329"
                    style={{display: 'block', height: '252px', width: '329px'}}
                  ></canvas>
                </div>
              </aside>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default MyClub;
