import React, { useState, useEffect, useMemo } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Slider from "../../components/Slider";
import { useMoralis } from "react-moralis";
import defaultProfileImage from "../../assets/images/avatar-empty.png";
import { toast } from "react-toastify";
import countryList from "react-select-country-list";
import "react-toastify/dist/ReactToastify.css";
import load from "../../assets/images/load.gif";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";

toast.configure();
const options = {
  autoClose: 500,
  hideProgressBar: true,
  position: "bottom-right",
  theme: "dark",
};

const Home = () => {
  const { user, Moralis } = useMoralis();

  const [userData, setUserData] = useState([]);
  const [clubData, setClubData] = useState([]);
  const [isNewClub, setIsNewClub] = useState(false);
  const [profileImage, setProfileImage] = useState();
  const [profileImageTemp, setProfileImageTemp] = useState();
  const [clubLogo, setClubLogo] = useState();
  const [clubLogoTemp, setClubLogoTemp] = useState();
  const [clubColor1, setClubColor1] = useState();
  const [clubColor2, setClubColor2] = useState();
  const [clubColor3, setClubColor3] = useState();

  const [loading, setLoading] = useState(false);

  const countries = useMemo(() => countryList().getData(), []);

  useEffect(() => {
    Moralis.User.current()
      .fetch()
      .then((user) => {
        if (user) {
          getUserData(user);
          getClubData(user);
        }
      });
  }, []);

  const getUserData = async (user) => {
    const query = new Moralis.Query("UserInfo");
    query.equalTo("userId", user);
    const result = await query.first();
    setUserData({
      ...userData,
      username: result.get("username"),
      profileImage: result.get("photo")
        ? result.get("photo").url()
        : defaultProfileImage,
    });
  };
  const getClubData = async (user) => {
    const userQuery = new Moralis.Query("UserInfo");
    userQuery.equalTo("userId", user);
    const userResult = await userQuery.first();
    const query = new Moralis.Query("ClubInfo");
    query.equalTo("userId", userResult);
    const result = await query.first();
    if (result) {
      setIsNewClub(false);
      setClubData({
        ...clubData,
        name: result.get("name"),
        logo: result.get("logo")
          ? result.get("logo").url()
          : defaultProfileImage,
        country: result.get("country"),
        countryCode: result.get("countryCode"),
        slogan: result.get("slogan"),
        nickname: result.get("nickname"),
        stockSelling: result.get("stockSelling"),
        clubColor1: result.get("clubColor1"),
        clubColor2: result.get("clubColor2"),
        clubColor3: result.get("clubColor3")
      });
    } else {
      setIsNewClub(true);
      setClubData({
        ...clubData,
        name: "",
        logo: defaultProfileImage,
        country: "",
        countryCode: "",
        slogan: "",
        nickname: "",
        stockSelling: false,
      });
    }
  };

  const handleSubmitForm = async (event) => {
    setLoading(true);
    event.preventDefault();
    const data = new FormData(event.target);

    const query = new Moralis.Query("UserInfo");
    query.equalTo("userId", user);
    const userResult = await query.first();
    userResult.set("username", data.get("username"));
    userResult.set("photo", profileImage);
    userResult.save(null, { useMasterKey: true }).then(
      (res) => {
        setLoading(false);
      },
      (error) => {
        setLoading(false);
      }
    );
    if (isNewClub) {
      const Club = Moralis.Object.extend("ClubInfo");
      const club = new Club();
      club.set("userId", userResult);
      club.set("name", data.get("clubName"));
      club.set("countryCode", data.get("country").split("-")[0]);
      club.set("country", data.get("country").split("-")[1]);
      club.set("slogan", data.get("slogan"));
      club.set("nickname", data.get("nickname"));
      club.set("logo", clubLogo);
      club.set("stockSelling", clubData.stockSelling);
      club.set("clubColor1", clubColor1);
      club.set("clubColor2", clubColor2);
      club.set("clubColor3", clubColor3);

      club.save(null, { useMasterKey: true }).then(
        (res) => {
          userResult.set("club", res);
          userResult.save(null, { useMasterKey: true }).then(
            (res) => {
              toast.success("Profile successfully updated!", options);
              setIsNewClub(false);
              setLoading(false);
            },
            (error) => {
              toast.error("Profile update unsuccessful!", options);
              setIsNewClub(false);
              setLoading(false);
            }
          );
        },
        (error) => {
          toast.error("Profile update unsuccessful!", options);
          setLoading(false);
        }
      );
    } else {
      const clubQuery = new Moralis.Query("ClubInfo");
      clubQuery.equalTo("userId", userResult);
      const club = await clubQuery.first();
      club.set("userId", userResult);
      club.set("name", data.get("clubName"));
      club.set("countryCode", data.get("country").split("-")[0]);
      club.set("country", data.get("country").split("-")[1]);
      club.set("slogan", data.get("slogan"));
      club.set("nickname", data.get("nickname"));
      club.set("logo", clubLogo);
      club.set("stockSelling", clubData.stockSelling);
      club.set("clubColor1", clubColor1);
      club.set("clubColor2", clubColor2);
      club.set("clubColor3", clubColor3);
      club.save(null, { useMasterKey: true }).then(
        (res) => {
          toast.success("Profile successfully updated!", options);
          setLoading(false);
        },
        (error) => {
          toast.error("Profile update unsuccessful!", options);
          setLoading(false);
        }
      );
    }
  };
  const handleProfilePicture = async (event) => {
    let file = event.target.files[0];
    const moralisFile = new Moralis.File(file.name, file);
    setProfileImage(moralisFile);
    let reader = new FileReader();
    reader.onloadend = () => {
      setProfileImageTemp(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleClubLogo = async (event) => {
    let file = event.target.files[0];
    const moralisFile = new Moralis.File(file.name, file);
    setClubLogo(moralisFile);
    let reader = new FileReader();
    reader.onloadend = () => {
      setClubLogoTemp(reader.result);
    };
    reader.readAsDataURL(file);
  };
  return (
    <div className="App">
      <Header />
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="card card--lg">
                <div className="card__header">
                  <h4>Personal Information</h4>
                </div>
                <div className="card__content">
                  <form
                    action="#"
                    className="df-personal-info"
                    onSubmit={handleSubmitForm}
                  >
                    <div className="row">
                      <div className="form-group form-group--upload col-md-6">
                        <div className="form-group__avatar">
                          <img
                            src={
                              userData.profileImage
                                ? profileImageTemp
                                  ? profileImageTemp
                                  : userData.profileImage
                                : defaultProfileImage
                            }
                            alt=""
                            style={{ height: "100px", width: "100px" }}
                          />
                          <div className="form-group__label">
                            <h6>Profile Photo</h6>
                            <span>Minimum size 60x60px</span>
                          </div>
                        </div>
                        <div className="form-group__upload">
                          <label className="btn btn-default upload-btn">
                            Upload Image{" "}
                            <input
                              id="tf-upload-img"
                              type="file"
                              name="avatar"
                              style={{ display: "none" }}
                              onChange={(event) => {
                                handleProfilePicture(event);
                              }}
                            />
                          </label>
                        </div>
                      </div>
                      <div className="form-group form-group--upload col-md-6">
                        <div className="form-group__avatar">
                          <img
                            src={
                              clubData.logo
                                ? clubLogoTemp
                                  ? clubLogoTemp
                                  : clubData.logo
                                : defaultProfileImage
                            }
                            alt=""
                            style={{ height: "100px", width: "100px" }}
                          />
                          <div className="form-group__label">
                            <h6>Club Logo</h6>
                            <span>Minimum size 60x60px</span>
                          </div>
                        </div>
                        <div className="form-group__upload">
                          <label className="btn btn-default upload-btn">
                            Upload Image{" "}
                            <input
                              id="tf-upload-img"
                              type="file"
                              name="logo"
                              style={{ display: "none" }}
                              onChange={(event) => {
                                handleClubLogo(event);
                              }}
                            />
                          </label>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="account-username">Username</label>{" "}
                          <input
                            type="text"
                            className="form-control"
                            name="username"
                            id="username"
                            defaultValue={userData.username}
                            placeholder="Username"
                            required
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="account-username">Club Name</label>{" "}
                          <input
                            type="text"
                            className="form-control"
                            name="clubName"
                            id="clubName"
                            defaultValue={clubData.name}
                            placeholder="Club Name"
                            required
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="account-country">Country</label>{" "}
                          <select
                            name="country"
                            id="country"
                            className="form-control"
                            required
                            value={
                              clubData.countryCode + "-" + clubData.country
                            }
                            onChange={(event) =>
                              setClubData({
                                ...clubData,
                                country: event.target.value.split("-")[1],
                                countryCode: event.target.value.split("-")[0],
                              })
                            }
                            style={{ color: "#31404b" }}
                          >
                            <option value="">Select your country</option>
                            {countries.map((data, index) => (
                              <option
                                value={data.value + "-" + data.label}
                                key={index}
                              >
                                {data.label}
                              </option>
                            ))}
                          </select>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="slogan">Slogan</label>{" "}
                          <input
                            type="text"
                            className="form-control"
                            name="slogan"
                            id="slogan"
                            defaultValue={clubData.slogan}
                            placeholder="Enter your club slogan"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="account-first-name">Nickname</label>{" "}
                          <input
                            type="text"
                            className="form-control"
                            name="nickname"
                            id="nickname"
                            defaultValue={clubData.nickname}
                            placeholder="Nickname"
                          />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="account-share">Stock Sale</label>{" "}
                          <select
                            name="share"
                            id="share"
                            className="form-control"
                            required
                            value={clubData.stockSelling == true ? "Yes" : "No"}
                            onChange={(event) =>
                              setClubData({
                                ...clubData,
                                stockSelling:
                                  event.target.value == "Yes" ? true : false,
                              })
                            }
                            style={{ color: "#31404b" }}
                          >
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <div className="row">
                            <div className="col-md-3">
                              <label htmlFor="account-clubcolor">
                                Club Colors  :
                              </label>{" "}
                            </div>
                            <div className="col-md-3">
                              <input
                                className="club-color-select"
                                type="color"
                                id="clubColor1"
                                name="clubColor1"
                                value={clubData.clubColor1}
                                onChange={(event) =>
                                  setClubColor1(event.target.value)
                                }
                                defaultValue="#e66465"
                              />
                            </div>
                            <div className="col-md-3">
                              <input
                                className="club-color-select"
                                type="color"
                                id="clubColor2"
                                name="clubColor2"
                                value={clubData.clubColor2}
                                onChange={(event) =>
                                  setClubColor2(event.target.value)
                                }
                                defaultValue="#253da1"
                              />
                            </div>
                            <div className="col-md-3">
                              <input
                                className="club-color-select"
                                type="color"
                                id="clubColor3"
                                name="clubColor3"
                                value={clubData.clubColor3}
                                onChange={(event) =>
                                  setClubColor3(event.target.value)
                                }
                                defaultValue="#f6b73c"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form-group--submit">
                      {!loading ? (
                        <div className="btn-auction">
                          <button
                            type="submit"
                            className="btn btn-default btn-lg btn-block general-save"
                          >
                            Save
                          </button>
                        </div>
                      ) : (
                        <button
                          className="btn btn-default btn-lg btn-block general-save"
                          type="submit"
                          disabled
                        >
                          <img src={load} className="loader" alt="loading..." />
                        </button>
                      )}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
