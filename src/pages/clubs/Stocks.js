import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMoralis } from "react-moralis";
import Modal from "react-modal";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
const Stocks = (props) => {
  const { Moralis } = useMoralis();
  const [stockSales, setStockSales] = useState([]);
  const { id } = useParams();
  const [modalIsOpen, setIsOpen] = useState(false);
  const [clubData, setClubData] = useState([]);
  function openModal() {
    setIsOpen(true);
  }
  function closeModal() {
    setIsOpen(false);
  }

  const buyStock = async (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
  };

  async function getStockSales() {
    const query = new Moralis.Query("ShareSales");
    query.equalTo("clubId", props.clubId);
    const result = await query.find();
    if(result.length > 0) {
        setStockSales({
            ...stockSales,
            id: result.id,
            userId: result.get("userId"),
            clubId: result.get("clubId"),
            amount: result.get("amount"),
            payed: result.get("payed"),
            profit: result.get("profit")
          });
    }
  }

  const getClubData = async () => {
    const clubDetail = Moralis.Object.extend("ClubInfo");
    const query = new Moralis.Query(clubDetail);
    query.equalTo("objectId", props.clubId);
    const result = await query.first();
    setClubData({
        ...clubData,
        stockSelling: result.get("stockSelling")
      });
  };

  useEffect(() => {
    getClubData();   
    getStockSales();
  }, []);

  return (
    <div className="site-content">
      <div className="container">
        { clubData.stockSelling == true ? <button
          type="submit"
          onClick={() => openModal()}
          className="btn-social-counter btn-social-counter--twitter"
        >
          <div className="btn-social-counter__icon">
            <i className="fas fa-gavel"></i>
          </div>
          <h6 className="btn-social-counter__title">Buy a Stock</h6>
          <span className="btn-social-counter__count">
            <span className="btn-social-counter__count-num"></span> Buy stock,
            share in profit!
          </span>{" "}
          <span className="btn-social-counter__add-icon"></span>
        </button> :
        <button
        type="submit"
        onClick={() => openModal()}
        className="btn-social-counter btn-social-counter--twitter disabled-a"
      >
        <div className="btn-social-counter__icon" style={{ backgroundColor: "darkgrey" }}>
          <i className="fas fa-gavel"></i>
        </div>
        <h6 className="btn-social-counter__title">Buy a Stock</h6>
        <span className="btn-social-counter__count">
          <span className="btn-social-counter__count-num"></span> Buy stock,
          share in profit!
        </span>{" "}
        <span className="btn-social-counter__add-icon"></span>
      </button>  }
      <br />
        <div className="card card--has-table">
          <div className="card__header">
            <h4>Stock Sales</h4>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table-hover team-schedule team-schedule--full">
                <thead>
                  <tr>
                    <th className="team-schedule__date">Date</th>
                    <th className="team-schedule__versus">User</th>
                    <th className="team-schedule__status">Amount</th>
                    <th className="team-schedule__time">Payed</th>
                    <th className="team-schedule__compet">Percent</th>
                  </tr>
                </thead>
                <tbody>
                  {stockSales.length == 0 && (
                    <tr>
                      <td
                        className="result__date no-results-found"
                        colSpan="11"
                      >
                        No results found!
                      </td>
                    </tr>
                  )}
                  {stockSales.map((data, index) => (
                    <tr>
                      <td className="team-schedule__date">Saturday, Mar 24</td>
                      <td className="team-schedule__versus">
                        <div className="team-meta">
                          <figure className="team-meta__logo">
                            <img
                              src="/assets/images/samples/logos/lucky_clovers_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="team-meta__info">
                            <h6 className="team-meta__name">Lucky Clovers</h6>
                            <span className="team-meta__place">
                              St. Patrick’s Institute
                            </span>
                          </div>
                        </div>
                      </td>
                      <td className="team-schedule__status">Away</td>
                      <td className="team-schedule__time">9:00PM EST</td>
                      <td className="team-schedule__compet">
                        West Bay Playoffs 2018
                      </td>
                      <td className="team-schedule__venue">
                        Madison Cube Stadium
                      </td>
                      <td className="team-schedule__tickets">
                        <a
                          href="#"
                          className="btn btn-xs btn-default btn-outline btn-block"
                        >
                          Buy Game Tickets
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {stockSales.length > 0 && (
          <nav className="team-pagination" aria-label="Team navigation">
            <ul className="pagination pagination--condensed pagination--lg justify-content-center">
              <li className="page-item active">
                <a className="page-link" href="#">
                  1
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  2
                </a>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  3
                </a>
              </li>
              <li className="page-item">
                <span className="page-link">...</span>
              </li>
              <li className="page-item">
                <a className="page-link" href="#">
                  16
                </a>
              </li>
            </ul>
          </nav>
        )}
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h5 style={{ textAlign: "center" }}>Buy Stock</h5>
          <div className="card__content">
            <form action="#" className="df-personal-info" onSubmit={buyStock}>
              <div className="col-md-12">
                <div className="form-group">
                  <label htmlFor="lot">
                    Enter the number of lots you want to buy.
                  </label>{" "}
                  <input
                    type="text"
                    className="form-control"
                    name="lot"
                    id="lot"
                    placeholder="Lot"
                    required
                  />
                </div>
              </div>

              <div className="form-group--submit">
                <div className="row">
                  <div className="btn-auction col-md-6">
                    <button
                      onClick={() => closeModal()}
                      className="btn btn-default btn-lg btn-block general-save"
                    >
                      Close
                    </button>
                  </div>
                  <div className="btn-auction col-md-6">
                    <button
                      type="submit"
                      className="btn btn-default btn-lg btn-block general-save"
                    >
                      Save
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </Modal>
      </div>
    </div>
  );
};

export default Stocks;
