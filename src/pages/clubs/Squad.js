import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMoralis } from "react-moralis";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";

const Squad = (props) => {
  const { Moralis } = useMoralis();
  const [squad, setSquad] = useState([]);

  const { id } = useParams();

  async function getSquad() {
    var Club = Moralis.Object.extend("ClubInfo");
    var pointerToClub = new Club();
    pointerToClub.id = props.clubId;

    const squadList = Moralis.Object.extend("ClubPlayers");
    const query = new Moralis.Query(squadList);
    query.equalTo("club", pointerToClub);
    query.include("player");
    const results = await query.find();
    setSquad(results);
  }

  useEffect(() => {
    getSquad();
  }, []);

  return (
    <div className="site-content">
      <div className="container">
        {squad.length > 0 && (
          <div className="team-roster team-roster--card-compact js-team-roster--card-compact row slick-initialized slick-slider">
            <Swiper
              breakpoints={{
                0: {
                  slidesPerView: 1,
                },
                767: {
                slidesPerView: 2,
                },
                991: {
                slidesPerView: 3,
                },
                1300: {
                  slidesPerView: 3,
                },
            }}
              spaceBetween={10}
              pagination={{
                clickable: true,
              }}
              modules={[Navigation, Pagination, Scrollbar, A11y]}
              className="mySwiper"
            >
              <div className="slick-list draggable">
                <div
                  className="slick-track"
                  style={{
                    opacity: "1",
                    width: "1980px",
                    transform: "translate3d(0px, 0px, 0px)",
                  }}
                >
                  {squad.map((data, index) => (
                    <SwiperSlide key={`swiper-${index}`}>
                      <div className="team-roster__item card">
                        <div className="card__content">
                          <div className="team-roster__content-wrapper">
                            <figure className="team-roster__player-img">
                              <img
                                src={`https://ipfs.moralis.io:2053/ipfs/${data.get("player").get("photo")}`}
                                alt=""
                                style={{ display: "inline" }}
                              />
                            </figure>
                            <div className="team-roster__player-info">
                              <h3 className="team-roster__player-name">
                                {/*<span className="team-roster__player-first-name">
                                James
                              </span>{" "}*/}
                                <span className="team-roster__player-first-name">
                                  {data.get("player").get("short_name")}
                                </span>
                              </h3>
                            </div>
                            <div
                              className="team-roster__player-details card"
                              style={{ flexDirection: "column" }}
                            >
                              <div className="team-roster__player-details-body">
                                <div className="progress-stats progress-stats--top-labels">
                                  <div className="progress__label">
                                    Overall
                                  </div>
                                  <div className="progress">
                                    <div
                                      className="progress__bar progress__bar--success"
                                      style={{ width: data.get("player").get("overall") + "%" }}
                                      role="progressbar"
                                      aria-valuenow={data.get("player").get("overall")}
                                      aria-valuemin="0"
                                      aria-valuemax="100"
                                    ></div>
                                  </div>
                                  <div className="progress__number">{data.get("player").get("overall")}%</div>
                                </div>
                                <div className="progress-stats progress-stats--top-labels">
                                  <div className="progress__label">
                                    Potential
                                  </div>
                                  <div className="progress">
                                    <div
                                      className="progress__bar progress__bar--success"
                                      style={{ width: data.get("player").get("potential") + "%" }}
                                      role="progressbar"
                                      aria-valuenow={data.get("player").get("potential")}
                                      aria-valuemin="0"
                                      aria-valuemax="100"
                                    ></div>
                                  </div>
                                  <div className="progress__number">{data.get("player").get("potential")}%</div>
                                </div>
                                <div className="progress-stats progress-stats--top-labels">
                                  <div className="progress__label">
                                    Best Overall Rating
                                  </div>
                                  <div className="progress">
                                    <div
                                      className="progress__bar progress__bar--success"
                                      style={{ width: data.get("player").get("best_overall_rating") + "%" }}
                                      role="progressbar"
                                      aria-valuenow={data.get("player").get("best_overall_rating")}
                                      aria-valuemin="0"
                                      aria-valuemax="100"
                                    ></div>
                                  </div>
                                  <div className="progress__number">{data.get("player").get("best_overall_rating")}%</div>
                                </div>
                              </div>
                              <aside className="team-roster__meta">
                                <div className="row">
                                  <div className="col-md-3">
                                    <div className="team-roster__meta-item">
                                      <div className="team-roster__meta-value">
                                        0
                                      </div>
                                      <div className="team-roster__meta-label">
                                        Goals
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-3">
                                    <div className="team-roster__meta-item">
                                      <div className="team-roster__meta-value">
                                        0
                                      </div>
                                      <div className="team-roster__meta-label">
                                        Games
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-3">
                                    <div className="team-roster__meta-item">
                                      <div className="team-roster__meta-value">
                                      {data.get("player").get("best_position")}
                                      </div>
                                      <div className="team-roster__meta-label">
                                        Posit.
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-3">
                                    <div className="team-roster__meta-item">
                                      <div className="team-roster__meta-value">
                                        {data.get("player").get("preferred_foot")}
                                      </div>
                                      <div className="team-roster__meta-label">
                                        Foot
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </aside>
                            </div>
                            <div className="team-roster__player-number">{index + 1}</div>
                          </div>
                        </div>
                      </div>
                    </SwiperSlide>
                  ))}
                </div>
              </div>
            </Swiper>
          </div>
        )}
        <div className="card card--has-table">
          <div className="card__header">
            <h4>Complete Squad</h4>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table--lg team-roster-table">
                <thead>
                  <tr>
                    <th className="team-roster-table__number">NBR</th>
                    <th className="team-roster-table__position">Pos</th>
                    <th className="team-roster-table__name">Player Name</th>
                    <th className="team-roster-table__foot">Foot</th>
                    <th className="team-roster-table__age">Age</th>
                    <th className="team-roster-table__height">Height</th>
                    <th className="team-roster-table__weight">Weight</th>
                    <th className="team-roster-table__goals">Goals</th>
                    <th className="team-roster-table__assists">Assists</th>
                    <th className="team-roster-table__fouls">Fouls</th>
                    <th className="team-roster-table__card-y">Y.CRDS</th>
                    <th className="team-roster-table__card-r">R.CRDS</th>
                  </tr>
                </thead>
                <tbody>
                  {squad.length == 0 && (
                    <tr>
                      <td
                        className="team-roster-table__number no-results-found"
                        colSpan="12"
                      >
                        No results found!
                      </td>
                    </tr>
                  )}
                  {squad.map((data, index) => (
                    <tr key={`tr-${index}`}>
                      <td className="team-roster-table__number">{ index + 1 }</td>
                      <td className="team-roster-table__position">
                        {data.get("player").get("best_position")}
                      </td>
                      <td className="team-roster-table__name">
                        {data.get("player").get("short_name")}
                      </td>
                      <td className="team-roster-table__foot">
                        {data.get("player").get("preferred_foot")}
                      </td>
                      <td className="team-roster-table__age">
                        {data.get("player").get("age")}
                      </td>
                      <td className="team-roster-table__height">
                        {data.get("player").get("height")} cm
                      </td>
                      <td className="team-roster-table__weight">
                        {data.get("player").get("weight")} kg
                      </td>
                      <td className="team-roster-table__goals">0</td>
                      <td className="team-roster-table__assists">0</td>
                      <td className="team-roster-table__fouls">0</td>
                      <td className="team-roster-table__card-y">0</td>
                      <td className="team-roster-table__card-r">0</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Squad;
