import React, { useMemo, useState, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import countryList from 'react-select-country-list'
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import ClubCard from "../../components/shared/ClubCard";
import Slider from "../../components/Slider";
import { useMoralis } from "react-moralis";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";

const Clubs = () => {
  const { Moralis } = useMoralis();


  const [clubs, setClubs] = useState([]);
  
  const countries = useMemo(() => countryList().getData(), []);

  const [clubName, setClubName] = useState('');
  const [countryChoice, setCountryChoice] = useState(false);
  
  const [selectedCountry, setSelectedCountry] = useState("");



  async function getClubs() {
    const clubList = Moralis.Object.extend("ClubInfo");
    const query = new Moralis.Query(clubList);
    if(clubName) {
        query.fullText("name", clubName);
    }
    if(selectedCountry) {
      query.equalTo("countryCode", selectedCountry);
    }
    /*
    switch(selectedPosition) {
      case 1:
        query.equalTo("positions", "GK");
        break;
      case 2:
        query.containedIn("positions", ["RWB", "RB", "CB", "LB", "LWB"]);
        break;
      case 3:
        query.containedIn("positions", ["CDM", "RM", "CM", "LM", "CAM"]);
        break;
      case 4:
        query.containedIn("positions", ["RF", "CF", "LF", "RW", "ST", "LW"])
        break;
      default:
        break;
    }
    switch(selectedOrder) {
      case 0:
        query.descending("market_value");
        break;
      case 1:
        query.ascending("market_value");
        break;
      case 2:
        query.descending("age");
        break;
      case 3:
        query.ascending("age");
        break;
      default:
        break;
    }*/
    
    query.limit(30);
    const results = await query.find();
    setClubs(results);
  }

  useEffect(() => {
    getClubs();
  }, []);

  return (
    <div className="App">
      <Header />
      <div className="post-filter">
        <div className="container">
          <div className="post-filter__form clearfix">
            <div className="post-filter__select">
              <label className="post-filter__label">Country</label>{" "}
              <div className={`cs-select cs-skin-border ${countryChoice ? "cs-active" : ""}`} tabIndex="0" onClick={() => { setCountryChoice(!countryChoice) }}>
                <span className="cs-placeholder">{selectedCountry ? selectedCountry : "ALL COUNTRIES"}</span>
                <div className="cs-options">
                  <ul className="country-select-list">
                    <li data-option="" data-value="" onClick={() => { setSelectedCountry("") }} key={`country-all`}>
                      <span>All Countries</span>
                    </li>
                    {countries.map((data,index) => (
                    selectedCountry != data.value && <li data-option="" data-value={data.value} onClick={() => { setSelectedCountry(data.value) }} key={`country-${data.value}`}>
                      <span>{data.label}</span>
                    </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
            {/*<div className="post-filter__select">
              <label className="post-filter__label">Position</label>{" "}
              <div className={`cs-select cs-skin-border ${positionChoice ? "cs-active" : ""}`} tabIndex="0" onClick={() => { setPositionChoice(!positionChoice); setStatusChoice(false); setOrderChoice(false) }}>
                <span className="cs-placeholder">{positionList[selectedPosition].title}</span>
                <div className="cs-options">
                  <ul>
                    {positionList.map((data,index) => (
                    selectedPosition != data.id && <li data-option="" data-value={data.id} onClick={() => { setSelectedPosition(data.id) }} key={`position-${data.id}`}>
                      <span>{data.title}</span>
                    </li>
                    ))}
                  </ul>
                </div>
                <select className="cs-select cs-skin-border">
                  {positionList.map((data,index) => (
                  selectedPosition != data.id && <option value={data.id} key={`position-option-${data.id}`}>
                    {data.title}
                  </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="post-filter__select">
              <label className="post-filter__label">Order</label>{" "}
              <div className={`cs-select cs-skin-border ${orderChoice ? "cs-active" : ""}`} tabIndex="0" onClick={() => { setOrderChoice(!orderChoice); setStatusChoice(false); setPositionChoice(false) }}>
                <span className="cs-placeholder">{orderList[selectedOrder].title}</span>
                <div className="cs-options">
                  <ul>
                  {orderList.map((data,index) => (
                    selectedOrder != data.id && <li data-option="" data-value={data.id} onClick={() => { setSelectedOrder(data.id) }} key={`order-${data.id}`}>
                      <span>{data.title}</span>
                    </li>
                    ))}
                  </ul>
                </div>
                <select className="cs-select cs-skin-border">
                  {orderList.map((data,index) => (
                  selectedOrder != data.id && <option value={data.id} key={`order-option-${data.id}`}>
                    {data.title}
                  </option>
                  ))}
                </select>
              </div>
            </div>*/}
            <div className="post-filter__select">
              <label className="post-filter__label">Name</label>{" "}
              <div className="cs-select cs-skin-border" tabIndex="0">
                <input
                  type="text"
                  className="form-control search-player-name-input"
                  name="account-email"
                  id="account-email"
                  placeholder="Club Name"
                  value={clubName}
                  onChange={(event) => {setClubName(event.target.value)}}
                />
              </div>
            </div>
            <div className="post-filter__submit">
              <button
                type="button"
                className="btn btn-default btn-lg btn-block"
                onClick={getClubs}
              >
                Filter Clubs
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="site-content">
        <div className="container">
          <div className="row">
            {clubs.map((item, index) => (
              <ClubCard key={index} data={item}/>
            ))}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Clubs;
