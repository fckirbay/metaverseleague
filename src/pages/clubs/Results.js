import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMoralis } from "react-moralis";

const Results = () => {
  const { Moralis } = useMoralis();
  const [results, setResults] = useState([]);
  const { id } = useParams();

  useEffect(() => {}, []);

  return (
    <div className="site-content">
      <div className="container">
        <div className="card card--has-table">
          <div className="card__header card__header--has-btn">
            <h4>Latest Results</h4>
            <ul className="team-result-filter">
              {/*<li className="team-result-filter__item">
                <select className="form-control input-xs">
                  <option>Game Date</option>
                  <option>December 2016</option>
                  <option>November 2016</option>
                  <option>October 2016</option>
                  <option>September 2016</option>
                  <option>August 2016</option>
                  <option>July 2016</option>
                  <option>June 2016</option>
                  <option>May 2016</option>
                  <option>April 2016</option>
                  <option>March 2016</option>
                  <option>February 2016</option>
                  <option>January 2016</option>
                </select>
              </li>*/}
              <li className="team-result-filter__item">
                <select className="form-control input-xs">
                  <option>Ascending</option>
                  <option>Descending</option>
                </select>
              </li>
              <li className="team-result-filter__item">
                <button
                  type="submit"
                  className="btn btn-default btn-outline btn-xs card-header__button"
                >
                  Filter Latest Results
                </button>
              </li>
            </ul>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table-hover team-result">
                <thead>
                  <tr>
                    <th className="team-result__date">Date</th>
                    <th className="team-result__vs">Versus</th>
                    <th className="team-result__score">Score</th>
                    <th className="team-result__status">Status</th>
                    <th className="team-result__ball-possession">
                      Ball Possession
                    </th>
                    <th className="team-result__shots">Shots (on goal)</th>
                    <th className="team-result__fouls">Total Fouls</th>
                    <th className="team-result__game-overview">Game Overview</th>
                  </tr>
                </thead>
                <tbody>
                  {results.length == 0 && (
                    <tr>
                      <td
                        className="result__date no-results-found"
                        colSpan="11"
                      >
                        No results found!
                      </td>
                    </tr>
                  )}
                  {results.map((data, index) => (
                    <tr>
                      <td className="team-result__date">Sat, Mar 10</td>
                      <td className="team-result__vs">
                        <div className="team-meta">
                          <figure className="team-meta__logo">
                            <img
                              src="/assets/images/samples/logos/sharks_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="team-meta__info">
                            <h6 className="team-meta__name">Sharks</h6>
                            <span className="team-meta__place">Marine College</span>
                          </div>
                        </div>
                      </td>
                      <td className="team-result__score">
                        <span className="team-result__game">W</span> 2-0
                      </td>
                      <td className="team-result__status">Home</td>
                      <td className="team-result__ball-possession">62% - 38%</td>
                      <td className="team-result__shots">25(14) - 16(6)</td>
                      <td className="team-result__fouls">12 - 14</td>
                      <td className="team-result__game-overview">
                        <a
                          href="#"
                          className="btn btn-xs btn-default btn-outline btn-block"
                        >
                          See Full Overview
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Results;
