import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMoralis } from "react-moralis";

const Fixture = () => {
  const { Moralis } = useMoralis();
  const [fixture, setFixture] = useState([]);
  const { id } = useParams();

  useEffect(() => {}, []);

  return (
    <div className="site-content">
      <div className="container">
        <div className="card card--has-table">
          <div className="card__header">
            <h4>Schedule and Tickets</h4>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table-hover team-schedule team-schedule--full">
                <thead>
                  <tr>
                    <th className="team-schedule__date">Date</th>
                    <th className="team-schedule__versus">Versus</th>
                    <th className="team-schedule__status">Status</th>
                    <th className="team-schedule__time">Time</th>
                    <th className="team-schedule__compet">Competition</th>
                    <th className="team-schedule__venue">Venue</th>
                    <th className="team-schedule__tickets">Tickets</th>
                  </tr>
                </thead>
                <tbody>
                {fixture.length == 0 && (
                <tr>
                  <td className="result__date no-results-found" colSpan="11">
                    No results found!
                  </td>
                </tr>
              )}
                {fixture.map((data, index) => (
                  <tr>
                    <td className="team-schedule__date">Saturday, Mar 24</td>
                    <td className="team-schedule__versus">
                      <div className="team-meta">
                        <figure className="team-meta__logo">
                          <img
                            src="/assets/images/samples/logos/lucky_clovers_shield.png"
                            alt=""
                          />
                        </figure>
                        <div className="team-meta__info">
                          <h6 className="team-meta__name">Lucky Clovers</h6>
                          <span className="team-meta__place">
                            St. Patrick’s Institute
                          </span>
                        </div>
                      </div>
                    </td>
                    <td className="team-schedule__status">Away</td>
                    <td className="team-schedule__time">9:00PM EST</td>
                    <td className="team-schedule__compet">
                      West Bay Playoffs 2018
                    </td>
                    <td className="team-schedule__venue">Madison Cube Stadium</td>
                    <td className="team-schedule__tickets">
                      <a
                        href="#"
                        className="btn btn-xs btn-default btn-outline btn-block"
                      >
                        Buy Game Tickets
                      </a>
                    </td>
                  </tr>))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {fixture.length > 0 && <nav className="team-pagination" aria-label="Team navigation">
          <ul className="pagination pagination--condensed pagination--lg justify-content-center">
            <li className="page-item active">
              <a className="page-link" href="#">
                1
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                2
              </a>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                3
              </a>
            </li>
            <li className="page-item">
              <span className="page-link">...</span>
            </li>
            <li className="page-item">
              <a className="page-link" href="#">
                16
              </a>
            </li>
          </ul>
        </nav>}
      </div>
    </div>
  );
};

export default Fixture;
