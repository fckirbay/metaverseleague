import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMoralis } from "react-moralis";

const Standings = () => {
  const { Moralis } = useMoralis();
  const [standings, setStandings] = useState([]);

  const { id } = useParams();

  useEffect(() => {}, []);

  return (
    <div className="site-content">
      <div className="container">
        <div className="card card--has-table">
          <div className="card__header">
            <h4>MetaverseLeague 2022</h4>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table-hover table-standings table-standings--full table-standings--full-soccer">
                <thead>
                  <tr>
                    <th className="team-standings__pos">Pos</th>
                    <th className="team-standings__team">Teams</th>
                    <th className="team-standings__played">Played</th>
                    <th className="team-standings__win">Won</th>
                    <th className="team-standings__lose">Lost</th>
                    <th className="team-standings__drawn">Drawn</th>
                    <th className="team-standings__goals-for">Goals For</th>
                    <th className="team-standings__goals-against">Goals Against</th>
                    <th className="team-standings__goals-diff">Goals Diff</th>
                    <th className="team-standings__total-points">Total Points</th>
                    <th className="team-standings__points-diff">Points Diff</th>
                  </tr>
                </thead>
                <tbody>
                  {standings.length == 0 && (
                    <tr>
                      <td
                        className="team-standings__played no-results-found"
                        colSpan="11"
                      >
                        No results found!
                      </td>
                    </tr>
                  )}
                  {standings.map((data, index) => (
                    <tr>
                      <td className="team-standings__pos">01</td>
                      <td className="team-standings__team">
                        <div className="team-meta">
                          <figure className="team-meta__logo">
                            <img
                              src="assets/images/samples/logos/pirates_shield.png"
                              alt=""
                            />
                          </figure>
                          <div className="team-meta__info">
                            <h6 className="team-meta__name">L.A. Pirates</h6>
                            <span className="team-meta__place">
                              Bebop Institute
                            </span>
                          </div>
                        </div>
                      </td>
                      <td className="team-standings__played">50</td>
                      <td className="team-standings__win">36</td>
                      <td className="team-standings__lose">14</td>
                      <td className="team-standings__drawn">10</td>
                      <td className="team-standings__goals-for">19</td>
                      <td className="team-standings__goals-against">2</td>
                      <td className="team-standings__goals-diff">17</td>
                      <td className="team-standings__total-points">118</td>
                      <td className="team-standings__points-diff">+ 15</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Standings;
