import React, { useRef, useState, useEffect } from "react";
import { useParams, Link, useLocation } from "react-router-dom";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Overview from "./Overview";
import Squad from "./Squad";
import Standings from "./Standings";
import Results from "./Results";
import Fixture from "./Fixture";
import News from "./News";
import Stocks from "./Stocks";
import { useMoralis } from "react-moralis";

const Club = () => {

  const style = {
    "&::before": {
      "backgroundImage": "linear-gradient(to right, red , yellow, white)"
    }
  };

  const { Moralis } = useMoralis();

  const { id } = useParams();
  const location = useLocation();

  const [clubId, setClubId] = useState(id);

  const getMyClub = async (user) => {
    const userQuery = new Moralis.Query("UserInfo");
    userQuery.equalTo("userId", user);
    const userResult = await userQuery.first();
    const query = new Moralis.Query("ClubInfo");
    query.equalTo("userId", userResult);
    const result = await query.first();
    setClubId(result.id)
        setClub({
        ...club,
        id: result.id,
        countryCode: result.get("countryCode"),
        logo: result.get("logo").url(),
        name: result.get("name"),
        nickname: result.get("nickname"),
        country: result.get("country"),
        slogan: result.get("slogan")
      });
  }

  const [selectedTab, setSelectedTab] = useState('overview');
  const [pageSelection, setPageSelection] = useState(false);

  const [club, setClub] = useState({
    id: null,
    countryCode: "",
    logo: "",
    name: "",
    nickname: "",
    country: "",
    slogan: ""
  });

  async function getClub() {
    const clubDetail = Moralis.Object.extend("ClubInfo");
    const query = new Moralis.Query(clubDetail);
    query.equalTo("objectId", clubId);
    const result = await query.first();
    setClub({
        ...club,
        id: result.id,
        countryCode: result.get("countryCode"),
        logo: result.get("logo").url(),
        name: result.get("name"),
        nickname: result.get("nickname"),
        country: result.get("country"),
        slogan: result.get("slogan")
      });
  }

  const changeSelectedTab = (tab) => {
    setSelectedTab(tab);
  }

  useEffect(() => {
    if(typeof id == 'undefined'){
      Moralis.User.current()
      .fetch()
      .then((user) => {
        if (user) {
          getMyClub(user);
        }
      });
    } else if(id) {
        getClub();
      }
  }, [location]);

  return (
    <div className="App">
      <Header />
     <div className="page-heading">
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              { club.logo && <img src={club.logo} className="detail-club-logo"/> }
              <h1 className="page-heading__title">
                <span className="highlight">{club.name}</span>
              </h1>
              <ol className="page-heading__breadcrumb breadcrumb">
                <li className="breadcrumb-item">
                  <Link to="#">{club.slogan}</Link>
                </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      <nav className="content-filter">
        <div className="container">
        <a className={`content-filter__toggle ${pageSelection ? "content-filter__toggle--active" : ""}`} onClick={() => setPageSelection(!pageSelection)}></a>
        <ul className={`content-filter__list ${pageSelection ? "content-filter__list--expanded" : ""}`}>
            <li className={`content-filter__item ${selectedTab == 'overview' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('overview')}
                className="content-filter__link"
                style={style}
              >
                <small>The Team</small>Overview
              </a>
            </li>
            <li className={`content-filter__item ${selectedTab == 'squad' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('squad')}
                className="content-filter__link"
                style={style}
              >
                <small>The Team</small>Squad
              </a>
            </li>
            {/*<li className={`content-filter__item ${selectedTab == 'standings' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('standings')}
                className="content-filter__link"
              >
                <small>The Team</small>Standings
              </a>
            </li>*/}
            <li className={`content-filter__item ${selectedTab == 'results' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('results')}
                className="content-filter__link"
              >
                <small>The Team</small>Latest Results
              </a>
            </li>
            <li className={`content-filter__item ${selectedTab == 'fixture' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('fixture')}
                className="content-filter__link"
              >
                <small>The Team</small>Fixture
              </a>
            </li>
            <li className={`content-filter__item ${selectedTab == 'news' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('news')}
                className="content-filter__link"
              >
                <small>The Team</small>News
              </a>
            </li>
            <li className={`content-filter__item ${selectedTab == 'stocks' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('stocks')}
                className="content-filter__link"
              >
                <small>The Team</small>Stocks
              </a>
            </li>
          </ul>
        </div>
      </nav>
      {selectedTab == 'overview' && <Overview changeTab={changeSelectedTab}/>}
      {selectedTab == 'squad' && <Squad clubId={clubId} />}
      {selectedTab == 'standings' && <Standings />}
      {selectedTab == 'results' && <Results />}
      {selectedTab == 'fixture' && <Fixture />}
      {selectedTab == 'news' && <News clubId={clubId} />}
      {selectedTab == 'stocks' && <Stocks clubId={clubId} />}
      <Footer />
    </div>
  );
};

export default Club;
