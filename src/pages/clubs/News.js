import React, { useRef, useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useMoralis } from "react-moralis";
import Moment from 'react-moment';

const News = (props) => {
  const { Moralis } = useMoralis();
  const [news, setNews] = useState([]);
  const [club, setClub] = useState([]);
  const { id } = useParams();

  async function getNews() {

    var Club = Moralis.Object.extend("ClubInfo");
    var pointerToClub = new Club();
    pointerToClub.id = props.clubId;
    setClub(pointerToClub);

    const newsList = Moralis.Object.extend("ClubNews");
    const query = new Moralis.Query(newsList);
    query.equalTo("club", pointerToClub);
    const results = await query.find();
    setNews(results);
  }


  useEffect(() => {
    getNews();
  }, []);

  return (
    <div className="site-content">
      <div className="container">
      {news.length > 0 && 
        <div
          className="posts posts--cards post-grid post-grid--fitRows row"
          style={{position: "relative", height: "1276.33px"}}
        >
      {news.map((data, index) => (
          <div
            className="post-grid__item col-sm-4"
            style={{position: "absolute", left: "0px", top: "0px"}}
            key={`news-${index}`}
          >
            <div className="posts__item posts__item--card posts__item--category-1 card">
              <figure className="posts__thumb">
                <Link to="#">
                  <img
                    src={data.get("photo").url()}
                    alt={data.get("title")}
                  />
                </Link>
              </figure>
              <div className="posts__inner card__content">
                <Link to="#" className="posts__cta"></Link>{" "}
                <time dateTime="2016-08-23" className="posts__date">
                  <Moment format="MMM DD, YYYY">
                    {data.get("createdAt")}
                  </Moment>
                </time>
                <h6 className="posts__title">
                  <Link to="#">
                    {data.get("title")}
                  </Link>
                </h6>
                <div className="posts__excerpt">
                  {data.get("text")}
                </div>
              </div>
              <footer className="posts__footer card__footer">
                <div className="post-author">
                  <figure className="post-author__avatar">
                    <img
                      src={club.get("logo").url()}
                      alt={club.get("name")}
                    />
                  </figure>
                  <div className="post-author__info">
                    <h4 className="post-author__name">{club.get("name")}</h4>
                  </div>
                </div>
                <ul className="post__meta meta">
                  <li className="meta__item meta__item--views">{data.get("views")}</li>
                  <li className="meta__item meta__item--likes">
                    <Link to="#">
                      <i className="meta-like icon-heart"></i> {data.get("likes")}
                    </Link>
                  </li>
                  <li className="meta__item meta__item--comments">
                    <Link to="#">{data.get("comments")}</Link>
                  </li>
                </ul>
              </footer>
            </div>
          </div>))}
        {news.length > 0 && (
          <nav className="team-pagination" aria-label="Team navigation">
            <ul className="pagination pagination--condensed pagination--lg justify-content-center">
              <li className="page-item active">
                <Link to="#" className="page-link">
                  1
                </Link>
              </li>
              <li className="page-item">
                <Link to="#" className="page-link">
                  2
                </Link>
              </li>
              <li className="page-item">
                <Link to="#" className="page-link">
                  3
                </Link>
              </li>
              <li className="page-item">
                <span className="page-link">...</span>
              </li>
              <li className="page-item">
                <Link to="#" className="page-link">
                  16
                </Link>
              </li>
            </ul>
          </nav>
        )}
        </div>}
      </div>
    </div>
  );
};

export default News;
