import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import { useMoralis } from "react-moralis";

const MyShares = () => {
  const { Moralis } = useMoralis();

  const [shares, setShares] = useState([]);

  async function getShares(user) {
    const userQuery = new Moralis.Query("UserInfo");
    userQuery.equalTo("userId", user);
    const userResult = await query.first();
    const query = new Moralis.Query("ShareSales");
    query.equalTo("userId", userResult);
    const result = await query.find();
    setShares(result);
  }

  useEffect(() => {
    const currentUser = Moralis.User.current();
    if (currentUser) {
      getShares(currentUser);
    }
  }, []);

  return (
    <div className="App">
      <Header />

      <div className="site-content">
        <div className="container">
          <div className="card card--has-table">
            <div className="card__header">
              <h4>My Shares</h4>
            </div>
            <div className="card__content">
              <div className="table-responsive">
                <table className="table table-hover team-schedule team-schedule--full">
                  <thead>
                    <tr>
                      <th className="team-schedule__date">Date</th>
                      <th className="team-schedule__versus">Club</th>
                      <th className="team-schedule__status">Amount</th>
                      <th className="team-schedule__time">Payed</th>
                      <th className="team-schedule__compet">Percent</th>
                    </tr>
                  </thead>
                  <tbody>
                    {shares.length == 0 && (
                      <tr>
                        <td
                          className="game-player-result__date no-results-found"
                          colSpan="7"
                        >
                          No results found!
                        </td>
                      </tr>
                    )}
                    {shares.map((data, index) => (
                      <tr>
                        <td className="team-schedule__date">
                          Saturday, Mar 24
                        </td>
                        <td className="team-schedule__versus">
                          <div className="team-meta">
                            <figure className="team-meta__logo">
                              <img
                                src="assets/images/samples/logos/lucky_clovers_shield.png"
                                alt=""
                              />
                            </figure>
                            <div className="team-meta__info">
                              <h6 className="team-meta__name">Lucky Clovers</h6>
                              <span className="team-meta__place">
                                St. Patrick’s Institute
                              </span>
                            </div>
                          </div>
                        </td>
                        <td className="team-schedule__status">
                          <div className="widget-results__team widget-results__team--second">
                            <figure className="widget-results__team-logo">
                              <img
                                src="assets/images/soccer/logos/alchemists_s_shield.png"
                                alt=""
                              />
                            </figure>
                            <div className="widget-results__team-details">
                              <h5 className="widget-results__team-name">
                                Alchemists
                              </h5>
                              <span className="widget-results__team-info">
                                Eric Bros School
                              </span>
                            </div>
                          </div>
                        </td>
                        <td className="team-schedule__time">9:00PM EST</td>
                        <td className="team-schedule__compet">
                          West Bay Playoffs 2018
                        </td>
                        <td className="team-schedule__venue">
                          Madison Cube Stadium
                        </td>
                        <td className="team-schedule__tickets">
                          <a
                            href="#"
                            className="btn btn-xs btn-default btn-outline btn-block"
                          >
                            Buy Ticket
                          </a>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {shares.length > 10 && (
            <nav className="team-pagination" aria-label="Team navigation">
              <ul className="pagination pagination--condensed pagination--lg justify-content-center">
                <li className="page-item active">
                  <a className="page-link" href="#">
                    1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    3
                  </a>
                </li>
                <li className="page-item">
                  <span className="page-link">...</span>
                </li>
                <li className="page-item">
                  <a className="page-link" href="#">
                    16
                  </a>
                </li>
              </ul>
            </nav>
          )}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default MyShares;
