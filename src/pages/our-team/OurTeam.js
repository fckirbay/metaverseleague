import React, { useState, useEffect } from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

const OurTeam = () => {
  return (
    <div className="App">
      <Header />
      <div className="page-heading">
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <h1 className="page-heading__title">
                Our <span className="highlight">Team</span>
              </h1>
            </div>
          </div>
        </div>
      </div>
      <div className="site-content">
        <div className="container">
          <div className="sponsors-grid row">
            <div className="col-sm-6 col-lg-4">
              <div className="card sponsor-card">
                <header className="card__header sponsor-card__header">
                  <figure className="sponsor-card__logo">
                      <img
                        src="assets/images/ourteam/ferdykirby.png"
                        alt="Ferdy"
                      />
                  </figure>
                </header>
                <div className="card__content sponsor-card__content">
                  <div className="sponsor-card__excerpt">Co-Founder</div>
                  <ul className="social-links sponsor-card__social">
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Facebook">
                        <i className="fab fa-facebook"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitter">
                        <i className="fab fa-twitter"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Instagram">
                        <i className="fab fa-instagram"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <footer className="card__footer sponsor-card__footer">
                  <a href="#" className="sponsor-card__link">
                    Ferdy 
                  </a>
                </footer>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="card sponsor-card">
                <header className="card__header sponsor-card__header">
                  <figure className="sponsor-card__logo">
                      <img
                        src="assets/images/ourteam/backerozy.png"
                        alt="Backer"
                      />
                  </figure>
                </header>
                <div className="card__content sponsor-card__content">
                  <div className="sponsor-card__excerpt">Co-Founder</div>
                  <ul className="social-links sponsor-card__social">
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Facebook">
                        <i className="fab fa-facebook"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitter">
                        <i className="fab fa-twitter"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <footer className="card__footer sponsor-card__footer">
                  <a
                    className="sponsor-card__link"
                  >
                    Backer
                  </a>
                </footer>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="card sponsor-card">
                <header className="card__header sponsor-card__header">
                  <figure className="sponsor-card__logo">
                      <img
                        src="assets/images/ourteam/melenormitroglu.png"
                        alt="Melenor"
                      />
                  </figure>
                </header>
                <div className="card__content sponsor-card__content">
                  <div className="sponsor-card__excerpt">Co-Founder</div>
                  <ul className="social-links sponsor-card__social">
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Facebook">
                        <i className="fab fa-facebook"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitter">
                        <i className="fab fa-twitter"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitch">
                        <i className="fab fa-twitch"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <footer className="card__footer sponsor-card__footer">
                  <a
                    className="sponsor-card__link"
                  >
                    Melenor
                  </a>
                </footer>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="card sponsor-card">
                <header className="card__header sponsor-card__header">
                  <figure className="sponsor-card__logo">
                      <img
                        src="assets/images/ourteam/muray.png"
                        alt="Berk"
                      />
                  </figure>
                </header>
                <div className="card__content sponsor-card__content">
                  <div className="sponsor-card__excerpt">Advisor</div>
                  <ul className="social-links sponsor-card__social">
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Facebook">
                        <i className="fab fa-facebook"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitter">
                        <i className="fab fa-twitter"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitch">
                        <i className="fab fa-twitch"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <footer className="card__footer sponsor-card__footer">
                  <a
                    className="sponsor-card__link"
                  >
                    Muray
                  </a>
                </footer>
              </div>
            </div>
            <div className="col-sm-6 col-lg-4">
              <div className="card sponsor-card">
                <header className="card__header sponsor-card__header">
                  <figure className="sponsor-card__logo">
                      <img
                        src="assets/images/ourteam/berk.png"
                        alt="Berk"
                      />
                  </figure>
                </header>
                <div className="card__content sponsor-card__content">
                  <div className="sponsor-card__excerpt">Social Media Expert</div>
                  <ul className="social-links sponsor-card__social">
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Facebook">
                        <i className="fab fa-facebook"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitter">
                        <i className="fab fa-twitter"></i>
                      </a>
                    </li>
                    <li className="social-links__item">
                      <a href="#" className="social-links__link" title="Twitch">
                        <i className="fab fa-twitch"></i>
                      </a>
                    </li>
                  </ul>
                </div>
                <footer className="card__footer sponsor-card__footer">
                  <a
                    className="sponsor-card__link"
                  >
                    Berk
                  </a>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default OurTeam;
