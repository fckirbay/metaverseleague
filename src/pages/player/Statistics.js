import React, { useState, useEffect } from "react";

const Statistics = () => {


  const [lastGameLog, setLastGameLog] = useState([]);

  const [stats, setStats] = useState({ "goals": 0, "assists": 0, "shotsOnTarget": 0, "fauls": 0, "injuries": 0, "yellowCards": 0, "redCards": 0, "matchs": 0 })

  useEffect(() => { }, []);

  return (
    <div className="site-content">
      <div className="container">

      <div className="content col-lg-12">
            <aside className="widget card widget--sidebar widget-player widget-player--alt">
              <div className="widget__title card__header">
                <h4>Player Main Stats</h4>
              </div>
              <div className="widget__content-secondary">
                <div className="widget-player__details">
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Matchs
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In this league
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.matchs}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Goals
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In this league
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.goals}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Assists
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In his career
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.assists}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Shots on Target
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In this league
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.shotsOnTarget}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Fouls
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In his career
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.fauls}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Injuries
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In this league
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.injuries}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Yellow cards
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In his career
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.yellowCards}</span>
                    </div>
                  </div>
                  <div className="widget-player__details__item">
                    <div className="widget-player__details-desc-wrapper">
                      <span className="widget-player__details-holder">
                        <span className="widget-player__details-label">
                          Red Cards
                        </span>{" "}
                        <span className="widget-player__details-desc">
                          In this league
                        </span>{" "}
                      </span>
                      <span className="widget-player__details-value">{stats.redCards}</span>
                    </div>
                  </div>

                </div>
              </div>
              <div className="widget__content-tertiary widget__content--bottom-decor">
                <div className="widget__content-inner"></div>
              </div>
            </aside>
          </div>

        <div className="card card--has-table">
          <div className="card__header">
            <h4>Last Game Log</h4>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table-hover game-player-result">
                <thead>
                  <tr>
                    <th className="game-player-result__date">Date</th>
                    <th className="game-player-result__vs">Versus</th>
                    <th className="game-player-result__score">Score</th>
                    <th className="game-player-result__min">Min</th>
                    <th className="game-player-result__tg">TG</th>
                    <th className="game-player-result__ts">TS</th>
                    <th className="game-player-result__st">ST</th>
                    <th className="game-player-result__ga">GA</th>
                    <th className="game-player-result__og">OG</th>
                    <th className="game-player-result__pc">PC</th>
                    <th className="game-player-result__pm">PM</th>
                    <th className="game-player-result__fc">FC</th>
                    <th className="game-player-result__fs">FS</th>
                    <th className="game-player-result__yc">YC</th>
                    <th className="game-player-result__rc">RC</th>
                  </tr>
                </thead>
                <tbody>
                  {lastGameLog.length === 0 &&
                    <tr>
                      <td className="game-player-result__date no-results-found" colSpan="15">No results found!</td>
                    </tr>}
                  {lastGameLog.map((data,index) => (
                  <tr>
                    <td className="game-player-result__date">Sat, Mar 10</td>
                    <td className="game-player-result__vs">
                      <div className="team-meta">
                        <figure className="team-meta__logo">
                          <img
                            src="/assets/images/samples/logos/sharks_shield.png"
                            alt=""
                          />
                        </figure>
                        <div className="team-meta__info">
                          <h6 className="team-meta__name">Sharks</h6>
                          <span className="team-meta__place">Marine College</span>
                        </div>
                      </div>
                    </td>
                    <td className="game-player-result__score">
                      <span className="game-player-result__game">W</span> 2-0
                    </td>
                    <td className="game-player-result__min">56</td>
                    <td className="game-player-result__tg">1</td>
                    <td className="game-player-result__ts">12</td>
                    <td className="game-player-result__st">6</td>
                    <td className="game-player-result__ga">1</td>
                    <td className="game-player-result__og">0</td>
                    <td className="game-player-result__pc">0</td>
                    <td className="game-player-result__pm">0</td>
                    <td className="game-player-result__fc">3</td>
                    <td className="game-player-result__fs">4</td>
                    <td className="game-player-result__yc">1</td>
                    <td className="game-player-result__rc">0</td>
                  </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {/*<div className="card card--has-table">
          <div className="card__header">
            <h4>Full Season Statistics</h4>
          </div>
          <div className="card__content">
            <div className="table-responsive">
              <table className="table table-hover player-season-avg">
                <thead>
                  <tr>
                    <th className="player-season-avg__season">Season</th>
                    <th className="player-season-avg__gmp">GMP</th>
                    <th className="player-season-avg__min">MIN</th>
                    <th className="player-season-avg__tg">TG</th>
                    <th className="player-season-avg__gg">GG</th>
                    <th className="player-season-avg__ts">TS</th>
                    <th className="player-season-avg__st">ST</th>
                    <th className="player-season-avg__ga">GA</th>
                    <th className="player-season-avg__ag">AG</th>
                    <th className="player-season-avg__og">OG</th>
                    <th className="player-season-avg__pc">PC</th>
                    <th className="player-season-avg__pm">PM</th>
                    <th className="player-season-avg__fc">FC</th>
                    <th className="player-season-avg__fs">FS</th>
                    <th className="player-season-avg__yc">YC</th>
                    <th className="player-season-avg__rc">RC</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="player-season-avg__season">
                      East Bay League 2015
                    </td>
                    <td className="player-season-avg__gmp">24</td>
                    <td className="player-season-avg__min">846</td>
                    <td className="player-season-avg__tg">32</td>
                    <td className="player-season-avg__gg">1.33</td>
                    <td className="player-season-avg__ts">28</td>
                    <td className="player-season-avg__st">25</td>
                    <td className="player-season-avg__ga">20</td>
                    <td className="player-season-avg__ag">0.83</td>
                    <td className="player-season-avg__og">2</td>
                    <td className="player-season-avg__pc">4</td>
                    <td className="player-season-avg__pm">2</td>
                    <td className="player-season-avg__fc">14</td>
                    <td className="player-season-avg__fs">26</td>
                    <td className="player-season-avg__yc">8</td>
                    <td className="player-season-avg__rc">3</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>*/}
        <div className="card">
          <div className="card__header">
            <h4>Glossary</h4>
          </div>
          <div className="card__content">
            <div className="glossary">
              <div className="glossary__item">
                <span className="glossary__abbr">MIN:</span> Minutes Played
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">TS:</span> Total Shots
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">OG:</span> Own Goals
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">FS:</span> Fouls Suffered
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">GMP:</span> Games Played
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">ST:</span> Shots on Target
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">PC:</span> Penalties Converted
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">YC:</span> Yellow Cards
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">TG:</span> Total Goals
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">GA:</span> Goal Assists
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">PM:</span> Penalties Missed
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">RC:</span> Red Cards
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">GG:</span> Goals per Game (Avg)
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">AG:</span> Assists per Game (Avg)
              </div>
              <div className="glossary__item">
                <span className="glossary__abbr">FC:</span> Fouls Commited
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Statistics;
