import React, { useRef, useState, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { useParams } from "react-router-dom";
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from "swiper";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";
import Overview from "./Overview";
import Statistics from "./Statistics";
import News from "./News";
import History from "./History";
import { PieChart } from 'react-minimal-pie-chart';
import { useMoralis } from "react-moralis";
import { toast } from "react-toastify";
import configData from "../../config/config.json";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";
toast.configure();
const toastOptions = {
  autoClose: 500,
  hideProgressBar: true,
  position: "bottom-right",
  theme: "dark"
};

const Player = () => {
  const { Moralis } = useMoralis();

  const { id } = useParams();

  const [selectedTab, setSelectedTab] = useState('overview');

  const [playerInfo, setPlayerInfo] = useState([]);
  const [pageSelection, setPageSelection] = useState(false);
  const [watchers, setWatchers] = useState(0);
  const [isWatcher, setIsWatcher] = useState(false);

  const [player, setPlayer] = useState({
    id: null,
    short_name: "",
    photo: "",
    height: 0,
    weight: 0,
    age: 0,
    nationality: "",
    team: null,
    bestPosition: "",
    preferredFoot: "",
    totalGoals: 0,
    totalAssists: 0,
    gamesPlayed: 0,
    goalsPerGame: 0,
    assistsPerGame: 0,
    overall: 0,
    potential: 0,
    sprintSpeed: 0,
    strength: 0,
    bestOverallRating: 0,
    marketValue: 0,
    wage: 0,
    finishing: 0,
    ballControl: 0,
    crossing: 0,
    headingAccuracy: 0,
    shortPassing: 0,
    volleys: 0,
    dribbling: 0,
    curve: 0,
    fkAccuracy: 0,
    longPassing: 0,
    ballControl: 0,
    acceleration: 0,
    sprintSpeed: 0,
    agility: 0,
    reactions: 0,
    balance: 0,
    shotPower: 0,
    jumping: 0,
    stamina: 0,
    strength: 0,
    longShots: 0,
    aggression: 0,
    interceptions: 0,
    positioning: 0,
    vision: 0,
    penalties: 0,
    composure: 0,
    defensiveAwareness: 0,
    standingTackle: 0,
    slidingTackle: 0,
    gkDiving: 0,
    gkHandling: 0,
    gkKicking: 0,
    gkPositioning: 0,
    gkReflexes: 0,
    watcherCount: 0,
    onSale: false,
    openLoan: false
  });

  async function getPlayer() {
    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    query.equalTo("player_id", parseInt(id));
    query.include("club");
    const result = await query.first();

    setPlayer({
      ...player,
      id: result.id,
      short_name: result.get("short_name"),
      photo: 'https://ipfs.moralis.io:2053/ipfs/'+ result.get("photo"),
      height: result.get("height"),
      weight: result.get("weight"),
      age: result.get("age"),
      nationality: result.get("nationaliy"),
      bestPosition: result.get("best_position"),
      preferredFoot: result.get("preferred_foot"),
      overall: result.get("overall"),
      potential: result.get("potential"),
      sprintSpeed: result.get("sprint_speed"),
      strength: result.get("strength"),
      bestOverallRating: result.get("best_overall_rating"),
      marketValue: result.get("market_value"),
      wage: result.get("wage"),
      finishing: result.get("finishing"),
      ballControl: result.get("ball_control"),
      crossing: result.get("crossing"),
      headingAccuracy: result.get("heading_accuracy"),
      shortPassing: result.get("short_passing"),
      volleys: result.get("volleys"),
      dribbling: result.get("dribbling"),
      curve: result.get("curve"),
      fkAccuracy: result.get("fk_accuracy"),
      longPassing: result.get("long_passing"),
      ballControl: result.get("ball_control"),
      acceleration: result.get("acceleration"),
      sprintSpeed: result.get("sprint_speed"),
      agility: result.get("agility"),
      reactions: result.get("reactions"),
      balance: result.get("balance"),
      shotPower: result.get("shot_power"),
      jumping: result.get("jumping"),
      stamina: result.get("stamina"),
      strength: result.get("strength"),
      longShots: result.get("long_shots"),
      aggression: result.get("aggression"),
      interceptions: result.get("interceptions"),
      positioning: result.get("positioning"),
      vision: result.get("vision"),
      penalties: result.get("penalties"),
      composure: result.get("composure"),
      defensiveAwareness: result.get("defensive_awaraness"),
      standingTackle: result.get("standing_tackle"),
      slidingTackle: result.get("sliding_tackle"),
      gkDiving: result.get("gk_diving"),
      gkHandling: result.get("gk_handling"),
      gkKicking: result.get("gk_kicking"),
      gkPositioning: result.get("gk_positioning"),
      gkReflexes: result.get("gk_reflexes"),
      watcherCount: result.get("watcherCount"),
      onSale: result.get("on_sale"),
      openLoan: result.get("open_loan"),
      team: result.get("club") ? result.get("club").get("name") : "-"
    });
    setPlayerInfo(result);
  }

  const changeSelectedTab = (tab) => {
    setSelectedTab(tab);
  }

  const watch = async () => {
    const currentUser = Moralis.User.current();
    if (currentUser) {
      const params =  { player: parseInt(id)};
      const watchResult = await Moralis.Cloud.run("watchlist", params);
      if(watchResult == 'watch') {
          setIsWatcher(true);
          setWatchers(watchers + 1);
      } else {
          setIsWatcher(false);
          setWatchers(watchers - 1);
      }
    } else {
        toast.error('Login to use all features!',toastOptions)
    }
}
  async function checkWatch(player, user) {
    const query1 = new Moralis.Query("UserInfo");
    query1.equalTo("userId", user);
    const watcherResult = await query1.first();

    const query2 = new Moralis.Query("Players");
    query2.equalTo("player_id", player);
    const playerResult = await query2.first();

    const query = new Moralis.Query("Watchlist");
    query.equalTo("watcherId", watcherResult);
    query.equalTo("playerId", playerResult);
    const result = await query.first();
    if(result) {
      setIsWatcher(true);
    } 
  }

  useEffect(() => {
    getPlayer();
    const currentUser = Moralis.User.current();
    if (currentUser) {
      checkWatch(parseInt(id), currentUser);
    }
  }, []);

  return (
    <div className="App">
      <Header />
      <div className="player-heading">
        <div className="container">
          <div className="player-info__title player-info__title--mobile">
            {/*<div className="player-info__number">07</div>*/}
            <h1 className="player-info__name">
              {/*<span className="player-info__first-name">James</span>{" "}*/}
              <span className="player-info__last-name">
                {player.short_name} 
              </span>
            </h1>
          </div>
          <div className="player-info slick-initialized slick-slider">
            <div className="slick-list draggable">
              <div
                className="slick-track"
                style={{
                  opacity: "1",
                  width: "15000px",
                  transform: "translate3d(0px, 0px, 0px)",
                }}
              >
                <div
                  className="player-info__item player-info__item--photo slick-slide slick-current slick-active"
                  data-slick-index="0"
                  aria-hidden="false"
                  style={{ marginTop: "40px" }}
                >
                

                  <figure className="player-info__photo">
                    <img src={player.photo} alt="" className="player-detail-photo"/>
                  </figure>

                  {!isWatcher ? 
                  <button
                    type="submit"
                    className="btn btn-inverse btn-sm btn-outline watchlist-button"
                    onClick={() => watch()}
                  >
                    <i className="fas fa-eye watchlist-icon"></i> Add to Watchlist
                  </button>  
                   :
                  <button
                  type="submit"
                  className="btn btn-inverse btn-sm btn-outline watchlist-button"
                  onClick={() => watch()}
                >
                  <i className="fas fa-eye-slash watchlist-icon"></i> Remove to Watchlist
                </button>
                }
                 
                </div>
                <div
                  className="player-info__item player-info__item--details slick-slide slick-active"
                  data-slick-index="1"
                  aria-hidden="false"
                >
                  <div className="player-info__title player-info__title--desktop">
                    {/*<div className="player-info__number">07</div>*/}
                    <h1 className="player-info__name">
                      {/*<span className="player-info__first-name">James</span>{" "}*/}
                      <span className="player-info__last-name">
                        {player.short_name}
                      </span>
                    </h1>
                  </div>
                  <div className="player-info-details">
                    <div className="player-info-details__item player-info-details__item--height">
                      <h6 className="player-info-details__title">Height</h6>
                      <div className="player-info-details__value">
                        {player.height} cm
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--weight">
                      <h6 className="player-info-details__title">Weight</h6>
                      <div className="player-info-details__value">
                        {player.weight} kg
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--age">
                      <h6 className="player-info-details__title">Age</h6>
                      <div className="player-info-details__value">
                        {player.age}
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--college">
                      <h6 className="player-info-details__title">
                        Nationality
                      </h6>
                      <div className="player-info-details__value">
                        {player.nationality}
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--born">
                      <h6 className="player-info-details__title">
                        Team / Club
                      </h6>
                      <div className="player-info-details__value">
                        {player.team}
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--position">
                      <h6 className="player-info-details__title">Position</h6>
                      <div className="player-info-details__value">
                        {player.bestPosition}
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--foot">
                      <h6 className="player-info-details__title">Foot</h6>
                      <div className="player-info-details__value">
                        {player.preferredFoot}
                      </div>
                    </div>
                  </div>
                  <div className="player-info-stats pt-0">
                    <div className="player-info-stats__item">
                      <div className="player-info-details">
                        <div className="player-info-details__item player-info-details__item--goals">
                          <h6 className="player-info-details__title">
                            T.Goals
                          </h6>
                          <div className="player-info-details__value">
                            {player.totalGoals}
                          </div>
                        </div>
                        <div className="player-info-details__item player-info-details__item--assists">
                          <h6 className="player-info-details__title">
                            T.Assists
                          </h6>
                          <div className="player-info-details__value">
                            {player.totalAssists}
                          </div>
                        </div>
                        <div className="player-info-details__item player-info-details__item--games pb-0">
                          <h6 className="player-info-details__title">
                            Total Games Played
                          </h6>
                          <div className="player-info-details__value">
                            {player.gamesPlayed}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="player-info-stats__item player-info-stats__item--top-padding">
                      <div className="circular">
                        <div
                          className="circular__bar"
                          data-percent="88"
                          data-bar-color="#9fe900"
                          data-track-color="rgba(255,255,255,.2)"
                        >                    
                         <PieChart
                          data={[{ value: player.overall, color: '#9FE900' }]}
                          totalValue={100}
                          lineWidth={15}
                          label={({ dataEntry }) => dataEntry.value}
                          labelStyle={{
                            fontSize: '30px',
                            fontFamily: 'sans-serif',
                            fill: '#fff',
                            fontWeight: 700
                          }}
                          labelPosition={0}
                          style={{ height: "50px", width: "50px" }}
                        />
                        </div>
                        <span className="circular__label">
                          <strong>Overall</strong>
                        </span>
                      </div>
                    </div>
                    <div className="player-info-stats__item player-info-stats__item--top-padding">
                      <div className="circular">
                        <div
                          className="circular__bar"
                          data-percent="74"
                          data-bar-color="#9fe900"
                          data-track-color="rgba(255,255,255,.2)"
                        >
                          {/*<span className="circular__percents">
                            {player.assistsPerGame}
                            <small>avg</small>
                        </span>*/}
                        <PieChart
                          data={[{ value: player.potential, color: '#9FE900' }]}
                          totalValue={100}
                          lineWidth={15}
                          label={({ dataEntry }) => dataEntry.value}
                          labelStyle={{
                            fontSize: '30px',
                            fontFamily: 'sans-serif',
                            fill: '#fff',
                            fontWeight: 700
                          }}
                          labelPosition={0}
                          style={{ height: "50px", width: "50px" }}
                        />
                          <canvas
                            height="180"
                            width="180"
                            style={{ height: "90px", width: "90px" }}
                          ></canvas>
                        </div>
                        <span className="circular__label">
                          <strong>Potential</strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="player-info-details">
                    <div className="player-info-details__item player-info-details__item--born">
                      <h6 className="player-info-details__title">
                        Market Value
                      </h6>
                      <div className="player-info-details__value">
                        ${player.marketValue / 10000}
                      </div>
                    </div>
                    <div className="player-info-details__item player-info-details__item--position">
                      <h6 className="player-info-details__title">Wage</h6>
                      <div className="player-info-details__value">
                        ${player.wage / 10000}
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className="player-info__item player-info__item--stats slick-slide slick-active"
                  data-slick-index="2"
                  aria-hidden="false"
                >
                  <div className="player-info__item--stats-inner">
                    <div className="progress-stats progress-stats--top-labels">
                      <div className="progress__label">Best Overall Rating</div>
                      <div className="progress">
                        <div
                          className={`progress__bar progress__bar--success`}
                          style={{ width: player.bestOverallRating + "%" }}
                          role="progressbar"
                          aria-valuenow={player.bestOverallRating}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <div className="progress__number">
                        {player.bestOverallRating}%
                      </div>
                    </div>
                    <div className="progress-stats progress-stats--top-labels">
                      <div className="progress__label">Speed</div>
                      <div className="progress">
                        <div
                          className="progress__bar progress__bar--success"
                          style={{ width: player.sprintSpeed + "%" }}
                          role="progressbar"
                          aria-valuenow={player.sprintSpeed}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <div className="progress__number">
                        {player.sprintSpeed}%
                      </div>
                    </div>
                    <div className="progress-stats progress-stats--top-labels">
                      <div className="progress__label">Strength</div>
                      <div className="progress">
                        <div
                          className="progress__bar progress__bar--success"
                          style={{ width: player.strength + "%" }}
                          role="progressbar"
                          aria-valuenow={player.strength}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <div className="progress__number">{player.strength}%</div>
                    </div>
                    <div className="progress-stats progress-stats--top-labels">
                      <div className="progress__label">Finishing</div>
                      <div className="progress">
                        <div
                          className={`progress__bar progress__bar--success`}
                          style={{ width: player.finishing + "%" }}
                          role="progressbar"
                          aria-valuenow={player.finishing}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <div className="progress__number">{player.finishing}%</div>
                    </div>
                    <div className="progress-stats progress-stats--top-labels">
                      <div className="progress__label">Ball Control</div>
                      <div className="progress">
                        <div
                          className={`progress__bar progress__bar--success`}
                          style={{ width: player.ballControl + "%" }}
                          role="progressbar"
                          aria-valuenow={player.ballControl}
                          aria-valuemin="0"
                          aria-valuemax="100"
                        ></div>
                      </div>
                      <div className="progress__number">
                        {player.ballControl}%
                      </div>
                    </div>
                   
                  </div>
                 
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <nav className="content-filter">
        <div className="container">
          <a className={`content-filter__toggle ${pageSelection ? "content-filter__toggle--active" : ""}`} onClick={() => setPageSelection(!pageSelection)}></a>
          <ul className={`content-filter__list ${pageSelection ? "content-filter__list--expanded" : ""}`}>
          <li className={`content-filter__item ${selectedTab == 'overview' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('overview')}
                className="content-filter__link"
              >
                <small>Player</small>Overview
              </a>
            </li>
            <li className={`content-filter__item ${selectedTab == 'statistics' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('statistics')}
                className="content-filter__link"
              >
                <small>Player</small>Full Statistics
              </a>
            </li>
            
            <li className={`content-filter__item ${selectedTab == 'news' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('news')}
                className="content-filter__link"
              >
                <small>Player</small>Related News
              </a>
            </li>
            <li className={`content-filter__item ${selectedTab == 'history' ? "content-filter__item--active" : ""}`}>
              <a
                onClick={() => setSelectedTab('history')}
                className="content-filter__link"
              >
                <small>Player</small>Transfer History
              </a>
            </li>
          </ul>
        </div>
      </nav>
      {selectedTab == 'overview' && <Overview changeTab={changeSelectedTab} playerInfo={player}/>}
      {selectedTab == 'statistics' && <Statistics />}
      {selectedTab == 'news' && <News playerId={player.id}/>}
      {selectedTab == 'history' && <History />}
      <Footer />
    </div>
  );
};

export default Player;
