import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useMoralis } from "react-moralis";

const History = () => {
  const { Moralis } = useMoralis();

  const { id } = useParams();

  const [history, setHistory] = useState([]);

  useEffect(() => {}, []);

  return (
    <div className="site-content">
      <div className="container">
        <div className="row">
          <div className="sidebar sidebar--player col-lg-8">
            <aside className="widget card widget--sidebar widget-newslog">
              <div className="widget__title card__header">
                <h4>Transfer History</h4>
              </div>
              <div className="widget__content card__content">
                <ul className="newslog">
                  {history.length == 0 &&
                    <li className="newslog__item no-results-found">
                      No results found!
                    </li>
                  }
                  {history.map((data,index) => (
                  <li className="newslog__item newslog__item--injury">
                    {/* newslog__item--join newslog__item--award newslog__item--injury */}
                    <div className="newslog__item-inner">
                      <div className="newslog__content">
                        <strong>James Messinal’s</strong> knee fracture is
                        healing properly and will be returning to the field{" "}
                        <strong>next week</strong>.
                      </div>
                      <time className="newslog__date" dateTime="2016-01-19">
                        January 19, 2016
                      </time>
                    </div>
                  </li>))}
                  
                </ul>
              </div>
            </aside>
          </div>
          <div className="sidebar col-lg-4">
              <aside className="widget widget--sidebar widget-social">
                <a
                  href="#"
                  className="btn-social-counter btn-social-counter--facebook"
                  target="_blank"
                >
                  <div className="btn-social-counter__icon">
                    <i className="fas fa-exchange-alt"></i>
                  </div>
                  <h6 className="btn-social-counter__title">
                    Transfer
                  </h6>
                  <span className="btn-social-counter__count">
                    <span className="btn-social-counter__count-num"></span> Sign contract now
                  </span>{" "}
                  <span className="btn-social-counter__add-icon"></span>{" "}
                </a>
                <a
                  href="#"
                  className="btn-social-counter btn-social-counter--twitter"
                  target="_blank"
                >
                  <div className="btn-social-counter__icon">
                    <i className="fas fa-gavel"></i>
                  </div>
                  <h6 className="btn-social-counter__title">
                    Make an Offer
                  </h6>
                  <span className="btn-social-counter__count">
                    <span className="btn-social-counter__count-num"></span>{" "}
                    Suggest a fee for the player
                  </span>{" "}
                  <span className="btn-social-counter__add-icon"></span>
                </a>
              </aside>
            </div>
        </div>
      </div>
    </div>
  );
};

export default History;
