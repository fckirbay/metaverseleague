import React, { useRef, useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import moment from 'moment';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useMoralis } from "react-moralis";
import configData from "../../config/config.json";
import mainContractAbi from "../../config/maincontractabi.json";

toast.configure()
const options = {
  autoClose: 2000,
  hideProgressBar: true,
  position: "bottom-right"
};

const Overview = (props) => {

  const {user, Moralis } = useMoralis();    

  const { id } = useParams();
  const [lastGameLog, setLastGameLog] = useState([]);
  const [news, setNews] = useState([]);
  const [negotiating, setNegotiating] = useState(false);
  const [isOwner, setIsOwner] = useState(false);

  const getBase64FromUrl = async (url) => {
    const data = await fetch(url.replace("https://cdn.sofifa.net", ""));
    const blob = await data.blob();
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        resolve(base64data);
      }
    });
  }
  
  async function sellPlayer(playerId) {
    toast.success('Coming soon!', options);
  }

  async function startTransfer(playerId) {


    if(props.playerInfo.onSale && !negotiating) {

      const transferOptions = {
        contractAddress: configData.TRANSFER_CONTRACT,
        functionName: "preSaleMftItem",
        abi: mainContractAbi,
        params: {
          amount: Moralis.Units.Token(props.playerInfo.marketValue / 10000, 6),
        },
      };
      Moralis.executeFunction(transferOptions).then((transferResult) => {

        let params = { player: playerId, transactionHash: transferResult.hash }
        const verifySign = Moralis.Cloud.run("startTransfer", params).then((res) => {
          if(res.result == "success") {
            setNegotiating(true);
            toast.success('Transfer negotiations have begun.',options);
          } else {
            toast.error('Transfer negotiations have could not begun.',options);
          }
        }, (error) => {
          toast.error('Transfer negotiations have could not begun.',options);
        });
  
      }, (error) => {
        toast.error("Transfer negotiations could not be started.", options);
      });

      
    }

    
  }

  async function getTokenMetadata() {
    const options = { address: configData.MAIN_CONTRACT, token_id: "261897", chain: "0x13881" };
    const tokenIdMetadata = await Moralis.Web3API.token.getTokenIdMetadata(options);
  }

  async function checkNegotiation() {
    let params = { player: id }
    const verifySign = Moralis.Cloud.run("checkNegotiation", params).then((res) => {
      if(res.result == true) {
        setNegotiating(true);
      }
      if(res.owner == true) {
        setIsOwner(true);
      }
    }, (error) => {
      //console.log("error", error);
    });
  }

  async function mintFunction() {
    var ids = [id];
    var amounts = [1];
    var prices = [props.playerInfo.marketValue / 10000];
    const mintOptions = {
      contractAddress: configData.MAIN_CONTRACT,
      functionName: "mintBatch",
      abi: mainContractAbi,
      params: {
        to: "0x5F9e9bcdA6E73827a0D159319CE341b8E245cC8f",
        ids: ids,
        amounts: amounts,
        prices: prices,
        data: []
      },
    };

    Moralis.executeFunction(mintOptions).then((mintBatchResult) => {
      console.log("mintBatchResult", mintBatchResult);
    }, (error) => {
      console.log("mintBatchResult error", error);
    });
  }
/*
  async function bulkMintFunction() {

    

    var ids = [];
    var amounts = [];
    var prices = [];

    const ipfsFiles = {
      "abi": []
    };

    const ipfsArray = {
      "abi": []
    };
    let promises = [];

    const playerList = Moralis.Object.extend("Players");
    const query = new Moralis.Query(playerList);
    const results = await query.find({ useMasterKey: true });

    results.map(function (element, i) {

      ids.push(element.get("player_id"));
      prices.push(Moralis.Units.ETH("0.0001"));
      amounts.push(1);

      let paddedHex = element.get("player_id");
            
      promises.push(new Promise((res, rej) => {

        var imageAsBase64 = fs.readFileSync('node/pngtransparent/'+element.get("player_id")+'.png', 'base64');
        ipfsFiles.abi.push({
          "path": `images/${paddedHex}.png`,
          "content": imageAsBase64
        })
        res(imageAsBase64);

      }));


    });

    

    Promise.all(promises).then(() => {
      
      Moralis.Web3API.storage.uploadFolder(ipfsFiles).then((ipfsPath) => {
        results.map(function (element, i) {
          ipfsArray.abi.push({
            path: `metadata/${element.get("player_id")}.json`,
            content: {
                image: `ipfs://${ipfsPath[0].path.split("/")[4]}/images/${element.get("player_id")}.png`,
                name: `${element.get("short_name")}`,
                description: `${element.get("short_name")}`,
                properties: {
                  positions: element.get("positions").toString(),
                  height: element.get("height"),
                  weight: element.get("weight"),
                  nationality: element.get("nationaliy"),
                  playerid: element.get("player_id")
                }
            }
          })
        });
        setTimeout(function () {
          Moralis.Web3API.storage.uploadFolder(ipfsArray).then((metadataIpfsPath) => {
            console.log("metadataIpfsPath", metadataIpfsPath);

            const options = {
              contractAddress: configData.MAIN_CONTRACT,
              functionName: "setURI",
              abi: mainContractAbi,
              params: {
                newuri: `ipfs://${metadataIpfsPath[0].path.split("/")[4]}/metadata/{id}.json`,
              },
            };

            Moralis.executeFunction(options).then((setUriResult) => {
              console.log("setUriResult", setUriResult);

              const mintOptions = {
                contractAddress: configData.MAIN_CONTRACT,
                functionName: "mintBatch",
                abi: mainContractAbi,
                params: {
                  to: "0x050ea6A9277B476a1d54f3B023B649C555a970Bb",
                  ids: ids,
                  amounts: amounts,
                  prices: prices,
                  data: []
                },
              };

              Moralis.executeFunction(mintOptions).then((mintBatchResult) => {
                console.log("mintBatchResult", mintBatchResult);
              }, (error) => {
                console.log("mintBatchResult error", error);
              });

            }, (error) => {
              console.log("setUri error", error);
            });

          });
        }, 5000);
        
      });
	    
    })
  }
*/

  useEffect(() => {
    const currentUser = Moralis.User.current();
    if (currentUser) {
      checkNegotiation();
    }
  }, []);

  return (
    <div className="site-content">
      <div className="container">
        <div className="row">
          <div className="sidebar col-lg-12">
            <aside className="widget widget--sidebar widget-social">
              {negotiating && <div className="row">
                <div className="col-md-12">
                  <div className="alert alert-info"><strong>Latest News!</strong> The player's negotiations with Galatasaray club continue.</div>
                </div>
              </div>}
              { !isOwner ? 
              <div className="row">
                <div className="col-md-6" onClick={() => startTransfer(id)}>
                  <a
                    className={`btn-social-counter btn-social-counter--facebook ${!props.playerInfo.onSale || negotiating ? "disabled-a" : ""}`}
                  >
                    <div className={`btn-social-counter__icon ${!props.playerInfo.onSale || negotiating ? "dark-grey-background" : ""}`}>
                      <i className="fas fa-exchange-alt"></i>
                    </div>
                    <h6 className="btn-social-counter__title">
                      Transfer
                    </h6>
                    <span className="btn-social-counter__count">
                      <span className="btn-social-counter__count-num"></span> Sign contract now
                    </span>{" "}
                    <span className="btn-social-counter__add-icon"></span>{" "}
                  </a>
                </div>
                <div className="col-md-6">
                  <a
                    className={`btn-social-counter btn-social-counter--twitter ${!props.playerInfo.openLoan || negotiating ? "disabled-a" : ""}`}
                  >
                    <div className={`btn-social-counter__icon ${!props.playerInfo.openLoan || negotiating ? "dark-grey-background" : ""}`}>
                      <i className="fas fa-gavel"></i>
                    </div>
                    <h6 className="btn-social-counter__title">
                      Make an Offer
                    </h6>
                    <span className="btn-social-counter__count">
                      <span className="btn-social-counter__count-num"></span>{" "}
                      Suggest a fee for the player
                    </span>{" "}
                    <span className="btn-social-counter__add-icon"></span>
                  </a>
                </div>
              </div> :
              <div className="row">
              <div className="col-md-6" onClick={() => sellPlayer(id)}>
                <a
                  className={`btn-social-counter btn-social-counter--facebook ${!props.playerInfo.onSale || negotiating ? "disabled-a" : ""}`}
                >
                  <div className={`btn-social-counter__icon ${!props.playerInfo.onSale || negotiating ? "dark-grey-background" : ""}`}>
                    <i className="fas fa-exchange-alt"></i>
                  </div>
                  <h6 className="btn-social-counter__title">
                    Sale
                  </h6>
                  <span className="btn-social-counter__count">
                    <span className="btn-social-counter__count-num"></span> Put in Transfer List
                  </span>{" "}
                  <span className="btn-social-counter__add-icon"></span>{" "}
                </a>
              </div>
              <div className="col-md-6">
                <a
                  className={`btn-social-counter btn-social-counter--twitter ${!props.playerInfo.openLoan || negotiating ? "disabled-a" : ""}`}
                >
                  <div className={`btn-social-counter__icon ${!props.playerInfo.openLoan || negotiating ? "dark-grey-background" : ""}`}>
                    <i className="fas fa-gavel"></i>
                  </div>
                  <h6 className="btn-social-counter__title">
                    Terminate
                  </h6>
                  <span className="btn-social-counter__count">
                    <span className="btn-social-counter__count-num"></span>{" "}
                    Terminate the player's contract
                  </span>{" "}
                  <span className="btn-social-counter__add-icon"></span>
                </a>
              </div>
            </div>
              }

            </aside>

            <div className="row">
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>Attacking</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Crossing</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.crossing + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.crossing}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.crossing}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Finishing</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.finishing + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.finishing}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.finishing}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Heading Accuracy</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.headingAccuracy + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.headingAccuracy}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.headingAccuracy}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Short Passing</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.shortPassing + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.shortPassing}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.shortPassing}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Volleys</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.volleys + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.volleys}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.volleys}%
                          </div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>Skill</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Dribbling</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.dribbling + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.dribbling}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.dribbling}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Curve</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.curve + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.curve}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.curve}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">FK Accuracy</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.fkAccuracy + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.fkAccuracy}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.fkAccuracy}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Long Passing</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.longPassing + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.longPassing}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.longPassing}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Ball Control</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.ballControl + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.ballControl}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.ballControl}%
                          </div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>Movement</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Acceleration</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.acceleration + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.acceleration}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.acceleration}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Sprint Speed</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.sprintSpeed + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.sprintSpeed}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.sprintSpeed}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Agility</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.agility + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.agility}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.agility}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Reactions</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.reactions + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.reactions}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.reactions}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Balance</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.balance + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.balance}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.balance}%</div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>Power</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Shot Power</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.shotPower + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.shotPower}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.shotPower}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Jumping</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.jumping + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.jumping}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.jumping}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Stamina</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.stamina + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.stamina}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.stamina}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Strength</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.strength + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.strength}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.strength}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Long Shots</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.longShots + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.longShots}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.longShots}%</div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>Mentality</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Aggression</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.aggression + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.aggression}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.aggression}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Interceptions</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.interceptions + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.interceptions}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.interceptions}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Positioning</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.positioning + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.positioning}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.positioning}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Vision</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.vision + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.vision}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.vision}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Penalties</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.penalties + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.penalties}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.penalties}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Composure</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.composure + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.composure}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.composure}%</div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>Defending</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Defensive Awareness</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.defensiveAwareness + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.defensiveAwareness}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.defensiveAwareness}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Standing Tackle</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.standingTackle + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.standingTackle}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.standingTackle}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">Sliding Tackle</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.slidingTackle + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.slidingTackle}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.slidingTackle}%</div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
              <div className="sidebar sidebar--player col-lg-3">
                <aside className="widget card widget--sidebar widget-newslog">
                  <div className="widget__title card__header">
                    <h4>GoalKeeping</h4>
                  </div>
                  <div className="widget__content card__content">
                    {props.playerInfo &&
                      <div className="player-info__item--stats-inner player-detail-overview-inner">
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">GK Diving</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.gkDiving + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.gkDiving}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.gkDiving}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">GK Handling</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.gkHandling + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.gkHandling}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.gkHandling}%
                          </div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">GK Kicking</div>
                          <div className="progress">
                            <div
                              className="progress__bar progress__bar--success"
                              style={{ width: props.playerInfo.gkKicking + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.gkKicking}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.gkKicking}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">GK Positioning</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.gkPositioning + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.gkPositioning}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">{props.playerInfo.gkPositioning}%</div>
                        </div>
                        <div className="progress-stats progress-stats--top-labels">
                          <div className="progress__label">GK Reflexes</div>
                          <div className="progress">
                            <div
                              className={`progress__bar progress__bar--success`}
                              style={{ width: props.playerInfo.gkReflexes + "%" }}
                              role="progressbar"
                              aria-valuenow={props.playerInfo.gkReflexes}
                              aria-valuemin="0"
                              aria-valuemax="100"
                            ></div>
                          </div>
                          <div className="progress__number">
                            {props.playerInfo.gkReflexes}%
                          </div>
                        </div>
                      </div>}
                  </div>
                </aside>
              </div>
            </div>

            {/*<aside className="widget card widget--sidebar widget-newslog">
                <div className="widget__title card__header">
                  <h4>Player Newslog</h4>
                </div>
                <div className="widget__content card__content">
                  <ul className="newslog">
                    <li className="newslog__item newslog__item--injury">
                      <div className="newslog__item-inner">
                        <div className="newslog__content">
                          <strong>James Messinal’s</strong> knee fracture is
                          healing properly and will be returning to the field{" "}
                          <strong>next week</strong>.
                        </div>
                        <time className="newslog__date" dateTime="2016-01-19">
                          January 19, 2016
                        </time>
                      </div>
                    </li>
                  </ul>
                </div>
              </aside>*/}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Overview;
