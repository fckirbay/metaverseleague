import React from "react";
import Header from "../../components/header/Header";
import Footer from "../../components/footer/Footer";

const PrivacyPolicy = () => {
  return (
    <div className="App">
      <Header />
      <div className="site-content">
        <div className="container">
          <div className="row">
            <div className="content col-lg-12">
              <article className="card card--lg post post--single">
                <div className="card__content">
                  <header className="post__header">
                    <h2 className="post__title">
                      Privacy Policy
                    </h2>
                  </header>
                  <div className="post__content">
                    <p>
                        At MVLIGA we know that you care about your personal data and you want to know how we will process it. That is why we are going to explain to you below, in a clear and transparent manner, in our Privacy Policy, how and when we obtain, share and protect your personal data. We will also explain how to exercise your rights in relation to them.
                    </p>
                    <p>
                        Please note that anonymised information or purely statistical data used by MVLIGA will not be considered personal data.
                    </p>
                    <div className="spacer"></div>
                    <h4>Who are we?</h4>
                    <p>
                      Name: MVLiga<br />
                      Website: mvliga.com
                    </p>
                    <div className="spacer"></div>
                    <h4>Information and consent</h4>
                    <p>
                        By reading and accepting this Privacy Policy, the user (the "User" or "Users") is informed of the circumstances in which his or her personal data will be processed.
                    </p>
                    <p>
                        Furthermore, in the event that this is the necessary legal basis for the processing of your data, your free, informed, specific and unequivocal consent will be requested so that the personal data you provide through the website www.mvliga.com (hereinafter, the "Website") and the application (hereinafter, the "Application") or any forms dependent on the Website and the Application, are processed by MVLIGA, as well as the data derived from your browsing and any other data that you may provide in the future.
                    </p>
                    <p>
                        The data requested in the forms on the Website and in the Application are, in general, mandatory (unless otherwise specified in the required field) in order to comply with the purposes for which they are being collected.
                    </p>
                    <p>
                        Therefore, if they are not provided or are not provided correctly, these requests cannot be met, without prejudice to the fact that you may freely view the content of the Website and the Application, except for specific content or certain functionalities of the Website and the Application, which are limited to registered Users.
                    </p>
                    <p>
                        Please read this Privacy Policy carefully before using or registering with MVLIGA.
                    </p>
                    <div className="spacer"></div>
                    <h4>What personal data does MVLIGA access about me?</h4>
                    <p>
                        If you register on the Website and/or the Application, we will process the following information about you:
                    </p>
                    <p>
                        <ul style={{listStyleType: "disc"}}>
                            <li><b>IP Address</b></li>
                            <li><b>Browsing data:</b> the Website and Application servers may automatically detect your IP address, domain name, unique device identifier, browser fingerprint, device fingerprint and cookie IDs when you have set them in your browser.</li>
                        </ul>
                    </p>
                    <p>
                        If you do not register, but simply browse the Website, we will process the following information about you:
                    </p>
                    <p>
                        <ul style={{listStyleType: "disc"}}>
                            <li><b>Browsing data:</b> the Website and Application servers may automatically detect your IP address, domain name, unique device identifier, browser fingerprint, device fingerprint and cookie IDs when you have set them in your browser.</li>
                        </ul>
                    </p>
                    <h4>Where does the data we process about you come from?</h4>
                    <p>
                        <ul style={{listStyleType: "disc"}}>
                            <li>From the User: This is the data that you provide us with by filling in forms on the Website and/or the Application, or through any interaction that you carry out with MVLIGA.</li>
                            <li>Of the cookies placed on the Website and/or the Application and the use of similar technologies (see the corresponding section of this Privacy Policy and the Cookies Policy).</li>
                            <li>From third parties: such as business partners with whom MVLIGA has signed a collaboration contract, third parties who provide a service to MVLIGA for the effective provision of the service.</li>
                        </ul>
                    </p>
                    <h4>What are the purposes and legal bases for the processing of User data?</h4>
                    <p>
                        We may only process your personal data if we have an adequate legal basis for doing so. We may process your personal data for any or all of the purposes specified below and on the legal bases indicated:
                    </p>
                    <h4>Will your information be shared with third parties or companies?</h4>
                    <p>
                        Your personal data will not be shared with third parties or companies under any circumstances. As MVLIGA, we have been committed to this since our establishment.
                    </p>
                    <h4>How long will MVLIGA keep the data?</h4>
                    <p>
                        Your personal data will be kept for the time strictly necessary for the purposes of the processing for which they have been provided, and, in any case, following the principle of data minimization provided for in the applicable regulations.
                    </p>
                    <p>
                        If you are a MVLIGA User, your data will be kept for the duration of your commercial relationship with MVLIGA and from the moment you exercise your right to cancel your data or cancel your account, your data will be kept blocked for a period of five years in order to meet the legal responsibilities that may derive from the services provided.
                    </p>
                    <h4>How can I exercise my rights?</h4>
                    <p>
                        <ul style={{listStyleType: "disc"}}>
                            <li>Revoke consents granted.</li>
                            <li>Obtain confirmation as to whether your personal data is being processed at MVLIGA.</li>
                            <li>Access your personal data.</li>
                            <li>Rectify inaccurate or incomplete data.</li>
                            <li>Request the deletion of your data when, among other reasons, the data are no longer necessary for the purposes for which they were collected.</li>
                            <li>To obtain from MVLIGA the limitation of data processing when any of the conditions provided for in the data protection regulations are met.</li>
                            <li>Request the portability of the data provided in those cases provided for in the regulations.</li>
                            <li>Contact MVLIGA at the following address: info@mvliga.com</li>
                        </ul>
                    </p>
                    <h4>Changes to the Privacy Policy</h4>
                    <p>
                        Our Privacy Policy may be updated due to changes and legal requirements, as well as due to improvements and changes included in the way we offer and provide our services and application utilities. Therefore, we recommend that you visit and access our Privacy Policy periodically, in order to have access to the latest changes that may have been incorporated.
                    </p>
                    <p>
                        In the event that such changes relate to the consent provided by the User, we will send you a separate and independent notification to collect it again.
                    </p>
                    <p>
                        If you have any questions or concerns about our Privacy Policy or wish to exercise any rights or take any action regarding your personal data, please contact us at the following email address info@mvliga.com
                    </p>
                    <h4>Privacy and blockchain</h4>
                    <p>
                        Transactions in cryptoassets available through the Website and the Application will be recorded on the relevant public blockchain. Public blockchains operate like ledgers, intended to immutably record transactions on networks of computer systems. Many of these public blockchains allow forensic analysis that can lead to de-anonymisation and inadvertent disclosure of private financial information, especially when blockchain data is combined with other data.
                    </p>
                    <p>
                        As blockchains are decentralised or third party networks that are not controlled or operated by MVLIGA, we cannot delete, modify or alter personal data on such networks.
                    </p>
                  </div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default PrivacyPolicy;
