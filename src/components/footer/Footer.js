import React from 'react';
import { Link } from "react-router-dom";

const Footer = () => {
    
    return (
        <footer id="footer" className="footer">
            <div className="footer-widgets">
                <div className="footer-widgets__inner">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6 col-lg-3">
                                <div className="footer-col-inner">
                                    <div className="footer-logo footer-logo--has-txt"><Link to="/"><img
                                                src="/assets/images/soccer/logo_5.svg"
                                                srcSet="/assets/images/soccer/logo_5.svg 2x" alt="The Alchemists"
                                                className="footer-logo__img"/>
                                        </Link></div>
                                    <div className="widget widget--footer widget-contact-info">
                                        <div className="widget__content">
                                            <div className="widget-contact-info__desc">
                                                <p>Blockchain based soccer club management game</p>
                                            </div>
                                            <div className="widget-contact-info__body info-block">
                                                <div className="info-block__item info-block__item--nopadding">
                                                    <ul className="social-links">
                                                        <li className="social-links__item"><a href="https://t.me/mvliga"
                                                                className="social-links__link" target="_blank" rel="noreferrer"><i
                                                                    className="fab fa-telegram-plane"></i> Telegram</a></li>
                                                        <li className="social-links__item"><a href="https://twitter.com/mvligacom"
                                                                className="social-links__link" target="_blank" rel="noreferrer"><i
                                                                    className="fab fa-twitter"></i> Twitter</a></li>
                                                        <li className="social-links__item"><a href="https://instagram.com/mvliga"
                                                                className="social-links__link" target="_blank" rel="noreferrer"><i
                                                                    className="fab fa-instagram"></i> Instagram</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-3">
                                <div className="footer-col-inner">
                                    <div className="widget widget--footer widget-featured-posts">
                                        <h4 className="widget__title">About MVL</h4>
                                        <div className="widget__content">
                                            <Link to="/about-game" className="info-block__link">About Game</Link>
                                            <Link to="/player-transfer" className="info-block__link footer-menu-link">Player Transfer</Link>
                                            <Link to="/play-to-earn" className="info-block__link footer-menu-link">Play to Earn</Link>
                                            <Link to="/our-team" className="info-block__link footer-menu-link">Our Team</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix visible-sm"></div>
                            <div className="col-sm-6 col-lg-3">
                                <div className="footer-col-inner">
                                    <div className="widget widget--footer widget-featured-posts">
                                        <h4 className="widget__title">Resources</h4>
                                        <div className="widget__content">
                                            <Link to="/whitepaper" className="info-block__link">Whitepaper</Link>
                                            <Link to="/privacy-policy" className="info-block__link footer-menu-link">Terms & Conditions</Link>
                                            <Link to="/privacy-policy" className="info-block__link footer-menu-link">Privacy Policy</Link>
                                            <Link to="/disclaimer" className="info-block__link footer-menu-link">Disclaimer</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 col-lg-3">
                                <div className="footer-col-inner">
                                    <div className="widget widget--footer widget-contact">
                                        <h4 className="widget__title">Quick Contact</h4>
                                        <div className="widget__content">
                                            <p>You can reach us using our e-mail address below or through our social media accounts.</p>
                                            <div className="info-block__item"><svg role="img"
                                                        className="df-icon df-icon--soccer-ball">
                                                    </svg>
                                                    <h6 className="info-block__heading">Contact Us</h6><a
                                                        className="info-block__link"
                                                        href="mailto:info@mvliga.com">info@mvliga.com</a>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<div className="container">
                    <div className="sponsors">
                        <h6 className="sponsors-title">Our Sponsors:</h6>
                        <ul className="sponsors-logos">
                            <li className="sponsors__item"><Link to="#" target="_blank"><img
                                        src="/assets/images/soccer/sponsor-visa.png" alt=""/></Link></li>
                            <li className="sponsors__item"><Link to="#" target="_blank"><img
                                        src="/assets/images/soccer/sponsor-discover.png" alt=""/></Link></li>
                            <li className="sponsors__item"><Link to="#" target="_blank"><img
                                        src="/assets/images/soccer/sponsor-paypal.png" alt=""/></Link></li>
                            <li className="sponsors__item"><Link to="#" target="_blank"><img
                                        src="/assets/images/soccer/sponsor-skrill.png" alt=""/></Link></li>
                            <li className="sponsors__item"><Link to="#" target="_blank"><img
                                        src="/assets/images/soccer/sponsor-westernunion.png" alt=""/></Link></li>
                            <li className="sponsors__item"><Link to="#" target="_blank"><img
                                        src="/assets/images/soccer/sponsor-payoneer.png" alt=""/></Link></li>
                        </ul>
                    </div>
                </div>*/}
            </div>
            <div className="footer-secondary">
                <div className="container">
                    <div className="footer-secondary__inner">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="footer-copyright"><Link to="/">MVLiga</Link> 2022
                                    &nbsp; | &nbsp; All Rights Reserved</div>
                            </div>
                            <div className="col-md-8">
                                <ul className="footer-nav footer-nav--right footer-nav--condensed footer-nav--sm">
                                    <li className="footer-nav__item"><Link to="/">Home</Link></li>
                                    <li className="footer-nav__item"><Link to="/stock-place">Stock Market</Link></li>
                                    <li className="footer-nav__item"><Link to="/market-place">Transfer Market</Link></li>
                                    <li className="footer-nav__item"><Link to="/fixture">Fixture</Link></li>
                                    <li className="footer-nav__item"><Link to="/clubs">Clubs</Link></li>
                                    <li className="footer-nav__item"><Link to="/user-watchlist">Watchlist</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
