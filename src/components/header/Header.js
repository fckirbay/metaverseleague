import React, { useRef, useState, useEffect } from "react";
import { useMoralis, useNativeBalance, useWeb3Transfer, useMoralisWeb3Api } from "react-moralis";
import { toast } from "react-toastify";
import configData from "../../config/config.json";
import mainContractAbi from "../../config/maincontractabi.json";
import usdcContractAbi from "../../config/usdccontractabi.json";

import defaultProfileImage from "../../assets/images/avatar.png";
import { Link, useNavigate } from "react-router-dom";
import Modal from "react-modal";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    zIndex: 9999
  },
};

toast.configure();
const options = {
  autoClose: 1500,
  hideProgressBar: true,
  position: "bottom-right",
  theme: "dark",
};


const Header = () => {
  const { authenticate, Moralis, isAuthenticated, user, login, logout } =
    useMoralis();
  const {
    getBalances,
    data: balance,
    nativeToken,
    error,
    isLoading,
  } = useNativeBalance();

  const navigate = useNavigate();

  const [mobileMenu, setMobileMenu] = useState(false);
  const [userData, setUserData] = useState({
    username: "",
    wallet: "",
    profileImage: defaultProfileImage,
  });
  const [userInfo, setUserInfo] = useState(null);
  const [watcherCount, setWatcherCount] = useState(0);
  const [addFundModal, setAddFundModal] = useState(false);
  const [withdrawModal, setWithdrawModal] = useState(false);
  const [amount, setAmount] = useState(0);
  const [funds, setFunds] = useState(0);
  const [funding, setFunding] = useState(false);
  const [openMobileMenu, setOpenMobileMenu] = useState(false);

  const { fetch, transferError, isFetching } = useWeb3Transfer({
    amount: Moralis.Units.Token(0.01, 6),
    receiver: "0x050ea6A9277B476a1d54f3B023B649C555a970Bb",
    type: "erc20",
    contractAddress: "0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174",
  });

  function openAddFundModal() {
    setAddFundModal(true);
  }
  function closeAddFundModal() {
    setAddFundModal(false);
  }

  function openWithdrawModal() {
    setWithdrawModal(true);
  }
  function closeWithdrawModal() {
    setWithdrawModal(false);
  }

  const withdraw = async (event) => {
    event.preventDefault();
    const data = new FormData(event.target);
    const inputAmount = parseFloat(data.get("usdc"));

    const withdrawOptions = {
      contractAddress: configData.TRANSFER_CONTRACT,
      functionName: "withdrawToWalletFundBalance",
      abi: mainContractAbi,
      params: {
        amount: Moralis.Units.Token(inputAmount, 6)
      },
    };
    Moralis.executeFunction(withdrawOptions).then((withdrawResult) => {
      closeWithdrawModal();
      setFunding(false);
      toast.success('Withdraw successful!', options)
    }, (error) => {
      closeWithdrawModal();
      setFunding(false);
      toast.error("An error occurred on withdraw operation!", options);
    });

  };

  const addFund = async (event) => {

    event.preventDefault();
    const data = new FormData(event.target);
    const inputAmount = parseFloat(data.get("usdc"));
    setAmount(parseFloat(data.get("usdc")));
    //var transferResult = await fetch();

    setFunding(true);

    const approveOptions = {
      contractAddress: configData.USDC_CONTRACT,
      functionName: "approve",
      abi: usdcContractAbi,
      params: {
        spender: configData.TRANSFER_CONTRACT,
        amount: Moralis.Units.Token(inputAmount, 6)
      },
    };
    Moralis.executeFunction(approveOptions).then((approveResult) => {

      approveResult.wait().then((finalTx) => {
        const addFundOptions = {
          contractAddress: configData.TRANSFER_CONTRACT,
          functionName: "addFundBalance",
          abi: mainContractAbi,
          params: {
            tokens: Moralis.Units.Token(inputAmount, 6)
          },
        };
        Moralis.executeFunction(addFundOptions).then((addFundResult) => {
          closeAddFundModal();
          setFunding(false);
          toast.success('Adding funds successful!', options)
        }, (error) => {
          closeAddFundModal();
          setFunding(false);
          toast.error("An error occurred on adding fund operation!", options);
        });

      });

    }, (error) => {
      setFunding(false);
      toast.error("An error occurred on approve!", options);
    });

    /*const addFundOptions = {
      contractAddress: configData.TRANSFER_CONTRACT,
      functionName: "addFundBalance",
      abi: mainContractAbi,
      params: {
        tokens: Moralis.Units.Token(1, 6)
      },
    };
    Moralis.executeFunction(addFundOptions).then((addFundResult) => {
      console.log("addFundResult", addFundResult);
    }, (error) => {
      console.log("addFundError", error);
      toast.error("Transfer negotiations could not be started.", options);
    });*/

    if (!transferError) {
      /*const DepositCollection = Moralis.Object.extend("Deposits");
      const depositCollection = new DepositCollection();
      depositCollection.set("user", userInfo);
      depositCollection.set("amount", amount);
      depositCollection.set("hash", transferResult.hash);
      depositCollection.set("status", 0);
      depositCollection.set("fromAddress", transferResult.from);
      depositCollection.set("toAddress", transferResult.to);
      depositCollection.save()
      .then((collection) => {
          toast.success('Fund adding successful!',options)
          //setLoading(false)
      }, (error) => {
          toast.error('Unsuccessful!', options)
          //setLoading(false)
          alert('Failed to create new object, with error code: ' + error.message);
      });*/
    }
    /* Get token metadata
    const options = { chain: "polygon", symbols: "USDC" };
    const tokenMetadata = await Moralis.Web3API.token.getTokenMetadataBySymbol(options);
    console.log("tokenMetadata", tokenMetadata);*/
  };

  const connectMobileWalletPressed = async () => {
    const user = await Moralis.authenticate({ provider: "walletconnect" })
    const params = {};
    const userInfoCreate = await Moralis.Cloud.run("createUserInfo", params);
    const web3Provider = await Moralis.enableWeb3({ provider: "walletconnect" });
  }

  const connectWalletPressed = async () => {
    await authenticate();
    const params = {};
    const userInfoCreate = await Moralis.Cloud.run("createUserInfo", params);

    const web3Provider = await Moralis.enableWeb3();
    const chainIdHex = web3Provider.network.chainId;
    if (chainIdHex != 80001) {
      //TODO Confige alınacak
      try {
        await web3Provider.provider.request({
          method: "wallet_switchEthereumChain",
          params: [{ chainId: "0x89" }],
        });
      } catch (error) {
        if (error.code === 4902) {
          try {
            await web3Provider.provider.request({
              method: "wallet_addEthereumChain",
              params: [
                {
                  chainId: "0x89",
                  chainName: "Polygon Mainnet",
                  rpcUrls: ["https://polygon-rpc.com"],
                  nativeCurrency: {
                    name: "MATIC",
                    symbol: "MATIC",
                    decimals: 18,
                  },
                  blockExplorerUrls: ["https://explorer.matic.network/"],
                },
              ],
            });
          } catch (error) {
            alert(error.message);
          }
        }
      }
    }
  };
  useEffect(() => {
    const currentUser = Moralis.User.current();
    if (currentUser) {
      getUserData(currentUser);
      getUserWatchlistCount(currentUser);

    }
  }, []);

  async function getUserBalance() {

    const transferOptions = {
      contractAddress: configData.TRANSFER_CONTRACT,
      functionName: "getFundBalance",
      abi: mainContractAbi,
      params: {},
    };
    Moralis.executeFunction(transferOptions).then((balanceResult) => {
      setFunds(parseInt(balanceResult._hex, 16) / 1000000);
    }, (error) => {
      toast.error("Polygon mainnet connection could not be established!", options);
    });
  }

  async function getUserData(userInfos) {
    const query = new Moralis.Query("UserInfo");
    query.equalTo("userId", Moralis.User.current());
    query.include("club");
    const result = await query.first();
    setUserInfo(result);
    setUserData({
      ...userData,
      username: result.get("username"),
      wallet: userInfos.get("ethAddress"),
      profileImage: result.get("profilePicture")
        ? result.get("profilePicture").url()
        : defaultProfileImage,
      club: result.get("club"),
    });
    getUserBalance();
  }
  async function getUserWatchlistCount(user) {
    const query = new Moralis.Query("UserInfo");
    query.equalTo("userId", user);
    const userResult = await query.first();
    const query1 = new Moralis.Query("Watchlist");
    query1.equalTo("watcherId", userResult);
    const watcherResult = await query1.count();
    setWatcherCount(watcherResult);
  }
  return (
    <div>
      <div className={`site-overlay ${mobileMenu ? "site-overlay-mobile-menu-open" : ""}`} onClick={() => setMobileMenu(!mobileMenu)}></div>

      <header className="header header--layout-1">
        <div className="header__top-bar clearfix">
          <div className="container">
            <div className="header__top-bar-inner">
              <ul className="nav-account">
                {isAuthenticated && <li className="nav-account__item">
                  <Link to="/my-club">My Club</Link>
                </li>}
                {isAuthenticated && <li className="nav-account__item">
                  <Link to="/edit-club">Edit Account</Link>
                </li>}
                {isAuthenticated &&
                  <li className="nav-account__item nav-account__item--wishlist">
                    <Link to="/user-watchlist">
                      Watchlist <span className="highlight">{watcherCount}</span>
                    </Link>
                  </li>}
                {isAuthenticated &&
                  <li className="nav-account__item nav-account__item--logout">
                    <Link to="/" onClick={() => logout()}>Logout</Link>
                  </li>}
                <ul className="social-links social-links--inline social-links--main-nav">
                  <li className="social-links__item">
                    <a
                      href="https://t.me/mvliga"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title="Telegram"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-telegram-plane"></i>
                    </a>
                  </li>
                  <li className="social-links__item">
                    <a href="https://twitter.com/mvligacom"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title="Twitter"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>
                  </li>
                  <li className="social-links__item">
                    <a href="https://instagram.com/mvliga"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title="Instagram"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </li>
                </ul>
              </ul>
            </div>
          </div>
        </div>
        <div className="header__secondary">
          <div className="container">
            <div className="header-logo">
              <Link to="/">
                <img
                  src="/assets/images/soccer/logo_5.svg"
                  srcSet="/assets/images/soccer/logo_5.svg 2x"
                  alt="Alchemists"
                  className="header-logo__img"
                />
              </Link>
            </div>
            <ul className="info-block info-block--header">
              {/*<li className="info-block__item info-block__item--contact-primary"><svg role="img"
                            className="df-icon df-icon--whistle">
                            </svg>
                            <h1 className="info-block__heading">Home</h1>
                        </li>*/}
              <li className="info-block__item info-block__item--contact-secondary">
                <Link to="/stock-market">
                  <h6 className="info-block__heading">Stock Market</h6>
                </Link>
              </li>
              <li className="info-block__item info-block__item--contact-secondary">
                <Link to="/transfer-market">
                  <h6 className="info-block__heading">Transfer Market</h6>
                </Link>
              </li>
              <li className="info-block__item info-block__item--contact-secondary">
                <Link to="/fixture">
                  <h6 className="info-block__heading">Fixture</h6>
                </Link>
              </li>
              <li className="info-block__item info-block__item--contact-secondary">
                <Link to="/clubs">
                  <h6 className="info-block__heading">Clubs</h6>
                </Link>
              </li>
              {isAuthenticated && (
                <li className="info-block__item info-block__item--shopping-cart js-info-block__item--onhover has-children item-your-bag">
                  <Link to="#" className="info-block__link-wrapper">
                    <div className="df-icon-stack df-icon-stack--bag">
                      <svg role="img" className="df-icon df-icon--bag"></svg>{" "}
                      <svg
                        role="img"
                        className="df-icon df-icon--bag-handle"
                      ></svg>
                    </div>
                    <h6 className="info-block__heading">Club Budget</h6>
                    <span className="info-block__cart-sum">
                      {'$' + funds.toFixed(2)}
                    </span>
                  </Link>
                  <ul className="header-cart">
                    {/* <li className="header-cart__item">
                                    <figure className="header-cart__product-thumb"><Link to="_soccer_shop-product.html"><img
                                        src="assets/images/soccer/samples/_soccer_cart-sm-1.jpg" alt=""/></Link></figure>
                                    <div className="header-cart__inner"><span className="header-cart__product-cat">Sneakers</span>
                                        <h5 className="header-cart__product-name"><Link to="_soccer_shop-product.html">Sundown
                                            Sneaker</Link></h5>
                                        <div className="header-cart__product-ratings"><i className="fas fa-star"></i> <i
                                            className="fas fa-star"></i> <i className="fas fa-star"></i> <i className="fas fa-star"></i> <i
                                                className="fas fa-star empty"></i></div>
                                        <div className="header-cart__product-sum"><span className="header-cart__product-price">$28.00</span>
                                            x <span className="header-cart__product-count">2</span></div>
                                        <div className="fas fa-times header-cart__close"></div>
                                    </div>
                                </li>
                                <li className="header-cart__item">
                                    <figure className="header-cart__product-thumb"><Link to="_soccer_shop-product.html"><img
                                        src="assets/images/soccer/samples/_soccer_cart-sm-4.jpg" alt=""/></Link></figure>
                                    <div className="header-cart__inner"><span className="header-cart__product-cat">Sneakers</span>
                                        <h5 className="header-cart__product-name"><Link to="_soccer_shop-product.html">Atlantik
                                            Sneaker</Link></h5>
                                        <div className="header-cart__product-ratings"><i className="fas fa-star"></i> <i
                                            className="fas fa-star"></i> <i className="fas fa-star"></i> <i className="fas fa-star"></i> <i
                                                className="fas fa-star"></i></div>
                                        <div className="header-cart__product-sum"><span className="header-cart__product-price">$30.00</span>
                                            x <span className="header-cart__product-count">4</span></div>
                                        <div className="fas fa-times header-cart__close"></div>
                                    </div>
                                </li>
                                <li className="header-cart__item">
                                    <figure className="header-cart__product-thumb"><Link to="_soccer_shop-product.html"><img
                                        src="assets/images/soccer/samples/_soccer_cart-sm-2.jpg" alt=""/></Link></figure>
                                    <div className="header-cart__inner"><span className="header-cart__product-cat">Sneakers</span>
                                        <h5 className="header-cart__product-name"><Link to="_soccer_shop-product.html">Aquarium
                                            Sneaker</Link></h5>
                                        <div className="header-cart__product-ratings"><i className="fas fa-star"></i> <i
                                            className="fas fa-star"></i> <i className="fas fa-star"></i> <i
                                                className="fas fa-star empty"></i> <i className="fas fa-star empty"></i></div>
                                        <div className="header-cart__product-sum"><span className="header-cart__product-price">$28.00</span>
                                            x <span className="header-cart__product-count">1</span></div>
                                        <div className="fas fa-times header-cart__close"></div>
                                    </div>
                                </li> */}
                    <li className="header-cart__item header-cart__item--subtotal">
                      <span className="header-cart__subtotal">
                        Club Budget
                      </span>{" "}
                      <span className="header-cart__subtotal-sum">
                        {'$' + funds.toFixed(2)}
                      </span>
                    </li>
                    <li className="header-cart__item header-cart__item--subtotal">
                      <span className="header-cart__subtotal">Total Investment</span>{" "}
                      <span className="header-cart__subtotal-sum">$0.00</span>
                    </li>
                    <li className="header-cart__item header-cart__item--subtotal">
                      <span className="header-cart__subtotal">Club</span>{" "}
                      <span className="header-cart__subtotal-sum">
                        {userData.club && userData.club.get("name")}
                      </span>
                    </li>
                    <li className="header-cart__item header-cart__item--action">
                      <button onClick={() => openWithdrawModal()}
                        className="btn btn-default btn-block"
                      >
                        Withdraw
                      </button>
                      <button onClick={() => openAddFundModal()}
                        className="btn btn-warning btn-block"
                      >
                        Add Fund
                      </button>


                    </li>
                  </ul>
                </li>
              )}
              {!isAuthenticated && (
                <li className="info-block__item info-block__item--wallet">
                  <button
                    type="submit"
                    className="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed"
                    onClick={() => connectWalletPressed()}
                  >
                    Connect with Wallet
                  </button>
                </li>
              )}
            </ul>
          </div>
        </div>
        <div className="header__primary">
          <div className="container">
            <div className="header__primary-inner">
              <div className="header-logo">
                <Link to="/">
                  <img
                    src="/assets/images/soccer/logo_5.svg"
                    srcSet="/assets/images/soccer/logo_5.svg 2x"
                    alt="Alchemists"
                    className="header-logo__img"
                  />
                </Link>
              </div>
              <nav className={`main-nav clearfix ${mobileMenu ? "main-nav-mobile-menu" : ""}`}>
                <ul className="main-nav__list">
                  <div className="header-mobile__logo">
                    <span className="main-nav__back" onClick={() => setMobileMenu(false)}></span>
                    <Link to="/">
                      <img
                        src="/assets/images/soccer/logo_5.svg"
                        srcSet="/assets/images/soccer/logo_5.svg 2x"
                        alt="Alchemists"
                        className="header-mobile__logo-img"
                      />
                    </Link>
                  </div>
                  <li className="active">
                    <Link to="/">Home</Link>
                  </li>
                  <li>
                    <Link to="/stock-market">Stock Market</Link>
                  </li>
                  <li>
                    <Link to="/transfer-market">Transfer Market</Link>
                  </li>
                  <li >
                    <Link to="/clubs">Clubs</Link>
                  </li>
                  <li >
                    <Link to="/fixture">Fixture</Link>
                  </li>
                  {isAuthenticated &&
                    <li className="nav-account__item">
                      <Link to="/my-club"
                        data-toggle="modal"
                        data-target="#modal-login-register-tabs"
                      >
                        My Club
                      </Link>
                    </li>
                  }
                  {isAuthenticated && <li className="nav-account__item nav-account__item--wishlist">
                    <Link to="/user-watchlist">
                      Wishlist <span className="highlight">{watcherCount}</span>
                    </Link>
                  </li>}
                  {isAuthenticated && <li className="nav-account__item nav-account__item--logout">
                    <Link to="/" onClick={() => logout()}>Logout</Link>
                  </li>}
                  <li className="info-block__item info-block__item--contact-secondary">
                    <h6 className="info-block__heading">Contact Us</h6>
                    <a
                      className="info-block__link"
                      href="mailto:info@alchemists.com"
                    >
                      info@mvliga.com
                    </a>
                  </li>
                  <li className="main-nav__item--social-links">
                    <a href="https://t.me/mvliga"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title=""
                      data-original-title="Telegram"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-telegram-plane"></i>
                    </a>
                    <a to="https://twitter.com/mvligacom"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title=""
                      data-original-title="Twitter"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>
                    <a href="https://instagram.com/mvliga"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title=""
                      data-original-title="Instagram"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </li>
                </ul>
                <ul className="social-links social-links--inline social-links--main-nav">
                  <li className="social-links__item">
                    <a href="https://t.me/mvliga"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title=""
                      data-original-title="Telegram"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-telegram-plane"></i>
                    </a>
                  </li>
                  <li className="social-links__item">
                    <a href="https://twitter.com/mvligacom"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title=""
                      data-original-title="Twitter"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>
                  </li>
                  <li className="social-links__item">
                    <a href="https://instagram.com/mvliga"
                      className="social-links__link"
                      data-toggle="tooltip"
                      data-placement="bottom"
                      title=""
                      data-original-title="Instagram"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </li>
                </ul>
                <Link to="#" className="pushy-panel__toggle">
                  <span className="pushy-panel__line"></span>{" "}
                </Link>
              </nav>
            </div>
          </div>
        </div>
      </header>
      <div className={`header-mobile clearfix ${openMobileMenu ? "header-mobile--expanded" : ""}`} id="header-mobile">

        <div className="header-mobile__inner">
          <a
            id="header-mobile__toggle"
            className="burger-menu-icon"
            onClick={() => setMobileMenu(!mobileMenu)}
          >
            <span className="burger-menu-icon__line"></span>
          </a>{" "}
          <div className="header-mobile__logo">
            <Link to="/">
              <img
                src="/assets/images/soccer/logo_5.svg"
                srcSet="/assets/images/soccer/logo_5.svg 2x"
                alt="Alchemists"
                className="header-mobile__logo-img"
              />
            </Link>
          </div>
          {!isAuthenticated ?
            <i className="fa fa-wallet logged-user-icon" onClick={() => connectMobileWalletPressed()}></i> :
            <span class="header-mobile__search-icon" id="header-mobile__search-icon" onClick={() => setOpenMobileMenu(!openMobileMenu)}></span>
          }
        </div>
        { openMobileMenu && <div class="header-search-form">
          <ul class="header-cart mobile-menu-ul">
            <li class="header-cart__item header-cart__item--subtotal mobile-menu-li">
              <span class="header-cart__subtotal mobile-menu-first-span">Club Budget</span> 
              <span class="header-cart__subtotal-sum mobile-menu-second-span">$1.10</span>
            </li>
            <li class="header-cart__item header-cart__item--subtotal mobile-menu-li">
              <span class="header-cart__subtotal mobile-menu-first-span">Total Investment</span> 
              <span class="header-cart__subtotal-sum mobile-menu-second-span">$0.00</span>
            </li>
            <li class="header-cart__item header-cart__item--subtotal mobile-menu-li">
              <span class="header-cart__subtotal mobile-menu-first-span">Club</span> 
              <span class="header-cart__subtotal-sum mobile-menu-second-span" onClick={() => navigate(`/edit-club`)}>Manchester United <i class="fas fa-eye edit-club-eye"></i></span>
            </li>
            <li class="header-cart__item header-cart__item--action mobile-menu-li">
              <button class="btn btn-default btn-block" onClick={() => openWithdrawModal()}>Withdraw</button>
              <button class="btn btn-warning btn-block" onClick={() => openAddFundModal()}>Add Fund</button>
            </li>
          </ul>
        </div> }
      </div>

      <Modal
        isOpen={addFundModal}
        onRequestClose={closeAddFundModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <h5 style={{ textAlign: "center" }}>Add Fund</h5>
        <div className="card__content">
          <form action="#" className="df-personal-info" onSubmit={addFund}>
            <div className="col-md-12">
              <div className="form-group">
                <label htmlFor="lot">
                  Enter the amount you want to fund.
                </label>{" "}
                <input
                  type="number"
                  className="form-control"
                  name="usdc"
                  id="usdc"
                  placeholder="USDC ($)"
                  min="0.1"
                  step="0.1"
                  required
                />
              </div>
            </div>

            {!funding ?
              <div className="form-group--submit">
                <div className="row">
                  <div className="btn-auction col-md-6">
                    <button
                      onClick={() => closeAddFundModal()}
                      className="btn btn-default btn-lg btn-block general-save"
                    >
                      Close
                    </button>
                  </div>
                  <div className="btn-auction col-md-6">
                    <button
                      type="submit"
                      className="btn btn-default btn-lg btn-block general-save"
                    >
                      Add
                    </button>
                  </div>
                </div>
              </div> :
              <div className="form-group--submit">
                <div className="row">
                  <div className="btn-auction col-md-12" style={{ textAlign: "center" }}>
                    <img src="assets/images/moneyloading.gif" style={{ width: "150px" }} alt="" />
                  </div>
                </div>
              </div>
            }
          </form>
        </div>
      </Modal>

      <Modal
        isOpen={withdrawModal}
        onRequestClose={closeWithdrawModal}
        style={customStyles}
        contentLabel="Withdraw Modal"
      >
        <h5 style={{ textAlign: "center" }}>Withdraw</h5>
        <div className="card__content">
          <form action="#" className="df-personal-info" onSubmit={withdraw}>
            <div className="col-md-12">
              <div className="form-group">
                <label htmlFor="lot">
                  Enter the amount you want to withdraw.
                </label>{" "}
                <input
                  type="number"
                  className="form-control"
                  name="usdc"
                  id="usdc"
                  placeholder="USDC ($)"
                  min="0.1"
                  step="0.1"
                  required
                />
              </div>
            </div>

            {!funding ?
              <div className="form-group--submit">
                <div className="row">
                  <div className="btn-auction col-md-6">
                    <button
                      onClick={() => closeWithdrawModal()}
                      className="btn btn-default btn-lg btn-block general-save"
                    >
                      Close
                    </button>
                  </div>
                  <div className="btn-auction col-md-6">
                    <button
                      type="submit"
                      className="btn btn-default btn-lg btn-block general-save"
                    >
                      Ok
                    </button>
                  </div>
                </div>
              </div> :
              <div className="form-group--submit">
                <div className="row">
                  <div className="btn-auction col-md-12" style={{ textAlign: "center" }}>
                    <img src="assets/images/moneyloading.gif" style={{ width: "150px" }} alt="" />
                  </div>
                </div>
              </div>
            }
          </form>
        </div>
      </Modal>
    </div>
  );
};

export default Header;
