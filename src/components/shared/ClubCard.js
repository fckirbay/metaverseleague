import React, { useState, useEffect, Fragment } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useMoralis } from "react-moralis";

const ClubCard = (props) => {
  const item = props.data;
  const { Moralis } = useMoralis();

  const navigate = useNavigate();

  const clubDetail = (clubId) => {
    navigate(`/club/${clubId}`);
  };

  useEffect(() => {}, []);
  return (
    <Fragment>
      <div className="col-lg-3" key={`player-${item.club_id}`}>
        <aside className="widget widget--sidebar card widget-preview">
          <div className="widget__content card__content">
            <div className="match-preview">
              <section className="match-preview__body" onClick={() => clubDetail(item.id)}>
                <div className="match-preview__content club-preview-in-club-detail">
                  <div className="match-preview__team match-preview__team--first club-logo-in-club-detail">
                    <figure className="match-preview__team-logo">
                      <img
                        className="club-detail-club-logo-img"
                        src={item.get("logo") ? item.get("logo").url() : "assets/images/soccer/logos/alchemists_buy_tickets.png"}
                        alt=""
                      />
                    </figure>
                    <h5 className="match-preview__team-name">{item.get("name")}</h5>
                    <div className="match-preview__team-info match-preview__team-info-slogan">
                      {item.get("slogan")}
                    </div>
                  </div>
                </div>
              </section>
              <div className="match-preview__action match-preview__action--ticket club-list-invest-button">
                <a
                  href="#"
                  className="btn btn-primary-inverse btn-sm btn-block"
                >
                  Invest Now
                </a>
              </div>
            </div>
          </div>
        </aside>
      </div>
    </Fragment>
  );
};

export default ClubCard;
