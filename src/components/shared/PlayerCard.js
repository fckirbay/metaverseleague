import React, { useState, useEffect, Fragment } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useMoralis } from "react-moralis";
import configData from "../../config/config.json";
import { toast } from "react-toastify";
toast.configure();
const options = {
  autoClose: 100,
  hideProgressBar: true,
  position: "bottom-right",
  theme: "dark"
};

const PlayerCard = (props) => {

    const item = props.data;
    const { Moralis } = useMoralis();

    const navigate = useNavigate();

    const playerDetail = (playerId) => {
        navigate(`/player/${playerId}`);
    }
    const [photoStatusStyle, setPhotoStatusStyle] = useState('widget-player__ribbon');

    const photoStatus = async (playerId) => {
        const playerList = Moralis.Object.extend("Players");
        const query = new Moralis.Query(playerList);
        query.equalTo("player_id", parseInt(playerId));
        const result = await query.first();
        result.set("photoStatus", false);
        result.save(null, { useMasterKey: true }).then(
        (res)  => {
            setPhotoStatusStyle('widget-player__ribbon_err')
            toast.success('Bad image!',options);
        },
        (error) => {
            toast.success('Good image!',options);
        }
        );
    }
    const getPhotoStatus = async (playerId) => {
        const playerList = Moralis.Object.extend("Players");
        const query = new Moralis.Query(playerList);
        query.equalTo("player_id", parseInt(playerId));
        const result = await query.first();
        if(result.get("photoStatus") == false) {
            setPhotoStatusStyle('widget-player__ribbon_err')
        }
    }

    useEffect(() => {
        getPhotoStatus(item.get("player_id"))
    }, [])
    return (
        <Fragment>
            <div className="col-md-12 col-lg-6 col-sm-12 col-xs-12 col-xl-4 homepage-player-card" key={`player-${item.player_id}`} onClick={() => playerDetail(item.get("player_id"))}>
                <aside className="widget card widget--sidebar widget-player widget-player--soccer">
                    <div className="widget__content card__content">
                        {/*<div className={photoStatusStyle}>
                            <div className="fas fa-star" onClick={() => photoStatus(item.get("player_id"))}>         
                            </div>
                        </div>*/}
                        <figure className="widget-player__photo">
                            <img
                                src={`${ 'https://ipfs.moralis.io:2053/ipfs/' + item.get("photo")}`}
                                onClick={() => playerDetail(item.get("player_id"))}
                                alt={item.get("short_name")}
                                className="player-photo"
                            />
                        </figure>
                        <header className="widget-player__header clearfix">
                            <div className="widget-player__number">07</div>
                            <h4 className="widget-player__name">
                                {/*<span className="widget-player__first-name">K.</span>*/}
                                <span className="widget-player__last-name">
                                    {item.get("short_name")}
                                </span>
                            </h4>
                        </header>
                        <div className="widget-player__content">
                            <div className="widget-player__content-inner">
                                <div className="widget-player__stat widget-player__goals">
                                    <div className="widget-player__stat-number">
                                        {item.get("overall")}
                                    </div>
                                    <h6 className="widget-player__stat-label">Overall</h6>
                                </div>
                                <div className="widget-player__stat widget-player__shots">
                                    <div className="widget-player__stat-number">
                                        {item.get("potential")}
                                    </div>
                                    <h6 className="widget-player__stat-label">
                                        Potential
                                    </h6>
                                </div>
                                <div className="widget-player__stat widget-player__assists">
                                    <div className="widget-player__stat-number">
                                        {item.get("preferred_foot")}
                                    </div>
                                    <h6 className="widget-player__stat-label">Foot</h6>
                                </div>
                                <div className="widget-player__stat widget-player__games">
                                    <div className="widget-player__stat-number">
                                        {item.get("best_position")}
                                    </div>
                                    <h6 className="widget-player__stat-label">
                                        Position
                                    </h6>
                                </div>
                            </div>
                            <div className="widget-player__content-alt">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="row">
                                            <div className="col-md-7 col-sm-7 col-7">
                                                <div className="progress-stats">
                                                    <div className="progress__label">
                                                        Market Value
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5">
                                                <div className="progress-stats player-stats-right">
                                                    <div className="progress__number">
                                                        ${item.get("market_value") / 10000}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="row">
                                            <div className="col-md-7 col-sm-7 col-7">
                                                <div className="progress-stats">
                                                    <div className="progress__label">Wage</div>
                                                </div>
                                            </div>
                                            <div className="col-md-5 col-sm-5 col-5">
                                                <div className="progress-stats player-stats-right">
                                                    <div className="progress__number">
                                                        ${item.get("wage") / 10000} / M
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </Fragment>
    );
};

export default PlayerCard;
