
import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Autoplay } from 'swiper';
import { Link } from "react-router-dom";
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/autoplay';
const Slider = () => {

    return (
        <div className="hero-slider-wrapper">
            <div className="hero-slider">
            <Swiper
                  slidesPerView={1}
                  spaceBetween={30}
                  pagination={{
                    clickable: true,
                  }}
                  navigation={true}
                  modules={[Pagination, Navigation, Autoplay]}
                  className="mySwiper"
                  autoplay={{ delay: 3000, disableOnInteraction: false, reverseDirection: false, waitForTransition: true }}
                >
                <SwiperSlide>
                <div className="hero-slider__item hero-slider__item--img1">
                    <div className="container hero-slider__item-container">
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className="post__meta-block post__meta-block--top">
                                    <h1 className="page-heading__title"><Link to="#">Create your own club
                                        <br /><span className="highlight"> own club!</span></Link>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </SwiperSlide>
                <SwiperSlide>
                <div className="hero-slider__item hero-slider__item--img2">
                    <div className="container hero-slider__item-container">
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className="post__meta-block post__meta-block--top">
                                    <h1 className="page-heading__title"><Link to="#">
                                        <span className="highlight">Make transfer, <br /></span>manage your budget!</Link>
                                    </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </SwiperSlide>
                <SwiperSlide>
                <div className="hero-slider__item hero-slider__item--img3">
                    <div className="container hero-slider__item-container">
                        <div className="row">
                            <div className="col-md-8 offset-md-2">
                                <div className="post__meta-block post__meta-block--top">
                                    <h1 className="page-heading__title"><Link to="#">Increase your income as <br /><span
                                        className="highlight"> you win matches!</span>
                                        </Link></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </SwiperSlide>

             </Swiper>
            {/*<div className="hero-slider-thumbs-wrapper">
                <div className="container">
                    <div className="hero-slider-thumbs posts posts--simple-list">
                        <div className="hero-slider-thumbs__item">
                            <div className="posts__item posts__item--category-1">
                                <div className="posts__inner">
                                    <div className="posts__cat"><span className="label posts__cat-label">The Team</span></div>
                                    <h6 className="posts__title">The Alchemists won the last game 2-0 against Clovers</h6>
                                    <time dateTime="2017-12-12" className="posts__date">August 28th, 2018</time>
                                </div>
                            </div>
                        </div>
                        <div className="hero-slider-thumbs__item">
                            <div className="posts__item posts__item--category-2">
                                <div className="posts__inner">
                                    <div className="posts__cat"><span className="label posts__cat-label">Injuries</span></div>
                                    <h6 className="posts__title">Franklin Stevens has a knee fracture and is gona be out
                                    </h6><time dateTime="2017-12-12" className="posts__date">August 28th, 2018</time>
                                </div>
                            </div>
                        </div>
                        <div className="hero-slider-thumbs__item">
                            <div className="posts__item posts__item--category-1">
                                <div className="posts__inner">
                                    <div className="posts__cat"><span className="label posts__cat-label">The Team</span></div>
                                    <h6 className="posts__title">The New Eco Friendly Stadium won a leafy award in 2016</h6>
                                    <time dateTime="2017-12-12" className="posts__date">August 28th, 2018</time>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div> */}
            </div>
        </div>
    );
}

export default Slider;
