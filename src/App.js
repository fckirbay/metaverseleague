import './App.css';
import { Routes , Route } from 'react-router-dom';
import React, { useEffect } from "react";
import routes from './routes'
import { useMoralis } from "react-moralis";


function App() {
    const { isAuthenticated, isWeb3Enabled, enableWeb3, Moralis } = useMoralis();
    const serverUrl = "https://spxyeab0nyng.usemoralis.com:2053/server";
    const appId = "4rxXn6cw1iUimwExzE26f99X2Wv3j6MTlyYSAVbi";
    Moralis.start({ serverUrl, appId});
    useEffect(() => {
        if(!isWeb3Enabled && isAuthenticated) {enableWeb3();
        }
    }, [isWeb3Enabled, isAuthenticated, enableWeb3]);
    return (
        <Routes >
            {
            routes.map((data,index) => (
                <Route onUpdate={() => window.scrollTo(0, 0)} exact={true} path={data.path} element={data.component} key={index} />
            ))
            }
      </Routes>
    );
}

export default App;
