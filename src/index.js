import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { MoralisProvider } from "react-moralis";

ReactDOM.render(
  <MoralisProvider
    appId="4rxXn6cw1iUimwExzE26f99X2Wv3j6MTlyYSAVbi"
    serverUrl="https://spxyeab0nyng.usemoralis.com:2053/server"
  >
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </MoralisProvider>,
  document.getElementById("root")
);
