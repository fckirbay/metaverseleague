import Home from "./pages/home/Home";
import Club from "./pages/clubs/Club";
import MyShares from "./pages/my-shares/MyShares";
import EditClub from "./pages/my-club/EditClub";
import TransferMarket from "./pages/transfer-market/TransferMarket";
import Fixture from "./pages/fixture/Fixture";
import Player from "./pages/player/Player";
import Clubs from "./pages/clubs/Clubs";
import PrivacyPolicy from "./pages/privacy-policy/PrivacyPolicy";
import TermsAndConditions from "./pages/terms-and-conditions/TermsAndConditions";
import Disclaimer from "./pages/disclaimer/Disclaimer";
import Whitepaper from "./pages/whitepaper/Whitepaper";
import StockMarket from "./pages/stock-market/StockMarket";
import OurTeam from "./pages/our-team/OurTeam";
import NoResult from "./pages/no-result/NoResult";
import UserWatchlist from "./pages/watchlist/UserWatchlist";

const routes = [
  { path: '/', component: <Home />},
  { path: '/my-club', component: <Club />},
  { path: '/my-shares', component: <MyShares />},
  { path: '/club/:id', component: <Club />},
  { path: '/club', component: <Club />},
  { path: '/edit-club', component: <EditClub />},
  { path: '/transfer-market', component: <TransferMarket />},
  { path: '/fixture', component: <Fixture />},
  { path: '/player/:id', component: <Player />},
  { path: '/clubs', component: <Clubs />},
  { path: '/user-watchlist', component: <UserWatchlist />},
  { path: '/privacy-policy', component: <PrivacyPolicy />},
  { path: '/terms-and-conditions', component: <TermsAndConditions />},
  { path: '/disclaimer', component: <Disclaimer />},
  { path: '/whitepaper', component: <Whitepaper />},
  { path: '/stock-market', component: <StockMarket />},
  { path: '/our-team', component: <OurTeam />},
  { path: '*', component: <NoResult />},
]

export default routes;